import numpy as np
import pandas as pd


def dcg_at_k(r, k, method=0):
    """Score is discounted cumulative gain (dcg)
    Relevance is positive real values.  Can use binary
    as the previous methods.
    Example from
    http://www.stanford.edu/class/cs276/handouts/EvaluationNew-handout-6-per.pdf
    >>> r = [3, 2, 3, 0, 0, 1, 2, 2, 3, 0]
    >>> dcg_at_k(r, 1)
    3.0
    >>> dcg_at_k(r, 1, method=1)
    3.0
    >>> dcg_at_k(r, 2)
    5.0
    >>> dcg_at_k(r, 2, method=1)
    4.2618595071429155
    >>> dcg_at_k(r, 10)
    9.6051177391888114
    >>> dcg_at_k(r, 11)
    9.6051177391888114
    Args:
        r: Relevance scores (list or numpy) in rank order
            (first element is the first item)
        k: Number of results to consider
        method: If 0 then weights are [1.0, 1.0, 0.6309, 0.5, 0.4307, ...]
                If 1 then weights are [1.0, 0.6309, 0.5, 0.4307, ...]
    Returns:
        Discounted cumulative gain
    """
    r = np.asfarray(r)[:k]
    if r.size:
        if method == 0:
            return r[0] + np.sum(r[1:] / np.log2(np.arange(2, r.size + 1)))
        elif method == 1:
            return np.sum(r / np.log2(np.arange(2, r.size + 2)))
        else:
            raise ValueError("method must be 0 or 1.")
    return 0.


def ndcg_at_k(r, k, method=0):
    """Score is normalized discounted cumulative gain (ndcg)
    Relevance is positive real values.  Can use binary
    as the previous methods.
    Example from
    http://www.stanford.edu/class/cs276/handouts/EvaluationNew-handout-6-per.pdf
    >>> r = [3, 2, 3, 0, 0, 1, 2, 2, 3, 0]
    >>> ndcg_at_k(r, 1)
    1.0
    >>> r = [2, 1, 2, 0]
    >>> ndcg_at_k(r, 4)
    0.9203032077642922
    >>> ndcg_at_k(r, 4, method=1)
    0.96519546960144276
    >>> ndcg_at_k([0], 1)
    0.0
    >>> ndcg_at_k([1], 2)
    1.0
    Args:
        r: Relevance scores (list or numpy) in rank order
            (first element is the first item)
        k: Number of results to consider
        method: If 0 then weights are [1.0, 1.0, 0.6309, 0.5, 0.4307, ...]
                If 1 then weights are [1.0, 0.6309, 0.5, 0.4307, ...]
    Returns:
        Normalized discounted cumulative gain
    """
    dcg_max = dcg_at_k(sorted(r, reverse=True), k, method)
    if not dcg_max:
        return 0.
    return dcg_at_k(r, k, method) / dcg_max


def _get_predicted_correct_labels(y_true: np.array,
                                  y_pred: np.array):
    return np.array([np.shape(np.intersect1d(np.where(y_true[idx])[0], np.where(y_pred[idx])[0]))[0]
                     for idx in range(y_true.shape[0])])


def multilabel_accuracy(y_true: np.array,
                        y_pred: np.array):
    """ Accuracy for each instance is defined as the proportion of the predicted correct labels
    to the total number (predicted and actual) of labels for that instance.
    Overall accuracy is the average across all instances.
    """
    predicted_correct_labels = _get_predicted_correct_labels(y_true, y_pred)
    total_number_of_labels = np.sum((y_true + y_pred) >= 1, axis=1)

    return np.mean(predicted_correct_labels / total_number_of_labels)


def multilabel_precision(y_true: np.array,
                         y_pred: np.array):
    """ Precision is the proportion of predicted correct labels to the total number of actual
    labels, averaged over all instances.
    """
    predicted_correct_labels = _get_predicted_correct_labels(y_true, y_pred)
    total_number_of_actual_labels = np.sum(y_true, axis=1)
    return np.mean(predicted_correct_labels / total_number_of_actual_labels)


def multilabel_recall(y_true: np.array,
                      y_pred: np.array):
    """ Recall is the proportion of predicted correct labels to the total number of predicted
    labels, averaged over all instances.
    """
    predicted_correct_labels = _get_predicted_correct_labels(y_true, y_pred)
    total_number_of_predicted_labels = np.sum(y_pred, axis=1)
    total_number_of_predicted_labels[total_number_of_predicted_labels == 0] = 1
    return np.mean(predicted_correct_labels / total_number_of_predicted_labels)


def hamming_loss(y_true: np.array,
                 y_pred: np.array):
    """ Hamming loss reports how many times on average, the relevance of and example to a class
    label is incorrectly predicted. Therefore, hamming loss takes into account the prediction
    error (and incorrect label is predicted) and the missing error (a relevant label not
    predicted), normalized over total number of classes and total number of examples.
    """
    return np.mean(np.abs(y_true - y_pred) != 0)


def topN_accuracy(y_true: np.array,
                  y_pred_onehot: np.array,
                  N: int):
    """ Top N accuracy
    """
    assert len(y_true.shape) == 1
    assert len(y_pred_onehot.shape) == 2
    assert y_true.shape[0] == y_pred_onehot.shape[0]
    assert y_pred_onehot.shape[1] >= N

    true_positive = 0
    for label, top_n_pred in zip(y_true, np.argsort(-y_pred_onehot, axis=-1)[:, :N]):
        if label in top_n_pred:
            true_positive += 1

    accuracy = true_positive / len(y_true)

    return accuracy


def diversity(model_scores: pd.DataFrame, true_scores: pd.DataFrame):
    assert isinstance(model_scores, pd.DataFrame)
    assert isinstance(true_scores, pd.DataFrame)
    # true_scores's shape is (num_images, num_filters)
    num_filters = true_scores.shape[1]

    dmr = true_scores.apply(lambda x: x.rank(ascending=False, method="min") <= 2, axis=1)
    dmp = model_scores.apply(lambda x: x.rank(ascending=False, method="min") <= 2, axis=1)
    x = np.arange(0, num_filters, 1)
    # there are no reason for number 5
    # range from 1 ~ 5 expoentially
    exp_weight = np.exp(x / ((num_filters - 1) / np.log(5)))
    diversity = ((dmr & dmp).astype(float) * exp_weight).max(axis=1).mean()
    return diversity


def jaccard_distance(model_scores: pd.DataFrame, true_scores: pd.DataFrame):
    assert isinstance(model_scores, pd.DataFrame)
    assert isinstance(true_scores, pd.DataFrame)
    dmr = true_scores.apply(lambda x: x.rank(ascending=False, method="min") <= 2, axis=1)
    dmp = model_scores.apply(lambda x: x.rank(ascending=False, method="min") <= 2, axis=1)
    avg_jaccard = ((dmr & dmp).sum(axis=1) / (dmr | dmp).sum(axis=1)).mean()
    return avg_jaccard
