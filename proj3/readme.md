# 기계학습 산업응용 Project3
## M1505.001300_001 2018 가을 Team5

| Team5 Members |||
| --- | --- | --- |
| 2011-11949 신범준 | 2011-11927 임능환 | 2012-11842 김진홍 |


```
pip install -r requirements.txt
```


- 조교님 뼈대 코드를 거의 그대로 사용하였기 때문에 자세하게 적진 않겠습니다.
- 프로젝트 루트폴더에 `data/`가 있다고 가정합니다 (`data/chords.pkl`, `data/features.pkl`, `data/data/*.wav`)
- 첫 학습에서 features.py에 새로 정의된 `compute_logmel_spectrogram`로 `features_mel128.pkl`을 만듭니다.
- Best Performance를 냈던 모델의 학습과 밸리데이션은 별도의 프로세스로 다음과 같은 커맨드로 돌렸습니다.

학습:

```
python main.py --epoch_num 100000 --model model11 --num_melbins 128  # checkpoint/run-XXXX-XXXX 생성
```

평가:

```
python main.py --epoch_num 100000 --model model11 --num_melbins 128 --checkpoint_path checkpoint/run-XXXX-XXXX --test_from_ckpt
```

- 위 내용을 바탕으로 조교님 데이터로 평가해주시면 될거 같습니다.


