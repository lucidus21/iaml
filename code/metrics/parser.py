from abc import ABC, ABCMeta, abstractmethod

import numpy as np
import tensorflow as tf

from metrics.base import DataStructure


class MetricDataParserBase(ABC):
    @classmethod
    def parse_build_data(cls, data):
        """
        Args:
            data: dictionary which will be passed to InputBuildData
        """
        data = cls._validate_build_data(data)
        data = cls._process_build_data(data)
        return data

    @classmethod
    def parse_non_tensor_data(cls, data):
        """
        Args:
            data: dictionary which will be passed to InputDataStructure
        """
        input_data = cls._validate_non_tensor_data(data)
        output_data = cls._process_non_tensor_data(input_data)
        return output_data

    @classmethod
    def _validate_build_data(cls, data):
        """
        Specify assertions that tensor data should contains

        Args:
            data: dictionary
        Return:
            InputDataStructure
        """
        return cls.InputBuildData(data)

    @classmethod
    def _validate_non_tensor_data(cls, data):
        """
        Specify assertions that non-tensor data should contains

        Args:
            data: dictionary
        Return:
            InputDataStructure
        """
        return cls.InputNonTensorData(data)

    """
    Override these two functions if needed.
    """
    @classmethod
    def _process_build_data(cls, data):
        """
        Process data in order to following metrics can use it

        Args:
            data: InputBuildData

        Return:
            OutputBuildData
        """
        # default function is just passing data
        return cls.OutputBuildData(data.to_dict())

    @classmethod
    def _process_non_tensor_data(cls, data):
        """
        Process data in order to following metrics can use it

        Args:
            data: InputNonTensorData

        Return:
            OutputNonTensorData
        """
        # default function is just passing data
        return cls.OutputNonTensorData(data.to_dict())

    """
    Belows should be implemented when inherit.
    """
    class InputBuildData(DataStructure, metaclass=ABCMeta):
        pass

    class OutputBuildData(DataStructure, metaclass=ABCMeta):
        pass

    class InputNonTensorData(DataStructure, metaclass=ABCMeta):
        pass

    class OutputNonTensorData(DataStructure, metaclass=ABCMeta):
        pass


class SingleLabelClassificationDataParser(MetricDataParserBase):
    """
    Single-label classification parser
    """
    class InputBuildData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",

            "losses",  # Dict | loss_key -> Tensor
        ]

    class OutputBuildData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",

            "losses",
        ]

    class InputNonTensorData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",
            "probcut_precision_cut_args",  # Namespace |
            # (probcut_threshold_list, precision_threshold_list,
            #  probcut_save_fullpath, precision_cut_save_fullpath)
            "predictions_onehot",
            "labels_onehot",
        ]

    class OutputNonTensorData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",
            "probcut_precision_cut_args",

            "predictions_onehot",
            "labels_onehot",
            "predictions",
            "labels",
        ]

    @classmethod
    def _process_non_tensor_data(cls, data):
        predictions = data.predictions_onehot.argmax(axis=-1)
        labels = data.labels_onehot.argmax(axis=-1)

        return cls.OutputNonTensorData({
            "dataset_split_name": data.dataset_split_name,
            "label_names": data.label_names,
            "probcut_precision_cut_args": data.probcut_precision_cut_args,

            "predictions_onehot": data.predictions_onehot,
            "labels_onehot": data.labels_onehot,
            "predictions": predictions,
            "labels": labels,
        })


class SingleLabelClassificationBCDataParser(MetricDataParserBase):
    """
    Single-label BC parser
    """
    class InputBuildData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",

            "losses",  # Dict | loss_key -> Tensor
        ]

    class OutputBuildData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",

            "losses",
        ]

    class InputNonTensorData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",
        ]

    class OutputNonTensorData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",
        ]

    @classmethod
    def _process_non_tensor_data(cls, data):
        return cls.OutputNonTensorData({
            "dataset_split_name": data.dataset_split_name,
            "label_names": data.label_names,
        })


class MultiLabelClassificationDataParser(MetricDataParserBase):
    """
    Multi-label classification parser
    """
    class InputBuildData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",

            "losses",  # Dict | loss_key -> Tensor
        ]

    class OutputBuildData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",

            "losses",
        ]

    class InputNonTensorData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",
            "sigmoid_threshold",

            "predictions_multihot",
            "labels_multihot",
        ]

    class OutputNonTensorData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",

            "predictions_multihot",
            "labels_multihot",
        ]

    @classmethod
    def _process_non_tensor_data(cls, data):
        predictions_multihot_treshold = np.array(data.predictions_multihot > data.sigmoid_threshold, dtype=np.uint8)

        return cls.OutputNonTensorData({
            "dataset_split_name": data.dataset_split_name,
            "label_names": data.label_names,

            "predictions_multihot": predictions_multihot_treshold,
            "labels_multihot": data.labels_multihot,
        })


class MultiTaskClassificationDataParser(MetricDataParserBase):
    """
    Multi-task classification parser
    """
    class InputBuildData(DataStructure):
        _keys = [
            "dataset_split_name",
            "task_label_names",  # Dict | task_name -> List[label_name]
            "num_task_labels",  # Dict | task_name -> int(num_labels)
            "task_names",  # List[task_name]
            "use_class_metrics",  # Dict | task_name -> bool(use_class_metric)

            "losses",  # Dict | loss_key -> Tensor
        ]

    class OutputBuildData(DataStructure):
        _keys = [
            "dataset_split_name",
            "task_label_names",
            "num_task_labels",
            "task_names",
            "use_class_metrics",

            "losses",
        ]

    class InputNonTensorData(DataStructure):
        _keys = [
            "dataset_split_name",
            "task_label_names",
            "num_task_labels",
            "task_names",
            "use_class_metrics",
            "probcut_precision_cut_args",  # Namespace |
            # (probcut_threshold_list, precision_threshold_list,
            #  probcut_save_fullpath, precision_cut_save_fullpath)
            "predictions_onehot",  # (N, t, c)
            "labels_onehot",  # (N, t, c)
        ]

    class OutputNonTensorData(DataStructure):
        _keys = [
            "dataset_split_name",
            "task_label_names",
            "num_task_labels",
            "task_names",
            "use_class_metrics",
            "probcut_precision_cut_args",

            "predictions_onehot",
            "labels_onehot",
            "predictions",  # (N, t)
            "labels",  # (N, t)
            "valid_indexes",  # (N, t)
        ]

    @classmethod
    def _process_non_tensor_data(cls, data):
        predictions = data.predictions_onehot.argmax(axis=-1)  # (N, t)
        labels = data.labels_onehot.argmax(axis=-1)  # (N, t)

        valid_indexes = np.zeros(predictions.shape, dtype=bool)
        for task_idx, task_name in enumerate(data.task_names):
            num_labels = data.num_task_labels[task_name]

            task_labels_onehot = data.labels_onehot[:, task_idx, :num_labels]
            weights = np.sum(task_labels_onehot, axis=-1)
            valid_indexes[:, task_idx] = (weights == 1.0)

        return cls.OutputNonTensorData({
            "dataset_split_name": data.dataset_split_name,
            "task_label_names": data.task_label_names,
            "num_task_labels": data.num_task_labels,
            "task_names": data.task_names,
            "use_class_metrics": data.use_class_metrics,
            "probcut_precision_cut_args": data.probcut_precision_cut_args,

            "predictions_onehot": data.predictions_onehot,
            "labels_onehot": data.labels_onehot,
            "predictions": predictions,
            "labels": labels,
            "valid_indexes": valid_indexes,
        })


class SegmentationDataParser(MetricDataParserBase):
    """ Segmentation parser
    """
    class InputBuildData(DataStructure):
        _keys = [
            "dataset_split_name",
            "target_shape",  # Tuple(int, int) | (height, width)

            "losses",  # Dict | loss_key -> Tensor
            "summary_images",  # Dict | summary_name -> Tensor
            "misc_images",  # Dict | name -> Tensor
            "masks",  # Tensor
            "probs",  # Tensor
        ]

    class OutputBuildData(DataStructure):
        _keys = [
            "dataset_split_name",
            "target_shape",

            "losses",
            "summary_images",
            "misc_images",
            "masks",
            "alpha_scores",  # Tensor
        ]

    class InputNonTensorData(DataStructure):
        _keys = [
            "dataset_split_name",

            "batch_infer_time",
            "unit_infer_time",
            "misc_images",
            "image_save_dir",
        ]

    class OutputNonTensorData(DataStructure):
        _keys = [
            "dataset_split_name",

            "batch_infer_time",
            "unit_infer_time",
            "misc_images",
            "image_save_dir",
        ]

    @classmethod
    def _process_build_data(cls, data):
        masks = tf.expand_dims(data.masks, axis=3)
        alpha_scores = data.probs[:, :, :, -1:]

        if data.target_shape:
            # TODO: why align_corner is different from above?
            masks = tf.image.resize_bilinear(masks, data.target_shape)
            alpha_scores = tf.image.resize_bilinear(alpha_scores, data.target_shape, align_corner=True)

        return cls.OutputBuildData({
            "dataset_split_name": data.dataset_split_name,
            "target_shape": data.target_shape,
            "losses": data.losses,
            "summary_images": data.summary_images,
            "misc_images": data.misc_images,

            "masks": masks,
            "alpha_scores": alpha_scores,
        })

    @classmethod
    def _process_non_tensor_data(cls, data):
        return cls.OutputNonTensorData({
            "dataset_split_name": data.dataset_split_name,
            "batch_infer_time": data.batch_infer_time,
            "unit_infer_time": data.unit_infer_time,
            "misc_images": data.misc_images,
            "image_save_dir": data.image_save_dir,
        })


class SingleLabelDistillationDataParser(MetricDataParserBase):
    class InputBuildData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",

            "losses",  # Dict | loss_key -> Tensor
        ]

    class OutputBuildData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",

            "losses",
        ]

    class InputNonTensorData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",
            "predictions_onehot",
            "labels_onehot",
        ]

    class OutputNonTensorData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",

            "predictions_onehot",
            "labels_onehot",
            "predictions",
            "labels",
        ]

    @classmethod
    def _process_non_tensor_data(cls, data):
        predictions = data.predictions_onehot.argmax(axis=-1)
        labels = data.labels_onehot.argmax(axis=-1)

        return cls.OutputNonTensorData({
            "dataset_split_name": data.dataset_split_name,
            "label_names": data.label_names,

            "predictions_onehot": data.predictions_onehot,
            "labels_onehot": data.labels_onehot,
            "predictions": predictions,
            "labels": labels,
        })


class MultiLabelDistillationDataParser(MetricDataParserBase):
    class InputBuildData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",

            "losses",  # Dict | loss_key -> Tensor
        ]

    class OutputBuildData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",

            "losses",
        ]

    class InputNonTensorData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",
            "sigmoid_threshold",

            "predictions_multihot",
            "labels_multihot",
        ]

    class OutputNonTensorData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",

            "predictions_multihot",
            "labels_multihot",
        ]

    @classmethod
    def _process_non_tensor_data(cls, data):
        predictions_multihot_treshold = np.array(data.predictions_multihot > data.sigmoid_threshold, dtype=np.uint8)

        return cls.OutputNonTensorData({
            "dataset_split_name": data.dataset_split_name,
            "label_names": data.label_names,

            "predictions_multihot": predictions_multihot_treshold,
            "labels_multihot": data.labels_multihot,
        })


class ImageGenerateDataParser(MetricDataParserBase):
    """ Image Generation parser
    """
    class InputBuildData(DataStructure):
        _keys = [
            "dataset_split_name",

            "losses",  # Dict | loss_key -> Tensor
            "summary_images",  # Dict | summary_name -> Tensor
            "gts",  # Tensor
            "predictions",  # Tensor
        ]

    class OutputBuildData(DataStructure):
        _keys = [
            "dataset_split_name",

            "losses",
            "summary_images",
            "gts",
            "predictions",
        ]

    class InputNonTensorData(DataStructure):
        _keys = [
            "dataset_split_name",

            "batch_infer_time",
            "unit_infer_time",
        ]

    class OutputNonTensorData(DataStructure):
        _keys = [
            "dataset_split_name",

            "batch_infer_time",
            "unit_infer_time",
        ]

    @classmethod
    def _process_build_data(cls, data):
        return cls.OutputBuildData({
            "dataset_split_name": data.dataset_split_name,
            "losses": data.losses,
            "summary_images": data.summary_images,
            "gts": data.gts,
            "predictions": data.predictions,
        })

    @classmethod
    def _process_non_tensor_data(cls, data):
        return cls.OutputNonTensorData({
            "dataset_split_name": data.dataset_split_name,
            "batch_infer_time": data.batch_infer_time,
            "unit_infer_time": data.unit_infer_time,
        })


class PicaiDataParser(MetricDataParserBase):
    class InputBuildData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",

            "summary_images",  # Dict | summary_name -> Tensor
            "losses",  # Dict | loss_key -> Tensor
        ]

    class OutputBuildData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",

            "summary_images",  # Dict | summary_name -> Tensor
            "losses",
        ]

    class InputNonTensorData(DataStructure):
        _keys = [
            "dataset_split_name",

            "label_names",
            "model_scores",
            "true_scores",
        ]

    class OutputNonTensorData(DataStructure):
        _keys = [
            "dataset_split_name",

            "label_names",
            "model_scores",
            "true_scores",
        ]

    @classmethod
    def _process_non_tensor_data(cls, data):
        return cls.OutputNonTensorData({
            "label_names": data.label_names,
            "model_scores": data.model_scores,
            "true_scores": data.true_scores,
            "dataset_split_name": data.dataset_split_name,
        })


class AudioDataParser(MetricDataParserBase):
    class InputBuildData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",
            "losses",  # Dict | loss_key -> Tensor
            "wavs",
            "spectrograms",
        ]

    class OutputBuildData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",
            "losses",
            "wavs",
            "spectrograms",
        ]

    class InputNonTensorData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",
            "predictions_onehot",
            "labels_onehot",
        ]

    class OutputNonTensorData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",
            "predictions_onehot",
            "labels_onehot",
            "predictions",
            "labels",
        ]

    @classmethod
    def _process_non_tensor_data(cls, data):
        predictions = data.predictions_onehot.argmax(axis=-1)
        labels = data.labels_onehot.argmax(axis=-1)

        return cls.OutputNonTensorData({
            "dataset_split_name": data.dataset_split_name,
            "label_names": data.label_names,
            "predictions_onehot": data.predictions_onehot,
            "labels_onehot": data.labels_onehot,
            "predictions": predictions,
            "labels": labels,
        })


class MultiLabelAudioDataParser(MetricDataParserBase):
    class InputBuildData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",
            "losses",  # Dict | loss_key -> Tensor
            "wavs",
            "spectrograms",
        ]

    class OutputBuildData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",
            "losses",
            "wavs",
            "spectrograms",
        ]

    class InputNonTensorData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",
            "predictions_multihot",
            "labels_multihot",

            "sigmoid_threshold",
        ]

    class OutputNonTensorData(DataStructure):
        _keys = [
            "dataset_split_name",
            "label_names",
            "predictions_multihot",
            "labels_multihot",
        ]

    @classmethod
    def _process_non_tensor_data(cls, data):
        predictions_multihot_threshold = np.array(data.predictions_multihot > data.sigmoid_threshold, dtype=np.uint8)

        return cls.OutputNonTensorData({
            "dataset_split_name": data.dataset_split_name,
            "label_names": data.label_names,
            "predictions_multihot": predictions_multihot_threshold,
            "labels_multihot": data.labels_multihot,
        })
