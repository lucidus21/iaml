footer: M1505.001300_001 2018 가을 Team5 , Project 1
autoscale: true

# Music Genre Classification
### 기계학습 산업응용, 2018 가을
### 2018-11-04 (Sunday)

| Team5 Members |||
| --- | --- | --- |
| 2011-11949 신범준 | 2011-11927 임능환 | 2012-11842 김진홍 |

-----

# 구현 리스트

- random-crop
- multi-crop evaluation
- multi-task?

----

# 실험 정리

https://docs.google.com/spreadsheets/d/1SQ9z5u_sSIKjT1hL4czWY8wDaqko-MH2zt40hv0bXDw/edit?usp=sharing


-----

# Log

## 2018-11-04 15:04:20 (Sunday)

```
ubuntu@ip-172-31-45-68 /home/ubuntu/dev/iaml/data/music_dataset_hit
❯❯❯ for d in training/*; do echo $d; ls $d | wc -l; done;
training/High
1110
training/Low
1173
training/Medium
1054
training/Very-High
1548
training/Very-Low
1513

ubuntu@ip-172-31-45-68 /home/ubuntu/dev/iaml/data/music_dataset_hit
❯❯❯ for d in validation/*; do echo $d; ls $d | wc -l; done;
validation/High
141
validation/Low
157
validation/Medium
141
validation/Very-High
196
validation/Very-Low
165
```


## TODO

- 실험 커맨드들을 쭉 debug 모드로 관리하고 나중에 production 넣어서 처음부터 다시 재현해가면서 돌리기

-----

Readlist

- https://r2rt.com/recurrent-neural-networks-in-tensorflow-ii.html


Question List

- MultiCell 인 경우에 state_size 가 input 의 state_size와 같아야했음.
- 아래 코드에서 cell * 2 인데도 불구하고 파라미터 사이즈가 늘어나는것처럼 보이지가 않음

```
cell = eval(f"tf.nn.rnn_cell.{args.v3_cell_type}")(args.v3_hidden_state, state_is_tuple=True)
cell = tf.nn.rnn_cell.DropoutWrapper(cell, input_keep_prob=args.v3_keep_prob, output_keep_prob=args.v3_keep_prob)
cell = tf.nn.rnn_cell.MultiRNNCell([cell] * 2, state_is_tuple=True)
outputs, states = tf.nn.dynamic_rnn(cell, inputs, dtype=tf.float32)
```

- bidirectional 은 https://stackoverflow.com/questions/46189318/how-to-use-multilayered-bidirectional-lstm-in-tensorflow 이거 보고 구현했는데, cell을 2개를 쓰네. 맞는걸까?
- CNN으로 acc 0.34 까지 가네.
- https://github.com/tensorflow/tensorflow/issues/21929 을 보니 원래 개느리고 해결이 안되나봄.

- Update height/width to [1665, 64] 로 돌렸는데 왜 커널 사이즈가 아래와 같을까?

```
 rnn/gru_cell/gates/kernel:0 : [164, 200], 32800 ... 32800
 rnn/gru_cell/gates/bias:0 : [200], 200 ... 33000
 rnn/gru_cell/candidate/kernel:0 : [164, 100], 16400 ... 49400
 rnn/gru_cell/candidate/bias:0 : [100], 100 ... 49500
 fully_connected/weights:0 : [100, 1], 100 ... 49600
 fully_connected/biases:0 : [1], 1 ... 49601
 fully_connected_1/weights:0 : [100, 8], 800 ... 50401
 fully_connected_1/biases:0 : [8], 8 ... 50409
```

