import subprocess
import pickle
import shutil
import time
import os
from typing import Dict
from itertools import groupby
from functools import reduce
from pathlib import Path
from operator import mul

import tensorflow as tf
import pandas as pd
import numpy as np
import deprecation
from termcolor import colored
from tensorflow.contrib.training import checkpoints_iterator
from common.utils import get_logger
from common.utils import wait
from common.utils import format_timespan
from tensorflow.python.profiler import option_builder
from tensorflow.python.framework import graph_util
from PIL import Image

import const
from common.utils import reconstruct_argv


def get_variables_to_train(trainable_scopes, logger):
    """Returns a list of variables to train.
    Returns:
    A list of variables to train by the optimizer.
    """
    if trainable_scopes is None or trainable_scopes == "":
        return tf.trainable_variables()
    else:
        scopes = [scope.strip() for scope in trainable_scopes.split(",")]

    variables_to_train = []
    for scope in scopes:
        variables = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope)
        variables_to_train.extend(variables)

    for var in variables_to_train:
        logger.info("vars to train > {}".format(var.name))

    return variables_to_train


def show_models(logger):
    trainable_vars = tf.trainable_variables()
    total_params = 0
    for v in trainable_vars:
        count_params = reduce(mul, v.get_shape().as_list(), 1)
        total_params += count_params
        logger.info(">>    %s : %s, %d ... %d" % (v.name, v.get_shape().as_list(), count_params, total_params))
    logger.info("Total Variables: %d , Total Params: %d" % (len(trainable_vars), total_params))
    return total_params


def add_batch_norm_debugging_ops(train_dataset_name, valid_dataset_names, summary_string_template, first_n, logger):
    # Verbose print and summaries by dataset and top scope
    scopes = np.unique([
        v.name.split("/")[0] for v in tf.contrib.framework.get_variables() if len(v.name.split("/")) > 1
    ])
    batch_norm_suffixes = ["moving_variance", "moving_mean", "beta", "gamma"]
    for suffix in batch_norm_suffixes:
        for scope in scopes:
            variables = tf.contrib.framework.get_variables(scope=scope, suffix=suffix)
            if len(variables) < 1:
                logger.info("There are no BatchNorm layer so no summaries and prints: {}, {}".format(
                    scope, suffix))
                continue
            value = tf.reduce_mean(tf.concat(variables, 0))
            name = "BatchNorm/{}/{}/{}/mean".format(train_dataset_name, scope, suffix)
            value = tf.Print(value, [value], "{}: ".format(name), first_n=first_n)
            tf.summary.scalar(name, value)
            for dataset_name in valid_dataset_names:
                name = "BatchNorm/{}/{}/{}/mean".format(dataset_name, scope, suffix)
                value = tf.Print(value, [value], "{}: ".format(name), first_n=first_n)
                tf.summary.scalar(name, value, collections=[summary_string_template.format(dataset_name)])


def add_mean_of_layer_summaries(logger):
    # Print out each layer's mean of variables
    n = 2
    topn_levels = np.unique(["/".join(v.name.split("/")[:n]) for v in tf.contrib.framework.get_variables()])
    suffixes = [
        "weights",
        "biases",
        "BatchNorm/beta",
        "BatchNorm/gamma",
        "BatchNorm/moving_mean",
        "BatchNorm/moving_variance"
    ]
    for scope in topn_levels:
        for suffix in suffixes:
            variables = tf.contrib.framework.get_variables(scope=scope, suffix=suffix)
            if len(variables) < 1:
                logger.info("There are no variables: {}/{}".format(scope, suffix))
                continue
            value = tf.reduce_mean([tf.reduce_mean(variable) for variable in variables])
            tf.summary.scalar("mean/{}/{}".format(scope, suffix), value)


def get_gradient_multiplier(rebirth_scopes):
    emphasize_rebirth_layer = dict()
    for var in tf.trainable_variables():
        is_checked = False
        for checkpoint_exclude_scope in rebirth_scopes.split(","):
            if not is_checked and var.name.startswith(checkpoint_exclude_scope):
                emphasize_rebirth_layer[var] = 1
                is_checked = True
        if not is_checked:
            emphasize_rebirth_layer[var] = 0.1
    return emphasize_rebirth_layer


def ckpt_iterator(checkpoint_dir, min_interval_secs=0, timeout=None, timeout_fn=None, logger=None):
    for ckpt_path in checkpoints_iterator(checkpoint_dir, min_interval_secs, timeout, timeout_fn):
        yield ckpt_path


class BestKeeper(object):
    def __init__(
        self,
        metric_with_modes,
        dataset_name,
        directory,
        logger=None,
        epsilon=0.00005,
        score_file="scores.tsv",
        metric_best: Dict={},
    ):
        """Keep best model's checkpoint by each datasets & metrics

        Args:
            metric_with_modes: Dict, metric_name: mode
                if mode is 'min', then it means that minimum value is best, for example loss(MSE, MAE)
                if mode is 'max', then it means that maximum value is best, for example Accuracy, Precision, Recall
            dataset_name: str, dataset name on which metric be will be calculated
            directory: directory path for saving best model
            epsilon: float, threshold for measuring the new optimum, to only focus on significant changes.
                Because sometimes early-stopping gives better generalization results
        """
        if logger is not None:
            self.log = logger
        else:
            self.log = get_logger("BestKeeper")

        self.score_file = score_file
        self.metric_best = metric_best

        self.log.info(colored(f"Initialize BestKeeper: Monitor {dataset_name} & Save to {directory}",
                              "yellow", attrs=["underline"]))
        self.log.info(f"{metric_with_modes}")

        self.x_better_than_y = {}
        self.directory = Path(directory)
        self.output_temp_dir = self.directory / f"{dataset_name}_best_keeper_temp"

        for metric_name, mode in metric_with_modes.items():
            if mode == "min":
                self.metric_best[metric_name] = self.load_metric_from_scores_tsv(
                    directory / dataset_name / metric_name / score_file,
                    metric_name,
                    np.inf,
                )
                self.x_better_than_y[metric_name] = lambda x, y: np.less(x, y - epsilon)
            elif mode == "max":
                self.metric_best[metric_name] = self.load_metric_from_scores_tsv(
                    directory / dataset_name / metric_name / score_file,
                    metric_name,
                    -np.inf,
                )
                self.x_better_than_y[metric_name] = lambda x, y: np.greater(x, y + epsilon)
            else:
                raise ValueError(f"Unsupported mode : {mode}")

    def load_metric_from_scores_tsv(
        self,
        full_path: Path,
        metric_name: str,
        default_value: float,
    ) -> float:
        def parse_scores(s: str):
            if len(s) > 0:
                return float(s)
            else:
                return default_value

        if full_path.exists():
            with open(full_path, "r") as f:
                header = f.readline().strip().split("\t")
                values = list(map(parse_scores, f.readline().strip().split("\t")))
                metric_index = header.index(metric_name)

            return values[metric_index]
        else:
            return default_value

    def monitor(self, dataset_name, eval_scores):
        metrics_keep = {}
        is_keep = False
        for metric_name, score in self.metric_best.items():
            score = eval_scores[metric_name]
            if self.x_better_than_y[metric_name](score, self.metric_best[metric_name]):
                old_score = self.metric_best[metric_name]
                self.metric_best[metric_name] = score
                metrics_keep[metric_name] = True
                is_keep = True
                self.log.info(colored("[KeepBest] {} {:.6f} -> {:.6f}, so keep it!".format(
                    metric_name, old_score, score), "blue", attrs=["underline"]))
            else:
                metrics_keep[metric_name] = False
        return is_keep, metrics_keep

    def save_best(self, dataset_name, metrics_keep, ckpt_glob):
        for metric_name, is_keep in metrics_keep.items():
            if is_keep:
                keep_path = self.directory / Path(dataset_name) / Path(metric_name)
                self.keep_checkpoint(keep_path, ckpt_glob)
                self.keep_converted_files(keep_path)

    def save_scores(self, dataset_name, metrics_keep, eval_scores, meta_info=None):
        eval_scores_with_meta = eval_scores.copy()
        if meta_info is not None:
            eval_scores_with_meta.update(meta_info)

        for metric_name, is_keep in metrics_keep.items():
            if is_keep:
                keep_path = self.directory / Path(dataset_name) / Path(metric_name)
                if not keep_path.exists():
                    keep_path.mkdir(parents=True)
                df = pd.DataFrame(pd.Series(eval_scores_with_meta)).sort_index().transpose()
                df.to_csv(keep_path / self.score_file, sep="\t", index=False, float_format="%.5f")

    @deprecation.deprecated(details="DO NOT USE. It will be removed at further refactoring.")
    def save_images(self, dataset_name, images):
        num_images = images[0].shape[0]
        h, w = images[0].shape[1:3]
        keep_path = self.directory / dataset_name / "images"
        if not keep_path.exists():
            keep_path.mkdir(parents=True)

        for nidx in range(num_images):
            _images = [Image.fromarray(img[nidx]) for img in images]

            merged_image = Image.new("RGB", (h * ((len(_images)-1) // 3 + 1), w * 3))
            for i, img in enumerate(_images):
                row = i // 3
                col = i % 3

                merged_image.paste(img, (row*h, col*w, (row+1)*h, (col+1)*w))
            merged_image.save(keep_path / f"img_{nidx}.jpg")

    def remove_old_best(self, dataset_name, metrics_keep):
        for metric_name, is_keep in metrics_keep.items():
            if is_keep:
                keep_path = self.directory / Path(dataset_name) / Path(metric_name)
                # Remove old directory to save space
                if keep_path.exists():
                    shutil.rmtree(str(keep_path))
                keep_path.mkdir(parents=True)

    def keep_checkpoint(self, keep_path, ckpt_glob):
        if not isinstance(keep_path, Path):
            keep_path = Path(keep_path)

        # .data-00000-of-00001, .meta, .index
        for ckpt_path in ckpt_glob.parent.glob(ckpt_glob.name):
            shutil.copy(str(ckpt_path), str(keep_path))
            """
            self.log.info(colored(
                "[KeepBest] copy {} -> {}".format(ckpt_path, keep_path), "blue", attrs=["underline"])
            )
            """

    def keep_converted_files(self, keep_path):
        if not isinstance(keep_path, Path):
            keep_path = Path(keep_path)

        for path in self.output_temp_dir.glob("*"):
            if path.is_dir():
                shutil.copytree(str(path), str(keep_path / path.name))
            else:
                shutil.copy(str(path), str(keep_path / path.name))

    def remove_temp_dir(self):
        if self.output_temp_dir.exists():
            shutil.rmtree(str(self.output_temp_dir))


def freeze_graph(input_checkpoint, output_node_names, output_graph):
    """https://blog.metaflow.fr/tensorflow-how-to-freeze-a-model-and-serve-it-with-a-python-api-d4f3596b3adc
    Args:
        input_checkpoint: str, without extension like .data, .meta, .index
        output_node_names: list of str,
            Before exporting our graph, we need to precise what is our output node
            This is how TF decides what part of the Graph he has to keep and what part it can dump
            NOTE: this variable is plural, because you can have multiple output nodes
        output_graph: str, full path of our freezed graph

    """
    # We retrieve our checkpoint fullpath
    # checkpoint = tf.train.get_checkpoint_state(model_folder)
    # input_checkpoint = checkpoint.model_checkpoint_path

    output_node_names = output_node_names

    # We clear devices to allow TensorFlow to control on which device it will load operations
    clear_devices = True

    # We import the meta graph and retrieve a Saver
    saver = tf.train.import_meta_graph(input_checkpoint + ".meta", clear_devices=clear_devices)

    # We retrieve the protobuf graph definition
    graph = tf.get_default_graph()
    input_graph_def = graph.as_graph_def()

    # We start a session and restore the graph weights
    with tf.Session() as sess:
        saver.restore(sess, input_checkpoint)

        # We use a built-in TF helper to export variables to constants
        output_graph_def = graph_util.convert_variables_to_constants(
            sess,  # The session is used to retrieve the weights
            input_graph_def,  # The graph_def is used to retrieve the nodes
            output_node_names.split(",")  # The output node names are used to select the usefull nodes
        )

        # Finally we serialize and dump the output graph to the filesystem
        with tf.gfile.GFile(output_graph, "wb") as f:
            f.write(output_graph_def.SerializeToString())
        print("%d ops in the final graph." % len(output_graph_def.node))


def call_subprocess(cmd, logger, timeout=60):
    if cmd.split(" ")[0] == "python":
        cmdfile = cmd.split(" ")[1]
    else:
        cmdfile = cmd.split(" ")[0]

    if not Path(cmdfile).exists():
        logger.info(colored("Command not exists: {}".format(cmd.split(" ")[0]), "red"))
        return

    logger.info(colored("Subprocess Call!\n{}".format(cmd), "yellow", attrs=["bold"]))
    try:
        p = subprocess.run(cmd, timeout=timeout, shell=True)
    except subprocess.TimeoutExpired as e:
        logger.error(colored("{}".format(e), "red"))
    else:
        if p.returncode != 0:
            logger.info(colored("Returncode {}".format(p.returncode), "red"))
            return


def args_var_tostring(args, name, is_bool):
    if hasattr(args, name) and getattr(args, name) is not None:
        value = getattr(args, name)
        if is_bool:
            if value:
                return f"--{name}"
            else:
                return f"--no-{name}"
        else:
            return f"--{name} {value}"
    else:
        return ""


def print_graph(graph, logger=None):
    if logger is None:
        printer = print
    else:
        printer = logger.info
    for op in graph.get_operations():
        printer(op.name)


def calc_sparsity(tensor):
    """Calculate sparsity ratio of tensor,
    if sparsity is 1, then all values of tesnor is zero, it may means that there is a problem in training
    """
    return tf.nn.zero_fraction(tensor)


def add_sparsity_summaries(tensor_dict, prefix_tag):
    for key, value in tensor_dict.items():
        tf.summary.scalar(prefix_tag + key, calc_sparsity(value))


def add_histogram_summaries(tensor_dict, prefix_tag):
    for key, value in tensor_dict.items():
        tf.summary.histogram(prefix_tag + key, value)


def dump_to_numpy_weights(session, path):
    weights = {}
    for tf_var in tf.contrib.framework.get_variables():
        weights[tf_var.name] = session.run(tf_var)
    pickle.dump(weights, open(path, "wb"))


def resolve_checkpoint_path(checkpoint_path, log, is_training):
    if checkpoint_path is not None and Path(checkpoint_path).is_dir():
        old_ckpt_path = checkpoint_path
        checkpoint_path = tf.train.latest_checkpoint(old_ckpt_path)
        if not is_training:
            def stop_checker():
                return (tf.train.latest_checkpoint(old_ckpt_path) is not None)
            wait("There are no checkpoint file yet", stop_checker)  # wait until checkpoint occurs
        checkpoint_path = tf.train.latest_checkpoint(old_ckpt_path)
        log.info(colored(
            "self.args.checkpoint_path updated: {} -> {}".format(old_ckpt_path, checkpoint_path),
            "yellow", attrs=["bold"]))
    else:
        log.info(colored("checkpoint_path is {}".format(checkpoint_path), "yellow", attrs=["bold"]))

    return checkpoint_path


def get_global_step_from_checkpoint(checkpoint_path):
    """It is assumed that `checkpoint_path` is path to checkpoint file, not path to directory
    with checkpoint files.
    In case checkpoint path is not defined, 0 is returned."""
    if checkpoint_path is None or checkpoint_path == "":
        return 0
    else:
        if "-" in Path(checkpoint_path).stem:
            return int(Path(checkpoint_path).stem.split("-")[-1])
        else:
            return 0


def load_from_caffe_array(nparray_path):
    np_vars_to_value = {}
    weights_from_caffe = np.load(nparray_path, encoding="bytes").item()
    for node, components in weights_from_caffe.items():
        for varname, value in components.items():
            key = "{}/{}".format(node, varname.decode("utf-8"))
            np_vars_to_value[key] = value
    return np_vars_to_value
