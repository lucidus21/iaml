from multiprocessing import Pool
from pathlib import Path
from typing import Callable
from typing import List
from typing import Tuple
import copy
import math

from tensorflow.contrib.framework.python.ops import audio_ops as contrib_audio
import tensorflow as tf
import numpy as np
import pandas as pd
import dask.dataframe as dd

import common.utils as utils
from common.utils import smart_log as smrtlg
from datasets.data_wrapper_base import DataWrapperBase
from datasets.augmentation_factory import get_audio_augmentation_fn


def _next_power_of_two(x):
    """Calculates the smallest enclosing power of two for an input.
    Args:
    x: Positive float or integer number.
    Returns:
    Next largest power of two integer.
    """
    return 1 if x == 0 else 2**(int(x) - 1).bit_length()


class AudioDataWrapper(DataWrapperBase):
    def __init__(
        self, args, session, dataset_split_name, is_training, name: str="AudioDataWrapper"
    ):
        super().__init__(args, dataset_split_name, is_training, name)
        self.setup()

        self.setup_dataset((self.filenames_placeholder, self.labels_placeholder, self.genre_idxs_placeholder))
        self.setup_iterator(
            session,
            (self.filenames_placeholder, self.labels_placeholder, self.genre_idxs_placeholder),
            (self.filenames, self.labels, self.genre_idxs),
        )

    def set_fileformat(self, filenames):
        file_formats = set(Path(fn).suffix for fn in filenames)
        assert len(file_formats) == 1
        self.file_format = file_formats.pop()[1:]  # decode_audio receives without .(dot)

    @property
    def num_samples(self):
        return self._num_samples

    def augment_audio(self, audio_binary, args, desired_samples, file_format, sample_rate):
        aug_fn = get_audio_augmentation_fn(args.augmentation_method)
        return aug_fn(audio_binary, desired_samples, file_format, sample_rate)

    def _parse_function(self, filename, label, genre_idx):
        """
        `filename` tensor holds full path of image
        `label` tensor is holding index of class to which it belongs to
        """
        self.desired_samples = int(self.args.sample_rate * self.args.clip_duration_ms / 1000)
        self.window_size_samples = int(self.args.sample_rate * self.args.window_size_ms / 1000)
        self.window_stride_samples = int(self.args.sample_rate * self.args.window_stride_ms / 1000)

        augmented_audio = self.augment_audio(
            tf.read_file(filename),
            self.args,
            self.desired_samples,
            self.file_format,
            self.args.sample_rate
        )

        magnitude_spectrograms = contrib_audio.audio_spectrogram(
            augmented_audio,
            window_size=self.window_size_samples,
            stride=self.window_stride_samples,
            # If magnitude_squared = True(power_spectrograms)#, tf.real(stfts * tf.conj(stfts))
            # If magnitude_squared = False(magnitude_spectrograms), tf.abs(stfts)
            magnitude_squared=False,
        )
        label_parsed = self.parse_label(label)
        genre_parsed = genre_idx  # just use embedding_lookup
        log_offset = 1e-6
        log_magnitude_spectrograms = tf.log(magnitude_spectrograms + log_offset)

        num_spectrogram_bins = magnitude_spectrograms.shape[-1].value
        linear_to_mel_weight_matrix = tf.contrib.signal.linear_to_mel_weight_matrix(
            self.args.num_mel_bins,
            num_spectrogram_bins,
            self.args.sample_rate,
            self.args.lower_edge_hertz,
            self.args.upper_edge_hertz
        )

        mel_spectrograms = tf.tensordot(magnitude_spectrograms, linear_to_mel_weight_matrix, 1)
        mel_spectrograms.set_shape(
            magnitude_spectrograms.shape[:-1].concatenate(linear_to_mel_weight_matrix.shape[-1:])
        )

        log_offset = 1e-6
        log_mel_spectrograms = tf.log(mel_spectrograms + log_offset)

        # Keep the first `num_mfccs` MFCCs.
        mfccs = tf.contrib.signal.mfccs_from_log_mel_spectrograms(log_mel_spectrograms)[..., :self.args.num_mfccs]

        fingerprint = None
        if self.args.audio_preprocess == "magnitude_spectrogram":
            fingerprint = tf.squeeze(magnitude_spectrograms, axis=0)
        elif self.args.audio_preprocess == "log_magnitude_spectrogram":
            fingerprint = tf.squeeze(log_magnitude_spectrograms, axis=0)
        elif self.args.audio_preprocess == "mel_spectrogram":
            fingerprint = tf.squeeze(mel_spectrograms, axis=0)
        elif self.args.audio_preprocess == "log_mel_spectrogram":
            fingerprint = tf.squeeze(log_mel_spectrograms, axis=0)
        elif self.args.audio_preprocess == "mfcc":
            fingerprint = tf.squeeze(mfccs, axis=0)
        else:
            raise ValueError(f"Undefined self.args.audio_preprocess: {self.args.audio_preprocess}")

        self.log.info(f"Update height/width to {fingerprint.shape.as_list()}")
        self.args.height = fingerprint.shape.as_list()[0]
        self.args.width = fingerprint.shape.as_list()[1]

        return augmented_audio, tf.squeeze(log_mel_spectrograms, axis=0), fingerprint, label_parsed, genre_parsed

    def update_mean_std_values(self):
        with tf.Session(config=tf.ConfigProto(device_count={"GPU": 0})) as session:
            dataset = self._create_itself_with_custom_batch_size(session)
            _, _, inputs, _ = dataset.get_input_and_output_op()
            self.args.mean_values, self.args.std_values = self._calc_mean_std(session, dataset, inputs)
        with utils.format_text("yellow", ["underline"]) as fmt:
            self.log.info(fmt((f"Update self.args.mean_values / self.args.std_values to "
                               f"{self.args.mean_values} / {self.args.std_values}")))

    @staticmethod
    def add_arguments(parser):
        g = parser.add_argument_group("(AudioDataWrapper) Arguments for Audio DataWrapper")
        g.add_argument(
            "--audio_preprocess",
            type=str,
            choices=["magnitude_spectrogram", "log_magnitude_spectrogram",
                     "mel_spectrogram", "log_mel_spectrogram",
                     "mfcc"],
            help="Expected sample rate of the wavs",)
        g.add_argument(
            "--sample_rate",
            type=int,
            default=16000,
            help="Expected sample rate of the wavs",)
        g.add_argument(
            "--clip_duration_ms",
            type=int,
            default=1000,
            help=("Expected duration in milliseconds of the wavs",
                  "the audio will be cropped or padded with zeroes based on this value"),)
        g.add_argument(
            "--window_size_ms",
            type=float,
            default=30.0,
            help="How long each spectrogram timeslice is.",)
        g.add_argument(
            "--window_stride_ms",
            type=float,
            default=30.0,
            help="How far to move in time between spectogram timeslices.",)

        # {{ -- Arguments for log-mel spectrograms
        # Default values are coming from tensorflow official tutorial
        g.add_argument("--lower_edge_hertz", type=float, default=80.0)
        g.add_argument("--upper_edge_hertz", type=float, default=7600.0)
        g.add_argument("--num_mel_bins", type=int, default=64)
        # Arguments for log-mel spectrograms -- }}

        # {{ -- Arguments for mfcc
        # Google speech_commands sample uses num_mfccs=40 as a default value
        # Official signal processing tutorial uses num_mfccs=13 as a default value
        g.add_argument("--num_mfccs", type=int, default=40)
        # Arguments for mfcc -- }}

        # TODO mean normalization of mfcc/logmel spectrograms
        g.add_argument("--input_file", default=None, type=str)
        g.add_argument("--description_file", default=None, type=str)
        g.add_argument("--num_partitions", default=2, type=int,
                       help=("Number of partition to which is input csv file split"
                             "and parallely processed"))

    @property
    def num_samples(self):
        return self._num_samples


class SingleLabelAudioDataWrapper(AudioDataWrapper):
    def parse_label(self, label):
        return tf.sparse_to_dense(sparse_indices=tf.cast(label, tf.int32),
                                  sparse_values=tf.ones([1], tf.float32),
                                  output_shape=[self.num_labels],
                                  validate_indices=False)

    def setup_popularity_logits(self, file_pathes):
        df = pd.read_csv("../etc/track_metadata.csv")
        df["track_listens"] = df.track_listens.apply(lambda x: x.replace(" ", "-"))
        track_ids = [int(p.split("/")[-1].split(".")[0]) for p in file_pathes]
        genres = df.set_index("track_id").loc[track_ids]["track_genre"].values
        self.hits = [v.replace(" ", "-") for v in sorted(list(df.track_listens.unique()))]
        self.genre_categories = [v for v in sorted(list(df.track_genre.unique()))]
        training = df[df.split == "training"].copy()
        popularities = {}
        for genre in self.genre_categories:
            popularity = []
            for hit in self.hits:
                popularity.append(training[(training.track_genre == genre) & (training.track_listens == hit)].shape[0])
            popularities[genre] = [p / sum(popularity) for p in popularity]
        genre_idxs = [self.genre_categories.index(g.replace(" ", "-")) for g in genres]
        popularities_values = [popularities[g] for g in genres]
        popularity_matrix = np.array([popularities[genre] for genre in self.genre_categories])
        return genre_idxs, popularity_matrix

    def setup(self):
        dataset_paths = self.get_all_dataset_paths()
        self.label_names, self.num_labels = self.get_label_names(dataset_paths)
        self.filenames, self.labels = self.get_filenames_labels(dataset_paths)

        # For hit prediction
        self.genre_idxs, self.popularity_matrix = self.setup_popularity_logits(self.filenames)

        self.set_fileformat(self.filenames)
        self._num_samples = self.count_samples(self.filenames)

        self.filenames_placeholder = tf.placeholder(tf.string, len(self.filenames))
        self.labels_placeholder = tf.placeholder(tf.int32, len(self.labels))
        self.genre_idxs_placeholder = tf.placeholder(tf.int32, len(self.genre_idxs))


# Refactor! Remove all duplicated codes with MultiLabelDataWrapper
class MultiLabelAudioDataWrapper(AudioDataWrapper):
    def __init__(
        self,
        args,
        session: tf.Session,
        dataset_split_name,
        is_training: bool,
        max_class_num: int,
        name: str="MultiLabelDataWrapper",
    ):
        """
        `input_file` is csv file with two columns: track_id and Labels.
        * track_id is name of image without path but with extension.
          Full path to image files is created from `self.args.dataset_path` and
          `self.args.dataset_split_name`.
        * Labels column consist of strings of integer numbers (labels) separated by space.

        `description_file` is csv files with tree columns: LabelName, ID, and optionally Description.
        * LabelName column contains unique strings
        * ID column holds integer values between 0 and NUM_LABELS-1
        * Description column holds string values.

        `max_class_num` represents maximum number of labels per object. It is used for padding
        labels before generating multihot encoding.
        """
        self.input_file = args.input_file
        self.description_file = args.description_file
        self.max_class_num = max_class_num
        super().__init__(args, session, dataset_split_name, is_training, name)

    def setup(self):
        self.filenames, self.labels, self._num_samples = self.process_input_file(self.input_file)
        self.label_names, self.num_labels = self.process_description_file(self.description_file)

        self.set_fileformat(self.filenames)

        self.filenames_placeholder = tf.placeholder(self.filenames.dtype, self.filenames.shape)
        self.labels_placeholder = tf.placeholder(self.labels.dtype, self.labels.shape)

    @property
    def num_samples(self):
        return self._num_samples

    @staticmethod
    def process_multilabel_dataset(
        df: pd.DataFrame,
        dataset_path: str,
        max_class_num: int,
    ):
        df["Labels"] = df["Labels"].map(lambda x: [int(i) for i in x.split(" ")] if not pd.isnull(x) else [])
        labels = list(df.Labels)
        num_samples = len(df)

        # create labels matrix padded to the `max_class_num` size
        multi_labels = np.empty((num_samples, max_class_num), dtype=np.int32)
        multi_labels.fill(-1)
        lens = list(map(lambda l: len(l), labels))
        mask = np.arange(max_class_num) < np.array(lens)[:, None]
        multi_labels[mask] = np.concatenate(labels)

        # create full paths to images
        df["track_id"] = str(dataset_path) + "/" + df["track_id"].astype(str)

        return df, multi_labels

    def parallelize_input_file_processing(self,
                                          fun: Callable,
                                          df: pd.DataFrame,
                                          num_partitions: int = 4,
                                          num_cores: int = 4):
        def get_partial_result(result, length, pos: int):
            return [result[idx][pos] for idx in range(length)]

        _input = zip(np.array_split(df, num_partitions),
                     [str(self.dataset_path_with_split_name)] * num_partitions,
                     [self.max_class_num] * num_partitions)

        with Pool(num_cores) as pool:
            result = pool.starmap(fun, _input)

        df = pd.concat(get_partial_result(result, num_partitions, 0))
        multi_labels = np.vstack(get_partial_result(result, num_partitions, 1))
        filenames = np.array(list(df.track_id))

        return filenames, multi_labels

    def process_input_file(self, input_file):
        with smrtlg(self.log, "Reading csv"):
            df = dd.read_csv(input_file).compute()
            num_samples = len(df)

        with smrtlg(self.log, "Loading"):
            assert self.args.num_partitions > 1, "https://hpcnt.slack.com/archives/G2Z4HMNR3/p1530084782000403"
            filenames, multi_labels = self.parallelize_input_file_processing(
                self.process_multilabel_dataset, df,
                num_partitions=self.args.num_partitions, num_cores=self.args.num_partitions)

        return filenames, multi_labels, num_samples

    def process_description_file(self, description_file):
        df = pd.read_csv(description_file)
        label_names = list(df.LabelName)
        num_labels = len(label_names)

        return label_names, num_labels

    def parse_label(self, label):
        indexes = tf.where(tf.not_equal(label,
                                        tf.constant(-1, dtype=tf.int32)))
        num_data = tf.shape(indexes)[0]

        label = tf.slice(label,
                         tf.cast([0], dtype=tf.int64),
                         tf.cast([num_data], dtype=tf.int64))

        # `tf.sparse_to_dense` implicitly set argument `validate_indices=True`.
        # If sparse_indices are not sorted in lexicographic order or if some indices repeat,
        # error is raised.
        label = tf.sparse_to_dense(sparse_indices=tf.cast(label, tf.int64),
                                   sparse_values=tf.ones([num_data], tf.float32),
                                   output_shape=[self.num_labels],
                                   validate_indices=False)
        # data type of label should be float since the type of logit would be float.
        # https://www.tensorflow.org/api_docs/python/tf/nn/softmax_cross_entropy_with_logits_v2
        return label
