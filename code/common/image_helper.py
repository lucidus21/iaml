import tensorflow as tf
import numpy as np


def sobel_gradient(img):
    """Get image and calculate the result of sobel filter.

    Args:
        imgs: Image Tensor. Either 3-D or 4-D.

    Return:
        A Tensor which concat the result of sobel in both
        horizontally and vertically.
        Therefore, number of the channels is doubled.

    """
    num_channels = img.get_shape().as_list()[-1]

    # load filter which can be reused
    with tf.variable_scope("misc/img_gradient", reuse=tf.AUTO_REUSE):
        filter_x = tf.constant([[-1/8, 0, 1/8],
                                [-2/8, 0, 2/8],
                                [-1/8, 0, 1/8]],
                               name="sobel_x", dtype=tf.float32, shape=[3, 3, 1, 1])
        filter_x = tf.tile(filter_x, [1, 1, num_channels, 1])

        filter_y = tf.constant([[-1/8, -2/8, -1/8],
                                [0, 0, 0],
                                [1/8, 2/8, 1/8]],
                               name="sobel_y", dtype=tf.float32, shape=[3, 3, 1, 1])
        filter_y = tf.tile(filter_y, [1, 1, num_channels, 1])

    # calculate
    grad_x = tf.nn.depthwise_conv2d(img, filter_x,
                                    strides=[1, 1, 1, 1], padding="VALID", name="grad_x")

    grad_y = tf.nn.depthwise_conv2d(img, filter_y,
                                    strides=[1, 1, 1, 1], padding="VALID", name="grad_y")

    grad_xy = tf.concat([grad_x, grad_y], axis=-1)

    return grad_xy


"""
SSIM implementation is taken from official tensorflow 1.9
"""
_SSIM_K1 = 0.01
_SSIM_K2 = 0.03


def _ssim_helper(x, y, reducer, max_val, compensation=1.0):
    r"""Helper function for computing SSIM.
    SSIM estimates covariances with weighted sums.  The default parameters
    use a biased estimate of the covariance:
    Suppose `reducer` is a weighted sum, then the mean estimators are
      \mu_x = \sum_i w_i x_i,
      \mu_y = \sum_i w_i y_i,
    where w_i's are the weighted-sum weights, and covariance estimator is
      cov_{xy} = \sum_i w_i (x_i - \mu_x) (y_i - \mu_y)
    with assumption \sum_i w_i = 1. This covariance estimator is biased, since
      E[cov_{xy}] = (1 - \sum_i w_i ^ 2) Cov(X, Y).
    For SSIM measure with unbiased covariance estimators, pass as `compensation`
    argument (1 - \sum_i w_i ^ 2).
    Arguments:
      x: First set of images.
      y: Second set of images.
      reducer: Function that computes 'local' averages from set of images.
        For non-covolutional version, this is usually tf.reduce_mean(x, [1, 2]),
        and for convolutional version, this is usually tf.nn.avg_pool or
        tf.nn.conv2d with weighted-sum kernel.
      max_val: The dynamic range (i.e., the difference between the maximum
        possible allowed value and the minimum allowed value).
      compensation: Compensation factor. See above.
    Returns:
      A pair containing the luminance measure, and the contrast-structure measure.
    """
    c1 = (_SSIM_K1 * max_val) ** 2
    c2 = (_SSIM_K2 * max_val) ** 2

    # SSIM luminance measure is
    # (2 * mu_x * mu_y + c1) / (mu_x ** 2 + mu_y ** 2 + c1).
    mean0 = reducer(x)
    mean1 = reducer(y)
    num0 = mean0 * mean1 * 2.0
    den0 = tf.square(mean0) + tf.square(mean1)
    luminance = (num0 + c1) / (den0 + c1)

    # SSIM contrast-structure measure is
    #   (2 * cov_{xy} + c2) / (cov_{xx} + cov_{yy} + c2).
    # Note that `reducer` is a weighted sum with weight w_k, \sum_i w_i = 1, then
    #   cov_{xy} = \sum_i w_i (x_i - \mu_x) (y_i - \mu_y)
    #          = \sum_i w_i x_i y_i - (\sum_i w_i x_i) (\sum_j w_j y_j).
    num1 = reducer(x * y) * 2.0
    den1 = reducer(tf.square(x) + tf.square(y))
    c2 *= compensation
    cs = (num1 - num0 + c2) / (den1 - den0 + c2)

    # SSIM score is the product of the luminance and contrast-structure measures.
    return luminance, cs


def _fspecial_gauss(size, sigma):
    """Function to mimic the 'fspecial' gaussian MATLAB function."""
    size = tf.convert_to_tensor(size, tf.int32)
    sigma = tf.convert_to_tensor(sigma)

    coords = tf.cast(tf.range(size), sigma.dtype)
    coords -= tf.cast(size - 1, sigma.dtype) / 2.0

    g = tf.square(coords)
    g *= -0.5 / tf.square(sigma)

    g = tf.reshape(g, shape=[1, -1]) + tf.reshape(g, shape=[-1, 1])
    g = tf.reshape(g, shape=[1, -1])  # For tf.nn.softmax().
    g = tf.nn.softmax(g)
    return tf.reshape(g, shape=[size, size, 1, 1])


def _verify_compatible_image_shapes(img1, img2):
    """Checks if two image tensors are compatible for applying SSIM or PSNR.
    This function checks if two sets of images have ranks at least 3, and if the
    last three dimensions match.
    Args:
      img1: Tensor containing the first image batch.
      img2: Tensor containing the second image batch.
    Returns:
      A tuple containing: the first tensor shape, the second tensor shape, and a
      list of control_flow_ops.Assert() ops implementing the checks.
    Raises:
      ValueError: When static shape check fails.
    """
    shape1 = img1.get_shape().with_rank_at_least(3)
    shape2 = img2.get_shape().with_rank_at_least(3)
    shape1[-3:].assert_is_compatible_with(shape2[-3:])

    if shape1.ndims is not None and shape2.ndims is not None:
        for dim1, dim2 in zip(reversed(shape1[:-3]), reversed(shape2[:-3])):
            if not (dim1 == 1 or dim2 == 1 or dim1.is_compatible_with(dim2)):
                raise ValueError(
                    "Two images are not compatible: %s and %s" % (shape1, shape2))

    # Now assign shape tensors.
    shape1, shape2 = tf.shape_n([img1, img2])

    # TODO(sjhwang): Check if shape1[:-3] and shape2[:-3] are broadcastable.
    checks = []
    checks.append(tf.Assert(
        tf.greater_equal(tf.size(shape1), 3),
        [shape1, shape2], summarize=10))
    checks.append(tf.Assert(
        tf.reduce_all(tf.equal(shape1[-3:], shape2[-3:])),
        [shape1, shape2], summarize=10))
    return shape1, shape2, checks


def _ssim_per_channel(img1, img2, max_val=1.0):
    """Computes SSIM index between img1 and img2 per color channel.
    This function matches the standard SSIM implementation from:
    Wang, Z., Bovik, A. C., Sheikh, H. R., & Simoncelli, E. P. (2004). Image
    quality assessment: from error visibility to structural similarity. IEEE
    transactions on image processing.
    Details:
      - 11x11 Gaussian filter of width 1.5 is used.
      - k1 = 0.01, k2 = 0.03 as in the original paper.
    Args:
      img1: First image batch.
      img2: Second image batch.
      max_val: The dynamic range of the images (i.e., the difference between the
        maximum the and minimum allowed values).
    Returns:
      A pair of tensors containing and channel-wise SSIM and contrast-structure
      values. The shape is [..., channels].
    """
    filter_size = tf.constant(11, dtype=tf.int32)
    filter_sigma = tf.constant(1.5, dtype=img1.dtype)

    shape1, shape2 = tf.shape_n([img1, img2])
    checks = [
        tf.Assert(tf.reduce_all(tf.greater_equal(
            shape1[-3:-1], filter_size)), [shape1, filter_size], summarize=8),
        tf.Assert(tf.reduce_all(tf.greater_equal(
            shape2[-3:-1], filter_size)), [shape2, filter_size], summarize=8)]

    # Enforce the check to run before computation.
    with tf.control_dependencies(checks):
        img1 = tf.identity(img1)

    # TODO(sjhwang): Try to cache kernels and compensation factor.
    kernel = _fspecial_gauss(filter_size, filter_sigma)
    kernel = tf.tile(kernel, multiples=[1, 1, shape1[-1], 1])

    # The correct compensation factor is `1.0 - tf.reduce_sum(tf.square(kernel))`,
    # but to match MATLAB implementation of MS-SSIM, we use 1.0 instead.
    compensation = 1.0

    # TODO(sjhwang): Try FFT.
    # TODO(sjhwang): Gaussian kernel is separable in space. Consider applying
    #   1-by-n and n-by-1 Gaussain filters instead of an n-by-n filter.
    def reducer(x):
        shape = tf.shape(x)
        x = tf.reshape(x, shape=tf.concat([[-1], shape[-3:]], 0))
        y = tf.nn.depthwise_conv2d(x, kernel, strides=[1, 1, 1, 1], padding="VALID")
        return tf.reshape(y, tf.concat([shape[:-3],
                                        tf.shape(y)[1:]], 0))

    luminance, cs = _ssim_helper(img1, img2, reducer, max_val, compensation)

    # Average over the second and the third from the last: height, width.
    axes = tf.constant([-3, -2], dtype=tf.int32)
    ssim_val = tf.reduce_mean(luminance * cs, axes)
    cs = tf.reduce_mean(cs, axes)
    return ssim_val, cs


def ssim(img1, img2, max_val):
    """Computes SSIM index between img1 and img2.
    This function is based on the standard SSIM implementation from:
    Wang, Z., Bovik, A. C., Sheikh, H. R., & Simoncelli, E. P. (2004). Image
    quality assessment: from error visibility to structural similarity. IEEE
    transactions on image processing.
    Note: The true SSIM is only defined on grayscale.  This function does not
    perform any colorspace transform.  (If input is already YUV, then it will
    compute YUV SSIM average.)
    Details:
      - 11x11 Gaussian filter of width 1.5 is used.
      - k1 = 0.01, k2 = 0.03 as in the original paper.
    The image sizes must be at least 11x11 because of the filter size.
    Example:
    ```python
        # Read images from file.
        im1 = tf.decode_png('path/to/im1.png')
        im2 = tf.decode_png('path/to/im2.png')
        # Compute SSIM over tf.uint8 Tensors.
        ssim1 = tf.image.ssim(im1, im2, max_val=255)
        # Compute SSIM over tf.float32 Tensors.
        im1 = tf.image.convert_image_dtype(im1, tf.float32)
        im2 = tf.image.convert_image_dtype(im2, tf.float32)
        ssim2 = tf.image.ssim(im1, im2, max_val=1.0)
        # ssim1 and ssim2 both have type tf.float32 and are almost equal.
    ```
    Args:
      img1: First image batch.
      img2: Second image batch.
      max_val: The dynamic range of the images (i.e., the difference between the
        maximum the and minimum allowed values).
    Returns:
      A tensor containing an SSIM value for each image in batch.  Returned SSIM
      values are in range (-1, 1], when pixel values are non-negative. Returns
      a tensor with shape: broadcast(img1.shape[:-3], img2.shape[:-3]).
    """
    _, _, checks = _verify_compatible_image_shapes(img1, img2)
    with tf.control_dependencies(checks):
        img1 = tf.identity(img1)

    # Need to convert the images to float32.  Scale max_val accordingly so that
    # SSIM is computed correctly.
    max_val = tf.cast(max_val, img1.dtype)
    max_val = tf.image.convert_image_dtype(max_val, tf.float32)
    img1 = tf.image.convert_image_dtype(img1, tf.float32)
    img2 = tf.image.convert_image_dtype(img2, tf.float32)
    ssim_per_channel, _ = _ssim_per_channel(img1, img2, max_val)
    # Compute average over color channels.
    return tf.reduce_mean(ssim_per_channel, [-1])
