from pathlib import Path
import argparse
import time
import json
import os

import pandas as pd

import factory.audio_nets as audio_nets
import common.tf_utils as tf_utils
import common.utils as utils
import const
import shutil


# TODO
CONFIG = "config.json"
SCORES = "scores.tsv"
GROUNDTRUTH_COL = "groundtruth"
ENSEMBLE_COL = "ensemble"
INFER_INDEX = "filename"
log = utils.get_logger("Ensemble")


class FakeParser():
    def __init__(self):
        self.arguments = set()
        self.log = utils.get_logger("FakeParser")

    def add_argument(self, argument, **kwargs):
        if not argument.startswith("--no-"):
            self.arguments.add(argument.replace("--", ""))
            self.log.info(f"add_argument: {argument}")

    def set_defaults(self, **kwargs):
        self.log.info(f"set_default: {kwargs}")


def traverse_train_dir(search_dir, best_metric, score_threshold, score_type, dataset_substring, verbose=0):
    train_dirs = []

    def check_score_threshold(score_path, best_metric):
        score = pd.read_csv(score_path, delim_whitespace=True)
        score_col = "/".join(best_metric.split("/")[1:]).rstrip("/")
        score_val = score[score_col].values[0]
        if verbose:
            log.info(f"[check_score_threshold({score_col})] score_val: {score_val} score_threshold: {score_threshold} score_path: {score_path}")
        if score_type == "max":
            return (score_val > score_threshold)
        elif score_type == "min":
            return (score_val < score_threshold)
        else:
            raise ValueError(f"Undefined score_type: {score_type}")

    def check_same_dataset(config_path):
        config = json.load(open(config_path, "r"))
        return dataset_substring in config["dataset_path"]

    for dirpath, dirnames, filenames in os.walk(search_dir, followlinks=True):
        metric_dir = Path(dirpath) / Path(best_metric)
        metric_score_path = Path(dirpath) / Path(best_metric) / SCORES
        config_path = Path(dirpath) / CONFIG
        basic_conditions = [
            config_path.exists(),
            metric_dir.exists(),
            metric_score_path.exists()
        ]
        if all(basic_conditions):
            score_path = Path(dirpath) / Path(best_metric) / SCORES
            advanced_conditions = [
                check_same_dataset(config_path),
                check_score_threshold(score_path, best_metric),
            ]
            if all(advanced_conditions):
                if verbose:
                    log.info(f"Satisfy condition!: {dirpath}")
                train_dirs.append(Path(dirpath))

    for train_dir in train_dirs:
        yield train_dir


def build_checkpoint_and_score_path(train_dir, best_metric):
    best_metric_dir = Path(train_dir) / Path(best_metric)
    ckpt_name = None
    score_name = None
    for f in best_metric_dir.iterdir():
        if f.suffix == ".data-00000-of-00001":
            ckpt_name = f.stem
        if f.suffix == ".tsv":
            score_filename = f.name
    ckpt_path = best_metric_dir / ckpt_name
    score_path = best_metric_dir / score_filename
    return ckpt_path, score_path


def build_groundtruth_df(dataset_path, dataset_split_name):
    rows = []
    for label_dir in (Path(dataset_path) / Path(dataset_split_name)).iterdir():
        for file_path in label_dir.iterdir():
            rows.append({INFER_INDEX: file_path.name, GROUNDTRUTH_COL: label_dir.name})
    return pd.DataFrame(rows).set_index(INFER_INDEX)


def build_cli_arguments(
    train_dir,
    preserved_keys,
    unknown_args,
    output_dir,
    output_type,
    ckpt_path,
    dataset_path,
    dataset_split_name,
    batch_size,
):
    config = json.load(open(Path(train_dir) / CONFIG, "r"))

    # preserved_keys from command line
    set_preserved_keys = set(preserved_keys)

    # Model specific arguments
    fake_parser = FakeParser()
    model_name = config["model"]
    eval(f"audio_nets.{model_name}").add_arguments(fake_parser)
    set_model_keys = fake_parser.arguments

    # Initialize cli_args
    cli_args = ""

    # Build dataset_path, dataset_split_name
    cli_args += f"--batch_size {batch_size} "
    cli_args += f"--dataset_path {dataset_path} "
    cli_args += f"--dataset_split_name {dataset_split_name} "

    # Build checkpoint_path
    cli_args += f"--checkpoint_path {ckpt_path} "

    # Build inference related arguments
    info = str(ckpt_path).replace("/", "_").replace("..", "__")[-256:]
    infer_output_fullpath = Path(output_dir) / Path(info) / Path(f"{output_type}.csv")
    if not infer_output_fullpath.parent.exists():
        infer_output_fullpath.parent.mkdir(parents=True)
        log.info(f"Make directory: {infer_output_fullpath.parent}")
    cli_args += f"--inference_output_fullpath {infer_output_fullpath} "
    cli_args += f"--inference_output_dirname {output_dir} "
    cli_args += f"--inference_output {output_type} "
    cli_args += f"--inference "

    # Build up cli arguments
    for key, val in config.items():
        if key in set_preserved_keys:
            if isinstance(val, bool):
                cli_args += f"--{key} " if val else f"--no-{key} "
            else:
                cli_args += f"--{key} {val} "

    # Override with unknown_args
    cli_args += (" ".join(unknown_args) + " ")

    # Add model name
    cli_args += "{} ".format(config["model"])

    # Add model arguments
    for key, val in config.items():
        if key in set_model_keys:
            if isinstance(val, bool):
                cli_args += f"--{key} " if val else f"--no-{key} "
            else:
                cli_args += f"--{key} {val} "

    return cli_args, infer_output_fullpath


def main(args, unknown_args):
    # Groundtruth dataframe
    groundtruth = build_groundtruth_df(args.dataset_path, args.dataset_split_name)
    batch_size = utils.get_largest_divisor(len(groundtruth), less_than=args.max_batch_size)
    log.info(f"Use batch_size: {batch_size}")
    assert len(groundtruth) % batch_size == 0

    for n, train_dir in enumerate(
        traverse_train_dir(
            args.search_dir, args.best_metric, args.score_threshold, args.score_type, args.dataset_substring, verbose=1
        )
    ):
        ckpt_path, score_path = build_checkpoint_and_score_path(train_dir, Path(args.best_metric))
        assert ckpt_path is not None and score_path is not None
        log.info(f"Ensemble({n}): {ckpt_path}")
        score = pd.read_csv(score_path, delim_whitespace=True).transpose()

    log.info(f"Ensemble {n+1} models!")
    time.sleep(5)

    if args.dryrun:
        log.info("dryrun done!")
        return

    for n, train_dir in enumerate(traverse_train_dir(
        args.search_dir, args.best_metric, args.score_threshold, args.score_type, args.dataset_substring
    )):
        ckpt_path, score_path = build_checkpoint_and_score_path(train_dir, Path(args.best_metric))
        cli_args, infer_output_fullpath = build_cli_arguments(
            train_dir,
            args.preserved_keys,
            unknown_args,
            args.inference_output_dirname,
            args.inference_output,  # output_type
            ckpt_path,
            args.dataset_path,
            args.dataset_split_name,
            batch_size,
        )
        log.info(f"Build up CLI arguments from {CONFIG}")
        log.info(cli_args)
        tf_utils.call_subprocess(f"python {args.evaluate_file} {cli_args}", log, timeout=28800)
        log.info(f"({n}) Inference done! : {infer_output_fullpath}")
        shutil.copy(score_path, infer_output_fullpath.parent / SCORES)

    log.info(f"="*30)
    # Each models' inference result
    df_dict = {}
    for infer_dir in Path(args.inference_output_dirname).iterdir():
        if not infer_dir.is_dir():
            continue
        df = pd.read_csv(infer_dir / f"{args.inference_output}.csv")
        df.filename = df.filename.apply(lambda p: Path(p).name)
        df.set_index(INFER_INDEX, inplace=True)
        df_dict[infer_dir] = df

        tmp = groundtruth.copy()
        tmp[infer_dir] = df.idxmax(axis=1)
        acc = (tmp[GROUNDTRUTH_COL] == tmp[infer_dir]).mean()
        log.info(f"Accuracy on {infer_dir}: {acc}")
    panel = pd.Panel(df_dict)

    log.info(f"="*30)
    # Calculate Accuracy on Ensemble Result!
    ensemble = panel.mean(axis=0).idxmax(axis=1)  # Series
    merged = groundtruth.copy()
    merged[ENSEMBLE_COL] = ensemble
    acc = (merged[GROUNDTRUTH_COL] == merged[ENSEMBLE_COL]).mean()
    log.info(f"Final accuracy: {acc} on {args.dataset_path}/{args.dataset_split_name}")
    log.info(f"Save prediction to {args.inference_output_dirname}/{args.ensemble_csv}")
    pd.DataFrame({"ensemble": ensemble}).to_csv(Path(args.inference_output_dirname) / args.ensemble_csv)
    log.info(f"="*30)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--dryrun', action='store_true')
    parser.add_argument("--search_dir", type=str)
    parser.add_argument("--best_metric", type=str)
    parser.add_argument("--max_batch_size", type=int, default=32)
    parser.add_argument("--ensemble_csv", type=str, default="ensemble.csv")

    parser.add_argument("--dataset_path", type=str, required=True)
    parser.add_argument("--dataset_split_name", type=str, required=True)
    parser.add_argument("--dataset_substring", type=str, required=True)
    parser.add_argument("--score_threshold", type=float, required=True)
    parser.add_argument("--score_type", type=str, required=True,
                        choices=["min", "max"])

    parser.add_argument("--inference_output_dirname", type=str)
    parser.add_argument("--inference_output", type=str)
    parser.add_argument("--preserved_keys", type=str, nargs="*",
                        help="")
    parser.add_argument("--evaluate_file", type=str, default="evaluate_audio.py")

    args, unknown_args = parser.parse_known_args()
    if Path(args.inference_output_dirname).exists():
        log.info("Remove all ../data/ensemble/")
        shutil.rmtree(args.inference_output_dirname)
    main(args, unknown_args)
