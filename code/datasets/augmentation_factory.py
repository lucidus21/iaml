import random
from math import pi

from tensorflow.contrib.framework.python.ops import audio_ops as contrib_audio
import tensorflow as tf
from PIL import Image
import numpy as np


_available_audio_augmentation_methods = [
    "anchored_slice_or_pad",
    "random_slice_or_pad",
    "random_slice_or_pad_with_repeat",
]
_available_augmentation_methods = _available_audio_augmentation_methods


# --- audio augmentation
def anchored_slice_or_pad(audio_binary, desired_samples, file_format, sample_rate):
    if file_format == "wav":
        wav_decoder = contrib_audio.decode_wav(
            audio_binary,
            desired_channels=1,
            # If desired_samples is set, then the audio will be cropped or padded with zeroes to the requested length.
            desired_samples=desired_samples,
        )
    else:
        raise ValueError(f"Unsupported file format: {file_format}")

    anchored_slice_or_pad_signals = wav_decoder.audio
    return anchored_slice_or_pad_signals


def _load_audio_file(audio_binary, file_format, sample_rate):
    if file_format == "wav":
        wav_decoder = contrib_audio.decode_wav(audio_binary, desired_channels=1)
        audio = wav_decoder.audio
    elif file_format in ("mp3", "mp4", "ogg"):
        audio = tf.contrib.ffmpeg.decode_audio(
            audio_binary,
            file_format=file_format,
            samples_per_second=sample_rate,
            channel_count=1,
            stream=None
        )
    return audio


def random_slice_or_pad(audio_binary, desired_samples, file_format, sample_rate):
    audio = _load_audio_file(audio_binary, file_format, sample_rate)
    shape = tf.shape(audio)
    wav_length = shape[0]
    def slice_fn():
        slice_length = wav_length-desired_samples
        slice_length = tf.cast(slice_length, tf.int32)
        slice_offset = tf.random_uniform(shape=[], minval=0, maxval=slice_length, dtype=tf.int32)
        return tf.slice(audio, [slice_offset, 0], [desired_samples, 1])

    def pad_fn():
        total_pad = tf.cast(desired_samples-wav_length, tf.int32)
        left_pad = tf.random_uniform(shape=[], minval=0, maxval=total_pad, dtype=tf.int32)
        right_pad = total_pad - left_pad
        return tf.pad(audio, [[left_pad, right_pad], [0, 0]], mode="CONSTANT")

    # random_uniform doesn't support [0, 0)
    random_slice_or_pad_signals = tf.cond(
        tf.equal(shape[0], desired_samples),
        true_fn=lambda : audio,
        false_fn=lambda : tf.cond(
            shape[0] > desired_samples,
            true_fn=slice_fn,
            false_fn=pad_fn,
        )
    )
    random_slice_or_pad_signals.set_shape([desired_samples, 1])
    return random_slice_or_pad_signals


def random_slice_or_pad_with_repeat(audio_binary, desired_samples, file_format, sample_rate):
    audio = _load_audio_file(audio_binary, file_format, sample_rate)
    shape = tf.shape(audio)
    wav_length = shape[0]
    def slice_fn():
        slice_length = wav_length-desired_samples
        slice_length = tf.cast(slice_length, tf.int32)
        slice_offset = tf.random_uniform(shape=[], minval=0, maxval=slice_length, dtype=tf.int32)
        return tf.slice(audio, [slice_offset, 0], [desired_samples, 1])

    def pad_fn():
        n = tf.cast(tf.divide(desired_samples, wav_length), tf.int32)
        repeated_audio = tf.tile(audio, [n+1, 1])
        slice_length = wav_length*(n+1)-desired_samples
        slice_length = tf.cast(slice_length, tf.int32)
        slice_offset = tf.random_uniform(shape=[], minval=0, maxval=slice_length, dtype=tf.int32)
        return tf.slice(repeated_audio, [slice_offset, 0], [desired_samples, 1])

    # random_uniform doesn't support [0, 0)
    random_slice_or_pad_signals = tf.cond(
        tf.equal(shape[0], desired_samples),
        true_fn=lambda : audio,
        false_fn=lambda : tf.cond(
            shape[0] > desired_samples,
            true_fn=slice_fn,
            false_fn=pad_fn,
        )
    )
    random_slice_or_pad_signals.set_shape([desired_samples, 1])
    return random_slice_or_pad_signals


def get_audio_augmentation_fn(name):
    if name not in _available_audio_augmentation_methods:
        raise ValueError(f"Augmentation name [{name}] was not recognized")
    return eval(name)


def get_augmentation_fn(name, on_PIL=False):
    """Returns augmentation_fn(image, height, width, channels, **kwargs).
    Args:
        name: The name of the preprocessing function.
        Returns:
        augmentation_fn: A function that preprocessing a single image (pre-batch).
            It has the following signature:
                image = augmentation_fn(image, output_height, output_width, ...).

    Raises:
        ValueError: If Preprocessing `name` is not recognized.
    """
    if on_PIL:
        name = name + ON_PIL_SUFFIX
    if name not in _available_augmentation_methods:
        raise ValueError(f"Augmentation name [{name}] was not recognized")

    def augmentation_fn(image, output_height: int, output_width: int, channels, **kwargs):
        # TODO keep variable `channels` into kwargs
        return eval(name)(
            image, output_height, output_width, channels, **kwargs)

    return augmentation_fn
