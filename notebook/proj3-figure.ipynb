{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 36,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 37,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "bidi-gru-eval.log  \u001b[0m\u001b[01;32mdataloader.py\u001b[0m*  model4.log    transformer-eval2.log\r\n",
      "bidi-gru.log       \u001b[01;32mfeatures.py\u001b[0m*    \u001b[01;34mnotebook\u001b[0m/     transformer-eval.log\r\n",
      "\u001b[01;34mcheckpoint\u001b[0m/        main.py         \u001b[01;34mofficial\u001b[0m/     transformer-train.log\r\n",
      "\u001b[01;36mdata\u001b[0m@              model3.log      \u001b[01;34m__pycache__\u001b[0m/  utils.py\r\n"
     ]
    }
   ],
   "source": [
    "ls ../proj3/"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_logs = [\"../proj3/bidi-gru.log\", \"../proj3/transformer-train.log\"]\n",
    "evaluate_logs = [\"../proj3/bidi-gru-eval.log\", \"../proj3/transformer-eval2.log\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def parse_train(log_path):\n",
    "    with open(log_path) as f:\n",
    "        lines = f.read().strip().split(\"\\n\")\n",
    "        interested_lines = []\n",
    "        for line in lines:\n",
    "            if \"Cost: \" in line:\n",
    "                interested_lines.append(line)\n",
    "\n",
    "    values = []\n",
    "    for line in interested_lines:\n",
    "        trimed_line = line.replace(\" \", \"\")\n",
    "        step, remained = trimed_line.split(\",\")\n",
    "        step = int(step.split(\":\")[1])\n",
    "        remained, acc = remained.split(\"Accuracy:\")\n",
    "        acc = float(acc)\n",
    "        cost = float(remained.split(\":\")[1])\n",
    "        values.append([step, cost, acc])\n",
    "    return pd.DataFrame(values, columns=[\"step\", \"cost\", \"acc\"])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def parse_eval(log_path):\n",
    "    with open(log_path, \"r\") as f:\n",
    "        lines = f.read().strip().split(\"\\n\")\n",
    "        interested_lines = []\n",
    "        for line in lines:\n",
    "            if \"Test accuracy from separate process: \" in line:\n",
    "                interested_lines.append(line)\n",
    "            \n",
    "    values = []\n",
    "    for line in interested_lines:\n",
    "        remained, acc = line.split(\"|Test accuracy from separate process: \")\n",
    "        acc = float(acc)\n",
    "        step = int(remained.split(\"-\")[-1])\n",
    "        values.append([step, acc])\n",
    "    return pd.DataFrame(values, columns=[\"step\", \"acc\"])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bidi_gru_train = parse_train(train_logs[0])\n",
    "transformer_train = parse_train(train_logs[1])\n",
    "bidi_gru_eval = parse_eval(evaluate_logs[0])\n",
    "transformer_eval = parse_eval(evaluate_logs[1])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#     R    G    B    hex\n",
    "# yellow    233    174    53    #e9ae35\n",
    "# red    213    77    47    #d54d2f\n",
    "# blue    27    109    181    #1b6db5\n",
    "# purple    118    47    134    #762f86\n",
    "# green    115    169    72    #73a948\n",
    "# sky    72    188    234    #48bcea\n",
    "colors = [\"#e9ae35\",\"#d54d2f\",\"#1b6db5\",\"#762f86\",\"#73a948\",\"#48bcea\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(figsize=(20, 8))\n",
    "\n",
    "bidi_gru_train.set_index(\"step\")[\"acc\"].plot(label=\"1-layer-bidi-gru128-attention(train)\", ax=ax, color=\"#e9ae35\", marker=\"v\", alpha=0.3)\n",
    "bidi_gru_eval.set_index(\"step\")[\"acc\"].plot(label=\"1-layer-bidi-gru128-attention(evaluate)\", ax=ax, color=\"#d54d2f\", marker=\"o\")\n",
    "\n",
    "transformer_train.set_index(\"step\")[\"acc\"].plot(label=\"transformer(train)\", ax=ax, color=\"#48bcea\", marker=\"v\", alpha=0.3)\n",
    "transformer_eval.set_index(\"step\")[\"acc\"].plot(label=\"transformer(evaluate)\", ax=ax, color=\"#1b6db5\", marker=\"o\")\n",
    "ax.legend(loc=\"lower right\", fancybox=True, shadow=True, fontsize=\"xx-large\")\n",
    "ax.set_xlim([-5, 5000])\n",
    "ax.set_title(\"RNN vs Transformer\")\n",
    "ax.set_ylabel(\"Accuarcy\")\n",
    "fig.savefig(\"rnn_vs_transformer_partial.pdf\", format='pdf', dpi=1000)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(figsize=(20, 8))\n",
    "\n",
    "bidi_gru_train.set_index(\"step\")[\"acc\"].plot(label=\"1-layer-bidi-gru128-attention(train)\", ax=ax, color=\"#e9ae35\", marker=\"v\", alpha=0.3)\n",
    "bidi_gru_eval.set_index(\"step\")[\"acc\"].plot(label=\"1-layer-bidi-gru128-attention(evaluate)\", ax=ax, color=\"#d54d2f\", marker=\"o\")\n",
    "\n",
    "transformer_train.set_index(\"step\")[\"acc\"].plot(label=\"transformer(train)\", ax=ax, color=\"#48bcea\", marker=\"v\", alpha=0.3)\n",
    "transformer_eval.set_index(\"step\")[\"acc\"].plot(label=\"transformer(evaluate)\", ax=ax, color=\"#1b6db5\", marker=\"o\")\n",
    "ax.legend(loc=\"lower right\", fancybox=True, shadow=True, fontsize=\"xx-large\")\n",
    "ax.set_xlim([-5, 23000])\n",
    "ax.set_title(\"RNN vs Transformer(Best at 1515 step)\")\n",
    "ax.set_ylabel(\"Accuarcy\")\n",
    "fig.savefig(\"rnn_vs_transformer_all.pdf\", format='pdf', dpi=1000)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
