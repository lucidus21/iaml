import tensorflow as tf
import numpy as np
from overload import overload

import common.image_helper as image_helper
import metrics.parser as parser
from metrics.ops.base_ops import TensorMetricOpBase
from metrics.summaries import BaseSummaries


class LossesMetricOp(TensorMetricOpBase):
    """ Loss Metric.
    """
    _properties = {
        "is_for_summary": True,
        "is_for_best_keep": True,
        "is_for_log": True,
        "valid_input_data_parsers": [
            parser.SingleLabelClassificationDataParser,
            parser.SingleLabelClassificationBCDataParser,
            parser.MultiLabelClassificationDataParser,
            parser.MultiTaskClassificationDataParser,
            parser.SegmentationDataParser,
            parser.SingleLabelDistillationDataParser,
            parser.MultiLabelDistillationDataParser,
            parser.ImageGenerateDataParser,
            parser.PicaiDataParser,
            parser.AudioDataParser,
            parser.MultiLabelAudioDataParser,
        ],
        "summary_collection_key": BaseSummaries.KEY_TYPES.DEFAULT,
        "summary_value_type": BaseSummaries.VALUE_TYPES.PLACEHOLDER,
        "min_max_mode": "min",
    }

    def __str__(self):
        return "losses"

    def build_op(self, data):
        result = dict()

        for loss_name, loss_op in data.losses.items():
            key = f"metric_loss/{data.dataset_split_name}/{loss_name}"
            result[key] = loss_op

        return result

    def expectation_of(self, data: np.array):
        assert len(data.shape) == 2
        return np.mean(data)


class ImageSummaryOp(TensorMetricOpBase):
    """ Image summary
    """
    _properties = {
        "is_for_summary": True,
        "is_for_best_keep": False,
        "is_for_log": False,
        "valid_input_data_parsers": [
            parser.SegmentationDataParser,
            parser.ImageGenerateDataParser,
            parser.PicaiDataParser,
        ],
        "summary_collection_key": BaseSummaries.KEY_TYPES.DEFAULT,
        "summary_value_type": BaseSummaries.VALUE_TYPES.IMAGE,
        "min_max_mode": None,
    }

    def __str__(self):
        return "summary_images"

    @overload
    def build_op(self,
                 data: parser.SegmentationDataParser.OutputBuildData):
        result = dict()

        for summary_name, op in data.summary_images.items():
            key = f"basic_images/{data.dataset_split_name}/{summary_name}"
            result[key] = op

        return result

    @build_op.add
    def build_op(self,
                 data: parser.PicaiDataParser.OutputBuildData):
        result = dict()

        for summary_name, op in data.summary_images.items():
            key = f"basic_images/{data.dataset_split_name}/{summary_name}"
            result[key] = op

        return result

    def expectation_of(self, data):
        pass

    @build_op.add
    def build_op(self,
                 data: parser.ImageGenerateDataParser.OutputBuildData):
        result = dict()

        for summary_name, op in data.summary_images.items():
            key = f"img_{summary_name}/{data.dataset_split_name}"
            result[key] = op

        return result


class MADMetricOp(TensorMetricOpBase):
    """ Mean Average Difference Metric.
    """
    _properties = {
        "is_for_summary": True,
        "is_for_best_keep": True,
        "is_for_log": True,
        "valid_input_data_parsers": [
            parser.SegmentationDataParser,
            parser.ImageGenerateDataParser,
        ],
        "summary_collection_key": BaseSummaries.KEY_TYPES.DEFAULT,
        "summary_value_type": BaseSummaries.VALUE_TYPES.PLACEHOLDER,
        "min_max_mode": "min",
    }

    def __str__(self):
        return "mad_metric"

    @overload
    def build_op(self,
                 data: parser.SegmentationDataParser.OutputBuildData):
        key = f"MAD/{data.dataset_split_name}"
        metric = tf.losses.absolute_difference(data.masks, data.alpha_scores)

        return {
            key: metric
        }

    def expectation_of(self, data: np.array):
        assert len(data.shape) == 2
        return np.mean(data)

    @build_op.add
    def build_op(self,
                 data: parser.ImageGenerateDataParser.OutputBuildData):
        key = f"MAD/{data.dataset_split_name}"
        metric = tf.losses.absolute_difference(data.gts, data.predictions)

        return {
            key: metric
        }


class MSEMetricOp(TensorMetricOpBase):
    """ Mean Squared Error Metric.
    """
    _properties = {
        "is_for_summary": True,
        "is_for_best_keep": True,
        "is_for_log": True,
        "valid_input_data_parsers": [
            parser.SegmentationDataParser,
            parser.ImageGenerateDataParser,
        ],
        "summary_collection_key": BaseSummaries.KEY_TYPES.DEFAULT,
        "summary_value_type": BaseSummaries.VALUE_TYPES.PLACEHOLDER,
        "min_max_mode": "min",
    }

    def __str__(self):
        return "mse_metric"

    @overload
    def build_op(self,
                 data: parser.SegmentationDataParser.OutputBuildData):
        key = f"MSE/{data.dataset_split_name}"
        metric = tf.losses.mean_squared_error(data.masks, data.alpha_scores)

        return {
            key: metric
        }

    def expectation_of(self, data: np.array):
        assert len(data.shape) == 2
        return np.mean(data)

    @build_op.add
    def build_op(self,
                 data: parser.ImageGenerateDataParser.OutputBuildData):
        key = f"MSE/{data.dataset_split_name}"
        metric = tf.losses.mean_squared_error(data.gts, data.predictions)

        return {
            key: metric
        }


class GradMetricOp(TensorMetricOpBase):
    """ Gradient Error Metric.
    """
    _properties = {
        "is_for_summary": True,
        "is_for_best_keep": True,
        "is_for_log": True,
        "valid_input_data_parsers": [
            parser.SegmentationDataParser,
            parser.ImageGenerateDataParser,
        ],
        "summary_collection_key": BaseSummaries.KEY_TYPES.DEFAULT,
        "summary_value_type": BaseSummaries.VALUE_TYPES.PLACEHOLDER,
        "min_max_mode": "min",
    }

    def __str__(self):
        return "grad_metric"

    @overload
    def build_op(self,
                 data: parser.SegmentationDataParser.OutputBuildData):
        key = f"GRAD/{data.dataset_split_name}"

        grad_masks = image_helper.sobel_gradient(data.masks)
        grad_alpha_scores = image_helper.sobel_gradient(data.alpha_scores)

        metric = tf.losses.absolute_difference(grad_masks, grad_alpha_scores)

        return {
            key: metric
        }

    def expectation_of(self, data: np.array):
        assert len(data.shape) == 2
        return np.mean(data)

    @build_op.add
    def build_op(self,
                 data: parser.ImageGenerateDataParser.OutputBuildData):
        key = f"GRAD/{data.dataset_split_name}"

        grad_gts = image_helper.sobel_gradient(data.gts)
        grad_predictions = image_helper.sobel_gradient(data.predictions)

        metric = tf.losses.absolute_difference(grad_gts, grad_predictions)

        return {
            key: metric
        }


class WavSummaryOp(TensorMetricOpBase):
    _properties = {
        "is_for_summary": True,
        "is_for_best_keep": False,
        "is_for_log": False,
        "valid_input_data_parsers": [
            parser.AudioDataParser,
            parser.MultiLabelAudioDataParser,
        ],
        "summary_collection_key": BaseSummaries.KEY_TYPES.DEFAULT,
        "summary_value_type": BaseSummaries.VALUE_TYPES.AUDIO,
        "min_max_mode": None,
    }

    def __str__(self):
        return "summary_wav"

    def build_op(self, data: parser.AudioDataParser.OutputBuildData):
        return {
            f"wav/{data.dataset_split_name}": data.wavs
        }

    def expectation_of(self, data):
        pass


class SpectrogramSummaryOp(TensorMetricOpBase):
    _properties = {
        "is_for_summary": True,
        "is_for_best_keep": False,
        "is_for_log": False,
        "valid_input_data_parsers": [
            parser.AudioDataParser,
            parser.MultiLabelAudioDataParser,
        ],
        "summary_collection_key": BaseSummaries.KEY_TYPES.DEFAULT,
        "summary_value_type": BaseSummaries.VALUE_TYPES.IMAGE,
        "min_max_mode": None,
    }

    def __str__(self):
        return "summary_spectrogram"

    def expectation_of(self, data):
        pass

    def build_op(self,
                 data: parser.AudioDataParser.OutputBuildData):
        result = dict()

        for summary_name, op in data.spectrograms.items():
            key = f"{summary_name}/{data.dataset_split_name}"
            result[key] = op

        return result
