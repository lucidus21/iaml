from pathlib import Path
import os

import tensorflow as tf


TF_SESSION_CONFIG = tf.ConfigProto(
    gpu_options=tf.GPUOptions(allow_growth=True),
    log_device_placement=False,
    device_count={"GPU": 1})

# RGB order
IMAGENET_MEAN = [122.67891434, 116.66876762, 104.00698793]  # in 0~1 version: [.481093782, .457524579, .407870541]
VGG_MEAN = [103.939, 116.779, 123.68]
# TensorFlow version
_R_MEAN = 123.68
_G_MEAN = 116.78
_B_MEAN = 103.94

# This values are used in https://github.com/CSAILVision/places365/blob/master/run_placesCNN_unified.py#L75
IMAGENET_MEAN_RATIO = [0.485, 0.456, 0.406]
IMAGENET_STD_RATIO = [0.229, 0.224, 0.225]

TRANSFORM_OPTIONS = (
    "remove_nodes(op=Identity, op=CheckNumerics) fold_constants(ignore_errors=true) "
    "fold_batch_norms fold_old_batch_norms fuse_resize_and_conv fuse_resize_pad_and_conv"
)

# Directories
PATH_MODEL_DIRECTORY = Path(os.path.dirname(os.path.realpath(__file__)))
PATH_DATA_WORKING = Path(PATH_MODEL_DIRECTORY) / "../data/working"
PATH_DATA_WORKING_SMOKE_TEST = Path(PATH_DATA_WORKING) / "smoke_test"
