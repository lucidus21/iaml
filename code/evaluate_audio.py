import argparse

import tensorflow as tf

from factory.base import CNNModel
import factory.audio_nets as audio_nets
from helper.base import Base
from helper.evaluator import Evaluator
from helper.evaluator import SingleLabelAudioEvaluator
from helper.evaluator import MultiLabelAudioEvaluator
from datasets.audio_data_wrapper import AudioDataWrapper
from datasets.audio_data_wrapper import SingleLabelAudioDataWrapper
from datasets.audio_data_wrapper import MultiLabelAudioDataWrapper
from datasets.audio_data_wrapper import DataWrapperBase
from metrics.base import MetricManagerBase
from common.tf_utils import ckpt_iterator
import common.utils as utils
import const


def main(args):
    is_training = False
    dataset_name = args.dataset_split_name[0]
    session = tf.Session(config=const.TF_SESSION_CONFIG)

    if args.task_type == "audio_single":
        dataset = SingleLabelAudioDataWrapper(
            args,
            session,
            dataset_name,
            is_training,
        )
    elif args.task_type == "audio_multilabel":
        dataset = MultiLabelAudioDataWrapper(
            args,
            session,
            dataset_name,
            is_training,
            args.num_classes,
        )
    wavs, spectrograms, fingerprints, labels, genres = dataset.get_input_and_output_op()

    model = eval(f"audio_nets.{args.model}")(args, dataset)
    model.build(
        wavs=wavs,
        spectrograms=spectrograms,
        fingerprints=fingerprints,
        labels=labels,
        is_training=is_training,
        genres=genres,
    )

    dataset_name = args.dataset_split_name[0]
    if args.task_type == "audio_single":
        evaluator = SingleLabelAudioEvaluator(model, session, args, dataset, dataset_name)
    elif args.task_type == "audio_multilabel":
        evaluator = MultiLabelAudioEvaluator(
            model,
            session,
            args,
            dataset,
            dataset_name,  # TODO remove dataset_name is already passed with the data
        )
    log = utils.get_logger("EvaluateAudio")

    if args.inference:
        evaluator.inference(args.checkpoint_path)
    else:
        if args.valid_type == "once":
            evaluator.evaluate_once(args.checkpoint_path)
        elif args.valid_type == "loop":
            log.info(f"Start Loop: watching {evaluator.watch_path}")

            kwargs = {
                "min_interval_secs": 0,
                "timeout": None,
                "timeout_fn": None,
                "logger": log,
            }

            for ckpt_path in ckpt_iterator(evaluator.watch_path, **kwargs):
                log.info(f"[watch] {ckpt_path}")

                evaluator.evaluate_once(ckpt_path)

        else:
            raise ValueError(f"Undefined valid_type: {args.valid_type}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__)
    subparsers = parser.add_subparsers(title="Model", description="")

    # Refactor add_arguments logic
    Base.add_arguments(parser)
    Evaluator.add_arguments(parser)
    DataWrapperBase.add_arguments(parser)
    AudioDataWrapper.add_arguments(parser)
    CNNModel.add_arguments(parser, default_type="classification")
    MetricManagerBase.add_arguments(parser)
    for class_name in audio_nets._available_nets:
        subparser = subparsers.add_parser(class_name)
        subparser.add_argument("--model", default=class_name, type=str, help="DO NOT FIX ME")
        add_audio_arguments = eval("audio_nets.{}.add_arguments".format(class_name))
        add_audio_arguments(subparser)

    args = parser.parse_args()

    # Hack!!! subparser's arguments and dynamically add it to args(Namespace)
    # it will be used for convert.py
    model_arguments = utils.get_subparser_argument_list(parser, args.model)
    args.model_arguments = model_arguments
    log = utils.get_logger("AudioNetEvaluate")
    log.info(args)
    main(args)
