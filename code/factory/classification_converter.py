from abc import ABC
from abc import abstractmethod

import tensorflow as tf
import tensorflow.contrib.slim as slim


class ConverterBase(ABC):
    @classmethod
    @abstractmethod
    def convert(
        cls,
        logits: tf.Tensor,
        output_name: str,
    ):
        raise NotImplementedError(f"convert() not defined in {cls.__class__.__name__}")


class SoftmaxConverter(ConverterBase):
    @classmethod
    def convert(
        cls,
        logits: tf.Tensor,
        output_name: str,
    ):
        output = slim.softmax(logits, scope=output_name + "/softmax")
        output = tf.identity(output, name=output_name)
        return output


class SigmoidConverter(ConverterBase):
    @classmethod
    def convert(
        cls,
        logits: tf.Tensor,
        output_name: str,
    ):
        output = tf.sigmoid(logits, name=output_name)
        return output


class ArgmaxConverter(ConverterBase):
    @classmethod
    def convert(
        cls,
        logits: tf.Tensor,
        output_name: str,
    ):
        output = tf.argmax(logits, name=output_name)
        return output
