import tensorflow as tf


class BaselineVAE():
    def __init__(self, is_train_mode):
        self.is_train_mode = is_train_mode

    def Encoder(self, x, hidden_dim=1024, latent_dim=32):

        with tf.variable_scope("Encoder", reuse=tf.AUTO_REUSE):
            fc = tf.layers.dense(x, hidden_dim, activation=tf.tanh)
            z_before = tf.layers.dense(fc, latent_dim * 2)

        return z_before

    def Decoder(self, z, data_dim=256*128, hidden_dim=1024):

        with tf.variable_scope("Decoder", reuse=tf.AUTO_REUSE):
            fc = tf.layers.dense(z, hidden_dim, activation=tf.tanh)
            output = tf.layers.dense(fc, data_dim)

        return output

    def VAE(self, x, data_dim=256*128, hidden_dim=1024, latent_dim=32):

        z_before = self.Encoder(x, hidden_dim=hidden_dim, latent_dim=latent_dim)
        mu = z_before[:, :latent_dim]
        sig = z_before[:, latent_dim:]

        eps = tf.random_normal(tf.shape(mu), dtype=tf.float64, mean=0., stddev=1.0,
                            name='epsilon')
        z = mu + eps * (tf.exp(sig / 2))

        x_reconstructed = self.Decoder(z, data_dim=data_dim, hidden_dim=hidden_dim)

        return x_reconstructed, mu, sig

    def VAE_loss(self, x_reconstructed, x_target, mu, sig):

        reconst_loss = tf.nn.sigmoid_cross_entropy_with_logits(logits=x_reconstructed,
                                                            labels=x_target)
        reconst_loss = tf.reduce_sum(reconst_loss, 1)

        kld_loss = 1 + sig - tf.square(mu) - tf.exp(sig)
        kld_loss = -0.5 * tf.reduce_sum(kld_loss, 1)

        return tf.reduce_mean(reconst_loss + kld_loss)


class DropoutBaselineVAE(BaselineVAE):
    def Encoder(self, x, hidden_dim=1024, latent_dim=32):
        with tf.variable_scope("Encoder", reuse=tf.AUTO_REUSE):
            fc = tf.layers.dense(x, hidden_dim, activation=tf.tanh)
            fc = tf.layers.dropout(fc, rate=0.8, training=self.is_train_mode)
            z_before = tf.layers.dense(fc, latent_dim * 2)
            z_before = tf.layers.dropout(z_before, rate=0.5, training=self.is_train_mode)

        return z_before

    def Decoder(self, z, data_dim=256*128, hidden_dim=1024):
        with tf.variable_scope("Decoder", reuse=tf.AUTO_REUSE):
            fc = tf.layers.dense(z, hidden_dim, activation=tf.tanh)
            fc = tf.layers.dropout(fc, rate=0.8, training=self.is_train_mode)
            output = tf.layers.dense(fc, data_dim)
            output = tf.layers.dropout(output, rate=0.5, training=self.is_train_mode)

        return output


class DropoutBaselineVAE2(BaselineVAE):
    def Encoder(self, x, hidden_dim=1024, latent_dim=32):
        with tf.variable_scope("Encoder", reuse=tf.AUTO_REUSE):
            fc = tf.layers.dense(x, hidden_dim, activation=tf.tanh)
            fc = tf.layers.dropout(fc, rate=0.8, training=self.is_train_mode)
            z_before = tf.layers.dense(fc, latent_dim * 2)
            # z_before = tf.layers.dropout(z_before, rate=0.8, training=self.is_train_mode)

        return z_before

    def Decoder(self, z, data_dim=256*128, hidden_dim=1024):
        with tf.variable_scope("Decoder", reuse=tf.AUTO_REUSE):
            fc = tf.layers.dense(z, hidden_dim, activation=tf.tanh)
            fc = tf.layers.dropout(fc, rate=0.8, training=self.is_train_mode)
            output = tf.layers.dense(fc, data_dim)
            # output = tf.layers.dropout(output, rate=0.8, training=self.is_train_mode)

        return output


class DropoutBaselineVAE3(BaselineVAE):
    def Encoder(self, x, hidden_dim=1024, latent_dim=32):
        with tf.variable_scope("Encoder", reuse=tf.AUTO_REUSE):
            fc = tf.layers.dense(x, hidden_dim, activation=tf.tanh)
            fc = tf.layers.dropout(fc, rate=0.8, training=self.is_train_mode)
            fc = tf.layers.dense(fc, hidden_dim, activation=tf.tanh)
            fc = tf.layers.dropout(fc, rate=0.8, training=self.is_train_mode)
            z_before = tf.layers.dense(fc, latent_dim * 2)
            # z_before = tf.layers.dropout(z_before, rate=0.8, training=self.is_train_mode)

        return z_before

    def Decoder(self, z, data_dim=256*128, hidden_dim=1024):
        with tf.variable_scope("Decoder", reuse=tf.AUTO_REUSE):
            fc = tf.layers.dense(z, hidden_dim, activation=tf.tanh)
            fc = tf.layers.dropout(fc, rate=0.8, training=self.is_train_mode)
            fc = tf.layers.dense(fc, hidden_dim, activation=tf.tanh)
            fc = tf.layers.dropout(fc, rate=0.8, training=self.is_train_mode)
            output = tf.layers.dense(fc, data_dim)
            # output = tf.layers.dropout(output, rate=0.8, training=self.is_train_mode)

        return output
