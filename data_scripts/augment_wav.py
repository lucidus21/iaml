from pathlib import Path
import argparse
import random
import hashlib
import uuid

from tqdm import tqdm
import soundfile as sf
import numpy as np
import librosa


def librosa_augmentation(prob_mask, filename, sample_rate):
    # https://www.kaggle.com/huseinzol05/sound-augmentation-librosa
    audio, _ = librosa.load(filename, sr=sample_rate)
    stamp = 0

    n_steps, speed, rand = None, None, None
    # pitch
    if prob_mask[0]:
        n_steps = random.choice(np.arange(-2, 2+1, step=1))  # np.arange -> [-N, ... N-1]
        stamp += n_steps*1
        audio = librosa.effects.pitch_shift(audio, sample_rate, n_steps=n_steps)

    # speed
    if prob_mask[1]:
        speed = np.random.uniform(low=0.9,high=1.1)
        stamp += round(speed, 2)*10000
        audio = librosa.effects.time_stretch(audio, speed)

    # noise
    if prob_mask[2]:
        rand = np.random.uniform()
        noise_amp = 0.005**np.amax(audio)
        stamp += round(rand, 2)*100000000
        audio = audio + noise_amp * np.random.normal(size=audio.shape[0])

    audio = np.expand_dims(audio, axis=1).astype(np.float32)
    print(n_steps, speed, rand)

    return audio, stamp


def main(args):
    maxv = np.iinfo(np.int16).max
    for split_dir in Path(args.source_dir).iterdir():
        if "validation" in split_dir.name or "test" in split_dir.name:
            continue
        for label_dir in tqdm(Path(split_dir).iterdir()):
            for wav_path in tqdm(Path(label_dir).iterdir()):
                for _ in range(args.factor):
                    prob_mask = np.random.randn(3) > 0.5  # probability of all augmentation = 2.7%
                    label_name = label_dir.name
                    split_name = split_dir.name

                    augmented_audio, stamp = librosa_augmentation(prob_mask, str(wav_path), args.sample_rate)
                    prefix = hashlib.md5(str(stamp).encode("utf-8")).hexdigest()[:8]
                    output_path = Path(args.target_dir) / split_name / label_name / (prefix + "_" + wav_path.name)

                    print(output_path, prob_mask, stamp, prefix)
                    output_path.parent.mkdir(exist_ok=True, parents=True)
                    # It doesn't work on recent change
                    # audio16 = (maxv * augmented_audio).astype(np.int16)
                    # librosa.output.write_wav(output_path, audio16, args.sample_rate)
                    sf.write(str(output_path), augmented_audio, args.sample_rate, subtype="PCM_16")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--source_dir', default=None, type=str)
    parser.add_argument('--target_dir', default=None, type=str)
    parser.add_argument('--sample_rate', default=16000, type=str)
    parser.add_argument('--factor', default=10, type=int)
    args = parser.parse_args()
    main(args)
