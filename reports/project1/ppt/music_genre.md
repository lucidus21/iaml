footer: M1505.001300_001 2018 가을 Team5 , Project 1
autoscale: true

# Music Genre Classification
### 기계학습 산업응용, 2018 가을
### 2018-10-30 (Tue)

| Team5 Members |||
| --- | --- | --- |
| 2011-11949 신범준 | 2011-11927 임능환 | 2012-11842 김진홍 |

<!-- TODO:
- ffmpeg 으로 데이터 늘리는거 보여주기
- 얼리 스타핑 추가하기, Regularization 줄기 만들기
-->

----

# 문제

- 30초 길이의 음원이 주어졌을 때 해당 음원의 장르가 무엇인지 맞춘다
- **Test Accuracy**를 최대화하는 것이 목표이다

---

# 전략

- CNN은 SOTA가 이미 충분히 좋기 때문에 그대로 가져온다
- 오디오 문제를 CNN 문제로 풀 수 있도록 적합한 전처리(Preprocessing) 방식에 신경쓴다
- 데이터양이 적음으로 오버피팅 방지를 위해 Regularization(Augmentation, L2 Normalization 등)에 노력한다
- 앙상블을 통해 Acc를 극단으로 끌어올린다

----

# 실험

- 오디오 데이터 전처리 비교 실험
- 오디오 데이터 확장 실험
- 모델 비교 실험
- L2 Regularization 실험

----

# 오디오 데이터 전처리 비교 실험

- 아날로그 시그널을 디지털 시그널로 변환한다
- 딥러닝은 좋은 피쳐 생성기임으로 특별환 변환없이 원본 시그널을 그대로 넣어도 될 수도 있다
- 하지만 FFT 는 여전히 학습하기 어려운 함수임으로 **불필요한 모델 복잡도를 줄이기 위해 변환을 사용**하자

----

# 오디오 데이터 전처리 비교 실험 (cont.)

- 빠른 실험 반복을 위해선 효율적인 데이터 파이프라인이 필수적이다
- [`tf.data`](https://www.tensorflow.org/performance/datasets_performance) API를 최대한으로 활용할 수 있는 전처리 방법만 사용한다
- mp3 보다 load가 빠른 wav 포맷으로 전부 데이터를 변환한다 (`ffmpeg -i [input_mp3] -ar 16000 [output_wav]`)

![inline](media/15407058207782.png)
![inline](media/15407058321761.png)

---

# 오디오 데이터 전처리 방법 후보

- `log_mel_spectrogram`
- `mel_spectrogram`
- `MFCC`
- `magnitude_spectrogram`
- `log_magnitude_spectrogram`

----

### 오디오 데이터 전처리 방법 시각화 비교[^1]
#### `log_magnitude_spectrogram` vs `magnitude_spectrogram` vs `power_spectrogram`

![inline](media/15370851333114.jpg)

[^1]: sound 'happy' from google speech command

----

### 오디오 데이터 전처리 방법 시각화 비교 (cont.)
#### `log_mel_sepctrogram`

![inline](media/15370870759762.jpg)

----

### 오디오 데이터 전처리 방법 시각화 비교 (cont.)
#### `MFCC`

![inline](media/15370882802770.jpg)

----

# 오디오 데이터 전처리 비교 실험

- Google Speech Command 데이터와 모델로 실험 진행[^2]
- [Google speech command dataset](https://www.kaggle.com/c/tensorflow-speech-recognition-challenge)
- [Convolutional Neural Networks for Small-footprint Keyword Spotting](https://static.googleusercontent.com/media/research.google.com/en//pubs/archive/43969.pdf)
- mAP(mean Average Precision)으로 비교[^3]
- 기타 하이퍼파라미터는 구글에서 사용하는 하이퍼파라미터를 사용[^4]


[^2]: 실험 당시 music 데이터 공개 전이였음

[^3]: [AP: Area under precision-recall curve](http://scikit-learn.org/stable/modules/generated/sklearn.metrics.average_precision_score.html)

[^4]: `window_size_ms`: 30ms, `window_stride_ms`: 15ms, `lower_edge_hertz`: 80ms, `upper_edge_hertz`: 7600ms, `num_mel_bins`: 64, `num_mfcc`: 40, `sample_rate`: 16000Hz

----

### 오디오 데이터 전처리 비교 실험에 사용한 모델: **conv** (cnn-trad-fpool3)

```
(fingerprint_input)
        v
    [Conv2D]<-(weights)
        v
    [BiasAdd]<-(bias)
        v
      [Relu]
        v
    [MaxPool]
        v
    [Conv2D]<-(weights)
        v
    [BiasAdd]<-(bias)
        v
      [Relu]
        v
    [MaxPool]
        v
    [MatMul]<-(weights)
        v
    [BiasAdd]<-(bias)
        v
```

----

# Top5 Acc 비교

![inline](media/15375481378946.jpg)

---

#### 각 메트릭별 Best모델간의 비교

![inline](media/15373196880910.jpg)

---

# 오디오 데이터 전처리 비교 실험 결과

- mel scale을 사용한 `log_mel_spectrogram` 혹은 `mel_spectrogram`이 가장 낫다
- 만약 모델이 correlated input에 민감하다면 MFCC를 사용하자

---

# 오디오 데이터 확장 실험

1. 배치로 음원 자체를 바꾸기(x15배, 96000=6400x15)
    - pitch shift: -1, -0.5, 0, 0.5, 1 tone
    - time stretch: x0.9 ~ x1.1
    - noise addition: max(audio) * 0.0005 (normal)
2. 데이터 파이프라인 상에서 음원의 시작 위치를 무작위로 바꾸기

---

## `augment_wav.py`[^5]

```python
prob_mask = np.random.randn(3) > 0.7

def librosa_augmentation(prob_mask, filename, sample_rate):
    audio, _ = librosa.load(filename, sr=sample_rate)
    # pitch
    if prob_mask[0]:
        n_steps = random.choice(np.arange(-2, 2+1, step=1))  # np.arange -> [-N, ... N-1]
        audio = librosa.effects.pitch_shift(audio, sample_rate, n_steps=n_steps)

    # speed
    if prob_mask[1]:
        speed = np.random.uniform(low=0.9,high=1.1)
        audio = librosa.effects.time_stretch(audio, speed)

    # noise
    if prob_mask[2]:
        noise_amp = 0.005*np.random.uniform()*np.amax(audio)
        audio = audio + noise_amp * np.random.normal(size=audio.shape[0])

    audio = np.expand_dims(audio, axis=1).astype(np.float32)
    return audio
```

[^5]: `python augment_wav.py --source_dir ~/data/music_dataset_split --target_dir ~/data/augmented_music_dataset_split --factor 15`

---

## (데이터 확장-1) 확장된 데이터(파란색) vs 원본 데이터(주황색)[^6]

![inline](media/15407094183296.jpg)![inline](media/15407094295218.jpg)

![inline](media/15407093770561.jpg)![inline](media/15407093962114.jpg)

[^6]: `augmented-vs-nonaugmented/` 폴더 참고, validation 실험이 중간에 끊겨서 중간 결과가 없습니다.

---

#### (데이터 확장-2) 데이터 파이프라인 상에서 음원의 시작 위치를 무작위로 바꾸기

| 구현 | `random_slice_or_pad` | `random_slice_or_pad_with_repeat` |
| --- | --- | --- |
| 실제 길이 < 타겟 길이 | random zero pad | Tile audio & random slice |
| 실제 길이 > 타겟 길이 | random slice | random slice |

---

### `random_slice_or_pad_with_repeat`

> 음원을 반드시 맨 앞부터 듣지 않더라도 충분히 장르 구분이 가능하다면 이 방법이 유용할 수 있음

![right fit](figures/figures.001.png)

---

#### `random_slice_or_pad`(빨간색) vs `random_slice_or_pad_with_repeat`(하늘색)[^8]

![inline](media/15407121926204.jpg)![inline](media/15407121780800.jpg)

[^8]: `slicepad_vs_slicepad_with_repeat/` 폴더 참고

---

# 오디오 데이터 확장 실험 결과

- (데이터 확장 - 1) step 100k 에서 데이터 확장을 하는 경우 Acc **57.34%** 까지 도달하였으나 그렇지 않은 경우엔 최대 Acc에서 **56.79%**였다.
- (데이터 확장 - 2) 하루 정도 학습을 돌려보았을 때 `random_slice_or_pad`가 약간 낫지만 큰 차이가 없었다.

---

# 모델 비교 실험
### 최근 CNN State of the art

| Research Paper | Metric | Year |
| --- | --- | --- |
| Aggregated Residual Transformations for Deep Neural Networks (Resnext101) | Top-1 Error: 20.4% | 2016 |
| Squeeze-and-Excitation Networks (SEnet) | Top-1 Error: 18.68% | 2017 |
| Learning Transferable Architectures for Scalable Image Recognition (NasNet) | Top-1 Error: 17.3% | 2017 |

---

# 모델 비교 실험 (cont.)

- 어차피 데이터가 작고 학습할 수 있는 시간이 한정되어있기 때문에 SENet, NasNet을 사용하는데 큰 이점이 없을 것이라 가정하였다
- 적절한 하이퍼파라미터 튜닝 없이는 오히려 작은 모델이 Regularization 하는 효과가 있어서 더 좋을 수도 있다

---

# 모델 비교 실험 (cont.)

- `Resnext101Model`
- `Resnext50Model`

---

## Renext101Model(핑크색) vs Resnext50Model(파란색)[^7]

![inline](media/15407110105784.jpg)![inline](media/15407109649630.jpg)

[^7]: `augmented-vs-nonaugmented/` 폴더 참고

---

# 모델 비교 실험 (cont.)

- 최소한 학습 초반엔 Resnext101Model이 우월하였고 학습을 오래할수록 차이가 있어보인다
- 하지만 차이가 크지 않음으로 둘 중 무엇을 사용하든 상관없다
- 오히려 상대적으로 모델이 작은 Resnext50Model의 경우 실험을 진행하기에 더 편리하다

----

# L2 Regularization 실험

- 같은 조건하에 weight_decay를 바꿔가며 실험한다
- 모델은 `Resnext50`을 사용하였다

> **`weight_decay`=0.01 을 사용한 경우
> 60k에서 Acc 58.29%까지 도달하였다.**

----

#### `weight_decay`=0.01(핑크색) vs `weight_decay`=0.001(초록색) vs `weight_decay`=0.0001(파란색)[^9]

![inline](media/15407133099966.jpg)![inline](media/15407134041085.jpg)

[^9]: `weight_decay/` 폴더 참고

----

# 앙상블

- 학습하는 과정에서 각 validation metric별로 최고 성능을 보인 모든 파라미터를 별도 저장해두었다
- Acc보단 mAP가 모델 간 비교에 좀 더 안정적인 지표라고 판단하였다.
- 각 실험별로 `mAP` 0.59 이상 만족시킨 파라미터를 가져와 각각 레이블별 확률을 예측한 이후 모든 확률 값을 평균내었다.

----

# 앙상블에 사용한 파라미터

| 실험 종류(폴더명)         | 세부 실험 결과 폴더명                      | 선택된 파라미터       | 정확도 |
|---------------------------|--------------------------------------------|-----------------------|--------|
| augmented-vs-nonaugmented | `181027001557-music_dataset_split`           | Resnext50Model-6000   | 0.52   |
| augmented-vs-nonaugmented | `181027011353-augmented_music_dataset_split` | Resnext101Model-35500 | 0.55   |
| augmented-vs-nonaugmented | `181027001557-augmented_music_dataset_split` | Resnext50Model-100000 | 0.54   |
| etc                       | `181026061608-ubuntu-Resnext101Model`        | Resnext101Model-43000 | 0.53   |
| etc                       | `181024163857-ubuntu-resnext50-from-scratch` | Resnext50Model-30000  | 0.53   |
| etc                       | `181028012832-Resnext50Model-mfcc`           | Resnext50Model-77000  | 0.5    |
| weight_decay              | `181028012639-weight_decay-0.01`             | Resnext50Model-45000  | 0.56   |
| weight_decay              | `181028012639-weight_decay-0.001`            | Resnext50Model-53000  | 0.53   |

<!--
[INFO|ensemble.py:216] 2018-10-28 20:17:25,201 > (7) Inference done! : ../data/ensemble/___selected_experiments_weight_decay_181028012639-weight_decay-0.001_validation_mAP_validation_macro_Resnext50Model-53000/prob.csv
[INFO|ensemble.py:219] 2018-10-28 20:17:25,216 > ==============================
[INFO|ensemble.py:233] 2018-10-28 20:17:25,307 > Accuracy on ../data/ensemble/___selected_experiments_etc_181026061608-ubuntu-Resnext101Model_validation_mAP_validation_macro_Resnext101Model-43000: 0.5399002493765586
[INFO|ensemble.py:233] 2018-10-28 20:17:25,344 > Accuracy on ../data/ensemble/___selected_experiments_augmented-vs-nonaugmented_181027001557-music_dataset_split_validation_mAP_validation_macro_Resnext50Model-6000: 0.5249376558603491
[INFO|ensemble.py:233] 2018-10-28 20:17:25,366 > Accuracy on ../data/ensemble/___selected_experiments_weight_decay_181028012639-weight_decay-0.01_validation_mAP_validation_macro_Resnext50Model-45000: 0.5685785536159601
[INFO|ensemble.py:233] 2018-10-28 20:17:25,402 > Accuracy on ../data/ensemble/___selected_experiments_augmented-vs-nonaugmented_181027011353-augmented_music_dataset_split-Resnext101Model_validation_mAP_validation_macro_Resnext101Model-35500: 0.5511221945137157
[INFO|ensemble.py:233] 2018-10-28 20:17:25,421 > Accuracy on ../data/ensemble/___selected_experiments_augmented-vs-nonaugmented_181027001557-augmented_music_dataset_split_validation_mAP_validation_macro_Resnext50Model-100000: 0.5486284289276808
[INFO|ensemble.py:233] 2018-10-28 20:17:25,446 > Accuracy on ../data/ensemble/___selected_experiments_etc_181024163857-ubuntu-resnext50-from-scratch-and-proj-dataset-alldata-softmax-fixevalwindowmismatch_validation_mAP_validation_macro_Resnext50Model-30000: 0.5336658354114713
[INFO|ensemble.py:233] 2018-10-28 20:17:25,456 > Accuracy on ../data/ensemble/___selected_experiments_etc_181028012832-Resnext50Model-mfcc_validation_mAP_validation_macro_Resnext50Model-77000: 0.5099750623441397
[INFO|ensemble.py:233] 2018-10-28 20:17:25,465 > Accuracy on ../data/ensemble/___selected_experiments_weight_decay_181028012639-weight_decay-0.001_validation_mAP_validation_macro_Resnext50Model-53000: 0.5361596009975063
[INFO|ensemble.py:236] 2018-10-28 20:17:25,467 > ==============================
[INFO|ensemble.py:242] 2018-10-28 20:17:25,469 > Final accuracy: 0.6221945137157108 on ../data/input/music_dataset_split/validation
[INFO|ensemble.py:243] 2018-10-28 20:17:25,469 > Save prediction to ../data/ensemble/ensemble.csv
[INFO|ensemble.py:245] 2018-10-28 20:17:25,490 > ==============================
-->

---

## 앙상블 결과

# 최종 밸리데이션 정확도(Accuracy): **62.219%**

```
==============================
Final accuracy: 0.6221945137157108 on ../data/input/music_dataset_split/validation
Save prediction to ../data/ensemble/ensemble.csv
==============================
```

---

# 연구 보완점

- 본 연구를 통해 알게된 괜찮은 하이퍼파라미터들의 조합으로 좀 더 실험하여 모델들을 얻으면 보다 좋은 성능을 만들어낼 수 있었을 것이다.
- 단순 평균하는 방법 이외에도 mAP 성능을 기초로 가중 평균을 사용하는 앙상블 방법 등 다양한 시도가 더 가능하다.

---

# 추가 연구 결과

- FMA large 데이터로 multi-label classification을 학습시킨 이후 fine-tuning한 결과 몇 스텝 가지 않아서 Acc 83% 도달하였다. (이후에 오버피팅)

![inline](media/15407139080228.jpg)
