from abc import ABC
from abc import abstractmethod
from termcolor import colored

import tensorflow as tf
import numpy as np

import const


class CNNModel(ABC):
    def preprocess_images(self, images, preprocess_method, reuse=False):
        with tf.variable_scope("preprocess", reuse=reuse):
            if images.dtype == tf.uint8:
                images = tf.cast(images, tf.float32)
            if preprocess_method == "preprocess_normalize":
                # -- * -- preprocess_normalize
                # Scale input images to range [0, 1], same scales like mean of masks
                images = tf.divide(images, tf.constant(255.0))
            elif preprocess_method == "preprocess_imagenet":
                # -- * -- preprocess_imagenet
                # Subtract mean from images and reverse channel
                means = tf.reshape(tf.constant(const.IMAGENET_MEAN), [1, 1, 1, 3])
                images = images - means
                channels = tf.unstack(images, axis=-1)
                images = tf.stack([channels[2], channels[1], channels[0]], axis=-1)
            elif preprocess_method == "preprocess_vgg":
                # -- * -- preprocess_vgg
                # https://github.com/tensorflow/models/blob/master/research/slim/preprocessing/vgg_preprocessing.py
                # https://github.com/tensorflow/models/issues/517#issuecomment-296427671
                r, g, b = tf.split(axis=3, num_or_size_splits=3, value=images)
                images = tf.concat(values=[b - const.VGG_MEAN[0], g - const.VGG_MEAN[1], r - const.VGG_MEAN[2]], axis=3)
                self.log.warning("[R, G, B] will be reversed by preprocess_vgg!")
            elif preprocess_method == "preprocess_inception":
                # -- * -- preprocess_inception
                # Scale image between -1 and 1
                images = tf.divide(images, tf.constant(255.0))
                images = tf.subtract(images, tf.constant(0.5))
                images = tf.multiply(images, tf.constant(2.0))
            elif preprocess_method == "preprocess_places365":
                images = tf.divide(images, tf.constant(255.0))
                # http://pytorch.org/docs/master/torchvision/transforms.html
                # https://github.com/CSAILVision/places365/blob/master/run_placesCNN_unified.py#L75
                images = images - const.IMAGENET_MEAN_RATIO
                images = images / const.IMAGENET_STD_RATIO
            elif preprocess_method == "preprocess_safematch_normalize":
                # https://github.com/facebook/fb.resnet.torch/blob/master/datasets/transforms.lua#L130
                # function M.ColorNormalize
                images = tf.divide(images, tf.constant(255.0))
                images = images - const.SAFEMATCH_MEAN
                images = images / const.SAFEMATCH_STD
            elif preprocess_method == "preprocess_meanstd_normalize":
                # -- * -- preprocess_meanstd_normalize
                # Subtract input images by mean and divide by standard deviation
                if self.args.mean_values is None or self.args.std_values is None:
                    self.dataset.update_mean_std_values()
                images = tf.subtract(
                    images,
                    tf.constant(np.array(self.args.mean_values, dtype=np.float32), name="inputs/mean")
                )
                images = tf.divide(
                    images,
                    tf.constant(np.array(self.args.std_values, dtype=np.float32), name="inputs/std")
                )
            elif preprocess_method == "preprocess_repeat3":
                # -- * -- preprocess_meanstd_normalize
                # Subtract input images by mean and divide by standard deviation
                images_with_channel = tf.expand_dims(images, axis=3)
                images = tf.concat([images_with_channel, images_with_channel, images_with_channel], axis=3)
            elif preprocess_method == "preprocess_expand_only":
                images = tf.expand_dims(images, axis=3)
            elif preprocess_method == "no_preprocessing":
                pass
            else:
                raise ValueError("Unsupported preprocess_method: {}".format(preprocess_method))

        # We exclude preprocessing part in TFLite model because TFLite doesn't support tf.divide, tf.subtract, etc.
        images = tf.identity(images, name=self.args.tflite_input_name)
        if self.args.verbosity >= 2:
            images = tf.Print(
                images,
                [images, tf.reduce_min(images), tf.reduce_max(images), tf.reduce_mean(images)],
                message="Raw/Min/Max/Mean of Preprocessed Images: ",
                first_n=100
            )
        return images

    @staticmethod
    def add_arguments(parser, default_type):
        g_cnn = parser.add_argument_group("(CNNModel) Arguments")
        g_cnn.add_argument("--task_type", type=str, required=True,
                           choices=[
                               "audio_single",
                               "audio_multilabel",
                           ])
        g_cnn.add_argument("--num_classes", type=int, default=None,
                           help=(
                               "It is currently not used in multi-task learning, "
                               "so it can't *required*"
                           ))
        g_cnn.add_argument("--checkpoint_path", default="", type=str)

        g_cnn.add_argument("--input_name", type=str, default="input/image")
        g_cnn.add_argument("--tflite_input_name", type=str, default="input/preprocessed",
                           help=("This specifies the node to be used as an input in TFLite model."))
        g_cnn.add_argument("--input_batch_size", type=int, default=1)
        g_cnn.add_argument("--output_name", type=str, required=True)
        g_cnn.add_argument("--output_type", type=str, required=True,
                           help=("Mainly used in convert.py",
                                 "Possible output_type is defined in each model file"
                                 "So if you do classification task, please search `OUTPUT_TYPES` in clfnets.py")
                           )

        g_cnn.add_argument("--no-use_fused_batchnorm", dest="use_fused_batchnorm", action="store_false")
        g_cnn.add_argument("--use_fused_batchnorm", dest="use_fused_batchnorm", action="store_true")
        g_cnn.set_defaults(use_fused_batchnorm=True)

        g_cnn.add_argument("--verbosity", default=0, type=int,
                           help="If verbosity > 0, then summary batch_norm scalar metrics etc")
        g_cnn.add_argument("--preprocess_method", required=True, type=str,
                           choices=["no_preprocessing", "preprocess_normalize",
                                    "preprocess_imagenet", "preprocess_inception",
                                    "preprocess_places365",
                                    "preprocess_safematch_normalize",
                                    "preprocess_meanstd_normalize",
                                    "preprocess_repeat3",
                                    "preprocess_expand_only"])

        g_cnn.add_argument("--no-ignore_missing_vars", dest="ignore_missing_vars", action="store_false")
        g_cnn.add_argument("--ignore_missing_vars", dest="ignore_missing_vars", action="store_true")
        g_cnn.set_defaults(ignore_missing_vars=False)

        g_cnn.add_argument("--checkpoint_exclude_scopes", default="", type=str,
                           help=("Prefix scopes that shoule be EXLUDED for restoring variables "
                                 "(comma separated)\n Usually Logits e.g. InceptionResnetV2/Logits/Logits, "
                                 "InceptionResnetV2/AuxLogits/Logits"))

        g_cnn.add_argument("--checkpoint_include_scopes", default="", type=str,
                           help=("Prefix scopes that should be INCLUDED for restoring variables "
                                 "(comma separated)"))
        g_cnn.add_argument("--num_tasks", type=int,
                           help="Number of tasks for multitask learning.")
        g_cnn.add_argument("--max_num_labels", type=int,
                           help=("Maximum number of classes for each task. "
                                 "If there are n tasks and each task has c[n] labels, "
                                 "max_num_labels == max(c[1], c[2], ..., c[n])."))

        g_cnn.add_argument("--mean_values", type=float, nargs="*", default=None,
                           help=("Mean values of input images(=between before preprocessing/after augmentation)"
                                 "for each dimensions"))
        g_cnn.add_argument("--std_values", type=float, nargs="*", default=None,
                           help=("Standard deviation of input images(=between before preprocessing/after augmentation)"
                                 "for each dimensions"
                           ))

    @abstractmethod
    def build_output(self):
        pass

    @property
    @abstractmethod
    def images(self):
        pass

    @property
    @abstractmethod
    def images_original(self):
        pass

    @property
    @abstractmethod
    def total_loss(self):
        pass

    @property
    @abstractmethod
    def model_loss(self):
        pass
