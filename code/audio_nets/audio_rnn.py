import tensorflow as tf

slim = tf.contrib.slim


def audio_rnn_arg_scope(is_training=True,
                        weight_decay=0.00004,
                        stddev=0.09,
                        batch_norm_decay=0.997,
                        batch_norm_epsilon=0.001,
                        use_fused_batchnorm=True):
    batch_norm_params = {
        "is_training": is_training,
        "decay": batch_norm_decay,
        "epsilon": batch_norm_epsilon,
        "fused": use_fused_batchnorm,
    }

    if stddev < 0:
        weight_intitializer = slim.initializers.xavier_initializer()
    else:
        weight_intitializer = tf.truncated_normal_initializer(stddev=stddev)

    with slim.arg_scope([slim.conv2d],
                        weights_initializer=weight_intitializer,
                        weights_regularizer=slim.l2_regularizer(weight_decay),
                        normalizer_fn=slim.batch_norm,
                        activation_fn=tf.nn.relu,
                        padding="SAME"):
        with slim.arg_scope([slim.batch_norm], **batch_norm_params) as scope:
            return scope


def audio_rnn_v1(inputs, num_classes, args):
    endpoints = {}
    cell = eval(f"tf.nn.rnn_cell.{args.v1_cell_type}")(args.v1_hidden_state)
    outputs, states = tf.nn.dynamic_rnn(cell, inputs, dtype=tf.float32)
    outputs = outputs[:, -1, :]
    logits = tf.contrib.layers.fully_connected(outputs, num_classes)
    return logits, endpoints


def audio_rnn_v2(inputs, num_classes, args):
    endpoints = {}
    cell = eval(f"tf.nn.rnn_cell.{args.v2_cell_type}")(args.v2_hidden_state, state_is_tuple=True)
    cell = tf.nn.rnn_cell.MultiRNNCell([cell] * 2, state_is_tuple=True)
    outputs, states = tf.nn.dynamic_rnn(cell, inputs, dtype=tf.float32)
    outputs = outputs[:, -1, :]
    logits = tf.contrib.layers.fully_connected(outputs, num_classes)
    return logits, endpoints


def audio_rnn_v3(inputs, num_classes, args):
    endpoints = {}
    cell = eval(f"tf.nn.rnn_cell.{args.v3_cell_type}")(args.v3_hidden_state, state_is_tuple=True)
    cell = tf.nn.rnn_cell.DropoutWrapper(cell, input_keep_prob=args.v3_keep_prob, output_keep_prob=args.v3_keep_prob)
    cell = tf.nn.rnn_cell.MultiRNNCell([cell] * 2, state_is_tuple=True)
    outputs, states = tf.nn.dynamic_rnn(cell, inputs, dtype=tf.float32)
    outputs = outputs[:, -1, :]
    logits = tf.contrib.layers.fully_connected(outputs, num_classes)
    return logits, endpoints


def audio_rnn_v4(inputs, num_classes, args):
    endpoints = {}
    cell = eval(f"{args.v4_cell_type}")(args.v4_hidden_state)
    cell = tf.nn.rnn_cell.MultiRNNCell([cell] * 2, state_is_tuple=True)
    outputs, states = tf.nn.dynamic_rnn(cell, inputs, dtype=tf.float32)
    outputs = outputs[:, -1, :]
    logits = tf.contrib.layers.fully_connected(outputs, num_classes)
    return logits, endpoints


def audio_rnn_v5(inputs, num_classes, args):
    endpoints = {}
    output = inputs
    num_layers = args.v5_num_layers
    for layer in range(num_layers):
        with tf.variable_scope('encoder_{}'.format(layer),reuse=tf.AUTO_REUSE):
            cell_type = eval(f"{args.v5_cell_type}")
            cell_fw = cell_type(args.v5_hidden_state, initializer=tf.truncated_normal_initializer(-0.1, 0.1, seed=2))
            cell_fw = tf.contrib.rnn.DropoutWrapper(cell_fw, input_keep_prob=args.v5_keep_prob)
            cell_bw = cell_type(args.v5_hidden_state, initializer=tf.truncated_normal_initializer(-0.1, 0.1, seed=2))
            cell_bw = tf.contrib.rnn.DropoutWrapper(cell_bw, input_keep_prob=args.v5_keep_prob)
            outputs, states = tf.nn.bidirectional_dynamic_rnn(cell_fw, cell_bw, output, dtype=tf.float32)
            output = tf.concat(outputs,2)
    outputs = output[:, -1, :]
    logits = tf.contrib.layers.fully_connected(outputs, num_classes)
    return logits, endpoints


def audio_rnn_v6(inputs, num_classes, args):
    endpoints = {}
    output = inputs
    num_layers = args.v6_num_layers
    for layer in range(num_layers):
        with tf.variable_scope('encoder_{}'.format(layer),reuse=tf.AUTO_REUSE):
            cell_fw = tf.contrib.rnn.LayerNormBasicLSTMCell(args.v6_hidden_state)
            cell_bw = tf.contrib.rnn.LayerNormBasicLSTMCell(args.v6_hidden_state)
            outputs, states = tf.nn.bidirectional_dynamic_rnn(cell_fw, cell_bw, output, dtype=tf.float32)
            output = tf.concat(outputs,2)
    outputs = output[:, -1, :]
    logits = tf.contrib.layers.fully_connected(outputs, num_classes)
    return logits, endpoints


def audio_rnn_v7(inputs, num_classes, args):
    # Attention mechanism
    endpoints = {}
    output = inputs
    num_layers = args.v7_num_layers
    for layer in range(num_layers):
        with tf.variable_scope('encoder_{}'.format(layer),reuse=tf.AUTO_REUSE):
            cell_type = eval(f"{args.v5_cell_type}")
            cell_fw = cell_type(args.v7_hidden_state)
            cell_bw = cell_type(args.v7_hidden_state)
            outputs, states = tf.nn.bidirectional_dynamic_rnn(cell_fw, cell_bw, output, dtype=tf.float32)
            output = tf.concat(outputs, 2)
    # output => (?, 1665, 128)
    unnormalized_attention = tf.contrib.layers.fully_connected(output, 1)  # (?, 1665, 1)
    attention = tf.nn.softmax(unnormalized_attention, axis=1)
    endpoints["attention"] = attention
    attentive_output = tf.reduce_sum(attention * output, axis=1)
    endpoints["before_attentive_output"] = output
    endpoints["attentive_output"] = attentive_output
    logits = tf.contrib.layers.fully_connected(attentive_output, num_classes)
    return logits, endpoints


def audio_rnn_v8(inputs, num_classes, args):
    endpoints = {}
    output = inputs
    num_layers = args.v8_num_layers
    for layer in range(num_layers):
        with tf.variable_scope('encoder_{}'.format(layer),reuse=tf.AUTO_REUSE):
            cell_type = eval(f"{args.v8_cell_type}")
            cell_fw = cell_type(args.v8_hidden_state)
            cell_fw = tf.contrib.rnn.DropoutWrapper(cell_fw, input_keep_prob=args.v8_keep_prob)
            cell_bw = cell_type(args.v8_hidden_state)
            cell_bw = tf.contrib.rnn.DropoutWrapper(cell_bw, input_keep_prob=args.v8_keep_prob)
            outputs, states = tf.nn.bidirectional_dynamic_rnn(cell_fw, cell_bw, output, dtype=tf.float32)
            output = tf.concat(outputs,2)
    outputs = output[:, -1, :]
    logits = tf.contrib.layers.fully_connected(outputs, num_classes)
    return logits, endpoints


def audio_rnn_v9(inputs, num_classes, args):
    endpoints = {}
    output = inputs
    num_layers = args.v9_num_layers
    for layer in range(num_layers):
        with tf.variable_scope('encoder_{}'.format(layer),reuse=tf.AUTO_REUSE):
            cell_type = eval(f"{args.v9_cell_type}")
            cell_fw = cell_type(args.v9_hidden_state)
            cell_fw = tf.contrib.rnn.DropoutWrapper(cell_fw, input_keep_prob=args.v9_keep_prob)
            cell_bw = cell_type(args.v9_hidden_state)
            cell_bw = tf.contrib.rnn.DropoutWrapper(cell_bw, input_keep_prob=args.v9_keep_prob)
            outputs, states = tf.nn.bidirectional_dynamic_rnn(cell_fw, cell_bw, output, dtype=tf.float32)
            output = tf.concat(outputs,2)
    unnormalized_attention = tf.contrib.layers.fully_connected(output, 1)  # (?, 1665, 1)
    attention = tf.nn.softmax(unnormalized_attention, axis=1)
    endpoints["attention"] = attention
    attentive_output = tf.reduce_sum(attention * output, axis=1)
    endpoints["before_attentive_output"] = output
    endpoints["attentive_output"] = attentive_output
    logits = tf.contrib.layers.fully_connected(attentive_output, num_classes)
    return logits, endpoints


def audio_rnn_final(inputs, num_classes, args):
    endpoints = {}
    cell = eval(f"{args.final_cell_type}")(args.final_hidden_state)
    if args.final_dropout > 0:
        cell = tf.nn.rnn_cell.DropoutWrapper(cell, input_keep_prob=args.final_keep_prob, output_keep_prob=args.final_keep_prob)
    outputs, states = tf.nn.dynamic_rnn(cell, inputs, dtype=tf.float32)

    if args.final_attention > 0:
        unnormalized_attention = tf.contrib.layers.fully_connected(outputs, 1)  # (?, 1665, 1)
        attention = tf.nn.softmax(unnormalized_attention, axis=1)
        endpoints["attention"] = attention
        attentive_output = tf.reduce_sum(attention * outputs, axis=1)
        endpoints["before_attentive_output"] = outputs
        endpoints["attentive_output"] = attentive_output
        logits = tf.contrib.layers.fully_connected(attentive_output, num_classes)
        return logits, endpoints
    else:
        outputs = outputs[:, -1, :]
        logits = tf.contrib.layers.fully_connected(outputs, num_classes)
        return logits, endpoints


def audio_rnn_final2(inputs, num_classes, args):
    endpoints = {}
    cell = eval(f"{args.final2_cell_type}")(args.final2_hidden_state)
    if args.final2_dropout > 0:
        cell = tf.nn.rnn_cell.DropoutWrapper(cell, input_keep_prob=args.final2_keep_prob, output_keep_prob=args.final2_keep_prob)
    outputs, states = tf.nn.dynamic_rnn(cell, inputs, dtype=tf.float32)

    outputs = tf.reduce_mean(outputs, axis=1)
    logits = tf.contrib.layers.fully_connected(outputs, num_classes)
    return logits, endpoints


def audio_rnn_final3(inputs, num_classes, args):
    endpoints = {}
    cell = eval(f"{args.final3_cell_type}")(args.final3_hidden_state)
    if args.final3_dropout > 0:
        cell = tf.nn.rnn_cell.DropoutWrapper(cell, input_keep_prob=args.final3_keep_prob, output_keep_prob=args.final3_keep_prob)
    outputs, states = tf.nn.dynamic_rnn(cell, inputs, dtype=tf.float32)

    if args.final3_attention > 0:
        unnormalized_attention = tf.contrib.layers.fully_connected(outputs, 1)  # (?, 1665, 1)
        attention = tf.nn.softmax(unnormalized_attention, axis=1)
        endpoints["attention"] = attention
        attentive_output = tf.reduce_sum(attention * outputs, axis=1)
        endpoints["before_attentive_output"] = outputs
        endpoints["attentive_output"] = attentive_output
        logits = tf.contrib.layers.fully_connected(attentive_output, num_classes)
        return logits, endpoints
    else:
        outputs = outputs[:, -1, :]
        logits = tf.contrib.layers.fully_connected(outputs, num_classes)
        return logits, endpoints
