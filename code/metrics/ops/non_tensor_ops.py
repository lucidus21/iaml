import numpy as np
import pandas as pd
from sklearn.metrics import accuracy_score
from sklearn.metrics import average_precision_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from sklearn.metrics import classification_report
from overload import overload

import metrics.parser as parser
from metrics.funcs import multilabel_accuracy
from metrics.funcs import multilabel_precision
from metrics.funcs import multilabel_recall
from metrics.funcs import hamming_loss
from metrics.funcs import topN_accuracy
from metrics.funcs import diversity
from metrics.funcs import jaccard_distance
from metrics.funcs import ndcg_at_k
from metrics.ops.base_ops import NonTensorMetricOpBase
from metrics.summaries import BaseSummaries


class MAPMetricOp(NonTensorMetricOpBase):
    """
    Micro Mean Average Precision Metric.
    """
    _properties = {
        "is_for_summary": True,
        "is_for_best_keep": True,
        "is_for_log": True,
        "valid_input_data_parsers": [
            parser.SingleLabelClassificationDataParser,
            parser.MultiTaskClassificationDataParser,
            parser.SingleLabelDistillationDataParser,
            parser.AudioDataParser,
        ],
        "summary_collection_key": BaseSummaries.KEY_TYPES.DEFAULT,
        "summary_value_type": BaseSummaries.VALUE_TYPES.PLACEHOLDER,
        "min_max_mode": "max",
    }

    _average_fns = {
        "macro": lambda t, p: average_precision_score(t, p, average="macro"),
        "micro": lambda t, p: average_precision_score(t, p, average="micro"),
        "weighted": lambda t, p: average_precision_score(t, p, average="weighted"),
        "samples": lambda t, p: average_precision_score(t, p, average="samples"),
    }

    def __str__(self):
        return "mAP_metric"

    @overload
    def build_op(self,
                 data: parser.SingleLabelClassificationDataParser.OutputBuildData):
        result = dict()

        for avg_name in self._average_fns:
            key = f"mAP/{data.dataset_split_name}/{avg_name}"
            result[key] = None

        return result

    @overload
    def evaluate(self,
                 data: parser.SingleLabelClassificationDataParser.OutputNonTensorData):
        result = dict()

        for avg_name, avg_fn in self._average_fns.items():
            key = f"mAP/{data.dataset_split_name}/{avg_name}"
            result[key] = avg_fn(data.labels_onehot,
                                 data.predictions_onehot)

        return result

    @build_op.add
    def build_op(self,
                 data: parser.MultiTaskClassificationDataParser.OutputBuildData):
        result = dict()

        for avg_name in self._average_fns:
            for task_name in data.task_names:
                key = f"mAP/{data.dataset_split_name}/{task_name}/{avg_name}"
                result[key] = None

        return result

    @evaluate.add
    def evaluate(self,
                 data: parser.MultiTaskClassificationDataParser.OutputNonTensorData):
        result = dict()

        for avg_name, avg_fn in self._average_fns.items():
            for task_idx, task_name in enumerate(data.task_names):
                key = f"mAP/{data.dataset_split_name}/{task_name}/{avg_name}"

                num_labels = data.num_task_labels[task_name]

                task_labels_onehot = data.labels_onehot[:, task_idx, :num_labels]
                task_predictions_onehot = data.predictions_onehot[:, task_idx, :num_labels]
                valid_idx = data.valid_indexes[:, task_idx]

                result[key] = avg_fn(task_labels_onehot[valid_idx, :],
                                     task_predictions_onehot[valid_idx, :])

        return result

    @build_op.add
    def build_op(self,
                 data: parser.SingleLabelDistillationDataParser.OutputBuildData):
        result = dict()

        for avg_name in self._average_fns:
            key = f"mAP/{data.dataset_split_name}/{avg_name}"
            result[key] = None

        return result

    @evaluate.add
    def evaluate(self,
                 data: parser.SingleLabelDistillationDataParser.OutputNonTensorData):
        result = dict()

        for avg_name, avg_fn in self._average_fns.items():
            key = f"mAP/{data.dataset_split_name}/{avg_name}"
            result[key] = avg_fn(data.labels_onehot,
                                 data.predictions_onehot)

        return result

    @build_op.add
    def build_op(self,
                 data: parser.AudioDataParser.OutputBuildData):
        result = dict()

        for avg_name in self._average_fns:
            key = f"mAP/{data.dataset_split_name}/{avg_name}"
            result[key] = None

        return result

    @evaluate.add
    def evaluate(self,
                 data: parser.AudioDataParser.OutputNonTensorData):
        result = dict()

        for avg_name, avg_fn in self._average_fns.items():
            key = f"mAP/{data.dataset_split_name}/{avg_name}"
            result[key] = avg_fn(data.labels_onehot, data.predictions_onehot)

        return result


class AccuracyMetricOp(NonTensorMetricOpBase):
    """
    Accuracy Metric.
    """
    _properties = {
        "is_for_summary": True,
        "is_for_best_keep": True,
        "is_for_log": True,
        "valid_input_data_parsers": [
            parser.SingleLabelClassificationDataParser,
            parser.MultiLabelClassificationDataParser,
            parser.MultiTaskClassificationDataParser,
            parser.SingleLabelDistillationDataParser,
            parser.MultiLabelDistillationDataParser,
            parser.AudioDataParser,
            parser.MultiLabelAudioDataParser,
        ],
        "summary_collection_key": BaseSummaries.KEY_TYPES.DEFAULT,
        "summary_value_type": BaseSummaries.VALUE_TYPES.PLACEHOLDER,
        "min_max_mode": "max",
    }

    def __str__(self):
        return "accuracy_metric"

    @overload
    def build_op(self,
                 data: parser.SingleLabelClassificationDataParser.OutputBuildData):
        key = f"accuracy/{data.dataset_split_name}"

        return {
            key: None
        }

    @overload
    def evaluate(self,
                 data: parser.SingleLabelClassificationDataParser.OutputNonTensorData):
        key = f"accuracy/{data.dataset_split_name}"

        metric = accuracy_score(data.labels, data.predictions)

        return {
            key: metric
        }

    @build_op.add
    def build_op(self,
                 data: parser.MultiLabelClassificationDataParser.OutputBuildData):
        key = f"accuracy/{data.dataset_split_name}"

        return {
            key: None
        }

    @evaluate.add
    def evaluate(self,
                 data: parser.MultiLabelClassificationDataParser.OutputNonTensorData):
        key = f"accuracy/{data.dataset_split_name}"

        metric = multilabel_accuracy(data.labels_multihot, data.predictions_multihot)

        return {
            key: metric
        }

    @build_op.add
    def build_op(self,
                 data: parser.MultiTaskClassificationDataParser.OutputBuildData):
        result = dict()

        for task_name in data.task_names:
            key = f"accuracy/{data.dataset_split_name}/{task_name}"
            result[key] = None

        return result

    @evaluate.add
    def evaluate(self,
                 data: parser.MultiTaskClassificationDataParser.OutputNonTensorData):
        result = dict()

        for task_idx, task_name in enumerate(data.task_names):
            key = f"accuracy/{data.dataset_split_name}/{task_name}"

            task_labels = data.labels[:, task_idx]
            task_predictions = data.predictions[:, task_idx]
            valid_idx = data.valid_indexes[:, task_idx]

            result[key] = accuracy_score(task_labels[valid_idx],
                                         task_predictions[valid_idx])

        return result

    @build_op.add
    def build_op(self,
                 data: parser.SingleLabelDistillationDataParser.OutputBuildData):
        key = f"accuracy/{data.dataset_split_name}"

        return {
            key: None
        }

    @evaluate.add
    def evaluate(self,
                 data: parser.SingleLabelDistillationDataParser.OutputNonTensorData):
        key = f"accuracy/{data.dataset_split_name}"

        metric = accuracy_score(data.labels, data.predictions)

        return {
            key: metric
        }

    @build_op.add
    def build_op(self,
                 data: parser.MultiLabelDistillationDataParser.OutputBuildData):
        key = f"accuracy/{data.dataset_split_name}"

        return {
            key: None
        }

    @evaluate.add
    def evaluate(self,
                 data: parser.MultiLabelDistillationDataParser.OutputNonTensorData):
        key = f"accuracy/{data.dataset_split_name}"

        metric = multilabel_accuracy(data.labels_multihot, data.predictions_multihot)

        return {
            key: metric
        }

    @build_op.add
    def build_op(self,
                 data: parser.AudioDataParser.OutputBuildData):
        key = f"accuracy/{data.dataset_split_name}"

        return {
            key: None
        }

    @evaluate.add
    def evaluate(self,
                 data: parser.AudioDataParser.OutputNonTensorData):
        key = f"accuracy/{data.dataset_split_name}"

        metric = accuracy_score(data.labels, data.predictions)

        return {
            key: metric
        }

    @build_op.add
    def build_op(self,
                 data: parser.MultiLabelAudioDataParser.OutputBuildData):
        key = f"accuracy/{data.dataset_split_name}"

        return {
            key: None
        }

    @evaluate.add
    def evaluate(self,
                 data: parser.MultiLabelAudioDataParser.OutputNonTensorData):
        key = f"accuracy/{data.dataset_split_name}"

        metric = multilabel_accuracy(data.labels_multihot, data.predictions_multihot)

        return {
            key: metric
        }


class Top5AccuracyMetricOp(NonTensorMetricOpBase):
    """
    Top 5 Accuracy Metric.
    """
    _properties = {
        "is_for_summary": True,
        "is_for_best_keep": True,
        "is_for_log": True,
        "valid_input_data_parsers": [
            parser.SingleLabelClassificationDataParser,
            parser.MultiTaskClassificationDataParser,
            parser.SingleLabelDistillationDataParser,
            parser.AudioDataParser,
        ],
        "summary_collection_key": BaseSummaries.KEY_TYPES.DEFAULT,
        "summary_value_type": BaseSummaries.VALUE_TYPES.PLACEHOLDER,
        "min_max_mode": "max",
    }

    def __str__(self):
        return "top5_accuracy_metric"

    @overload
    def build_op(self,
                 data: parser.SingleLabelClassificationDataParser.OutputBuildData):
        key = f"top5_accuracy/{data.dataset_split_name}"

        return {
            key: None
        }

    @overload
    def evaluate(self,
                 data: parser.SingleLabelClassificationDataParser.OutputNonTensorData):
        key = f"top5_accuracy/{data.dataset_split_name}"

        metric = topN_accuracy(y_true=data.labels,
                               y_pred_onehot=data.predictions_onehot,
                               N=5)

        return {
            key: metric
        }

    @build_op.add
    def build_op(self,
                 data: parser.MultiTaskClassificationDataParser.OutputBuildData):
        result = dict()

        for task_name in data.task_names:
            key = f"top5_accuracy/{data.dataset_split_name}/{task_name}"
            result[key] = None

        return result

    @evaluate.add
    def evaluate(self,
                 data: parser.MultiTaskClassificationDataParser.OutputNonTensorData):
        result = dict()

        for task_idx, task_name in enumerate(data.task_names):
            key = f"top5_accuracy/{data.dataset_split_name}/{task_name}"

            num_labels = data.num_task_labels[task_name]

            task_labels = data.labels[:, task_idx]
            task_predictions_onehot = data.predictions_onehot[:, task_idx, :num_labels]
            valid_idx = data.valid_indexes[:, task_idx]

            result[key] = topN_accuracy(y_true=task_labels[valid_idx],
                                        y_pred_onehot=task_predictions_onehot[valid_idx, :],
                                        N=5)

        return result

    @build_op.add
    def build_op(self,
                 data: parser.SingleLabelDistillationDataParser.OutputBuildData):
        key = f"top5_accuracy/{data.dataset_split_name}"

        return {
            key: None
        }

    @evaluate.add
    def evaluate(self,
                 data: parser.SingleLabelDistillationDataParser.OutputNonTensorData):
        key = f"top5_accuracy/{data.dataset_split_name}"

        metric = topN_accuracy(y_true=data.labels,
                               y_pred_onehot=data.predictions_onehot,
                               N=5)

        return {
            key: metric
        }

    @build_op.add
    def build_op(self,
                 data: parser.AudioDataParser.OutputBuildData):
        key = f"top5_accuracy/{data.dataset_split_name}"

        return {
            key: None
        }

    @evaluate.add
    def evaluate(self,
                 data: parser.AudioDataParser.OutputNonTensorData):
        key = f"top5_accuracy/{data.dataset_split_name}"

        metric = topN_accuracy(y_true=data.labels,
                               y_pred_onehot=data.predictions_onehot,
                               N=5)

        return {
            key: metric
        }


class PrecisionMetricOp(NonTensorMetricOpBase):
    """
    Precision Metric.
    """
    _properties = {
        "is_for_summary": True,
        "is_for_best_keep": True,
        "is_for_log": True,
        "valid_input_data_parsers": [
            parser.SingleLabelClassificationDataParser,
            parser.MultiLabelClassificationDataParser,
            parser.MultiTaskClassificationDataParser,
            parser.SingleLabelDistillationDataParser,
            parser.MultiLabelDistillationDataParser,
            parser.AudioDataParser,
            parser.MultiLabelAudioDataParser,
        ],
        "summary_collection_key": BaseSummaries.KEY_TYPES.DEFAULT,
        "summary_value_type": BaseSummaries.VALUE_TYPES.PLACEHOLDER,
        "min_max_mode": "max",
    }

    _multilabel_average_fns = {
        "macro": lambda t, p: precision_score(t, p, average="macro"),
        "micro": lambda t, p: precision_score(t, p, average="micro"),
        "multilabel": lambda t, p: multilabel_precision(t, p),
    }

    def __str__(self):
        return "precision_metric"

    @overload
    def build_op(self,
                 data: parser.SingleLabelClassificationDataParser.OutputBuildData):
        result = dict()

        label_idxes = list(range(len(data.label_names)))

        for label_idx in label_idxes:
            label_name = data.label_names[label_idx]
            key = f"precision/{data.dataset_split_name}/{label_name}"
            result[key] = None

        return result

    @overload
    def evaluate(self,
                 data: parser.SingleLabelClassificationDataParser.OutputNonTensorData):
        result = dict()

        label_idxes = list(range(len(data.label_names)))
        precisions = precision_score(data.labels, data.predictions, average=None, labels=label_idxes)

        for label_idx in label_idxes:
            label_name = data.label_names[label_idx]
            key = f"precision/{data.dataset_split_name}/{label_name}"
            metric = precisions[label_idx]
            result[key] = metric

        return result

    @build_op.add
    def build_op(self,
                 data: parser.MultiLabelClassificationDataParser.OutputBuildData):
        result = dict()

        for avg_name, avg_fn in self._multilabel_average_fns.items():
            key = f"precision/{data.dataset_split_name}/{avg_name}"
            result[key] = None

        return result

    @evaluate.add
    def evaluate(self,
                 data: parser.MultiLabelClassificationDataParser.OutputNonTensorData):
        result = dict()

        for avg_name, avg_fn in self._multilabel_average_fns.items():
            key = f"precision/{data.dataset_split_name}/{avg_name}"
            result[key] = avg_fn(data.labels_multihot, data.predictions_multihot)

        return result

    @build_op.add
    def build_op(self,
                 data: parser.MultiTaskClassificationDataParser.OutputBuildData):
        result = dict()

        for task_name in data.task_names:
            if data.use_class_metrics[task_name]:
                for label_name in data.task_label_names[task_name]:
                    key = f"precision/{data.dataset_split_name}/{task_name}/{label_name}"
                    result[key] = None

        return result

    @evaluate.add
    def evaluate(self,
                 data: parser.MultiTaskClassificationDataParser.OutputNonTensorData):
        result = dict()

        for task_idx, task_name in enumerate(data.task_names):
            if data.use_class_metrics[task_name]:
                task_labels = data.labels[:, task_idx]
                task_predictions = data.predictions[:, task_idx]
                valid_idx = data.valid_indexes[:, task_idx]

                num_labels = data.num_task_labels[task_name]
                label_idxes = list(range(num_labels))

                precisions = precision_score(task_labels[valid_idx],
                                             task_predictions[valid_idx],
                                             average=None,
                                             labels=label_idxes)

                for label_idx, label_name in enumerate(data.task_label_names[task_name]):
                    key = f"precision/{data.dataset_split_name}/{task_name}/{label_name}"
                    metric = precisions[label_idx]
                    result[key] = metric

        return result

    @build_op.add
    def build_op(self,
                 data: parser.SingleLabelDistillationDataParser.OutputBuildData):
        result = dict()

        label_idxes = list(range(len(data.label_names)))

        for label_idx in label_idxes:
            label_name = data.label_names[label_idx]
            key = f"precision/{data.dataset_split_name}/{label_name}"
            result[key] = None

        return result

    @evaluate.add
    def evaluate(self,
                 data: parser.SingleLabelDistillationDataParser.OutputNonTensorData):
        result = dict()

        label_idxes = list(range(len(data.label_names)))
        precisions = precision_score(data.labels, data.predictions, average=None, labels=label_idxes)

        for label_idx in label_idxes:
            label_name = data.label_names[label_idx]
            key = f"precision/{data.dataset_split_name}/{label_name}"
            metric = precisions[label_idx]
            result[key] = metric

        return result

    @build_op.add
    def build_op(self,
                 data: parser.MultiLabelDistillationDataParser.OutputBuildData):
        result = dict()

        for avg_name, avg_fn in self._multilabel_average_fns.items():
            key = f"precision/{data.dataset_split_name}/{avg_name}"
            result[key] = None

        return result

    @evaluate.add
    def evaluate(self,
                 data: parser.MultiLabelDistillationDataParser.OutputNonTensorData):
        result = dict()

        for avg_name, avg_fn in self._multilabel_average_fns.items():
            key = f"precision/{data.dataset_split_name}/{avg_name}"
            result[key] = avg_fn(data.labels_multihot, data.predictions_multihot)

        return result

    @build_op.add
    def build_op(self,
                 data: parser.AudioDataParser.OutputBuildData):
        result = dict()

        label_idxes = list(range(len(data.label_names)))

        for label_idx in label_idxes:
            label_name = data.label_names[label_idx]
            key = f"precision/{data.dataset_split_name}/{label_name}"
            result[key] = None

        return result

    @evaluate.add
    def evaluate(self,
                 data: parser.AudioDataParser.OutputNonTensorData):
        result = dict()

        label_idxes = list(range(len(data.label_names)))
        precisions = precision_score(data.labels, data.predictions, average=None, labels=label_idxes)

        for label_idx in label_idxes:
            label_name = data.label_names[label_idx]
            key = f"precision/{data.dataset_split_name}/{label_name}"
            metric = precisions[label_idx]
            result[key] = metric

        return result

    @build_op.add
    def build_op(self,
                 data: parser.MultiLabelAudioDataParser.OutputBuildData):
        result = dict()

        for avg_name, avg_fn in self._multilabel_average_fns.items():
            key = f"precision/{data.dataset_split_name}/{avg_name}"
            result[key] = None

        return result

    @evaluate.add
    def evaluate(self,
                 data: parser.MultiLabelAudioDataParser.OutputNonTensorData):
        result = dict()

        for avg_name, avg_fn in self._multilabel_average_fns.items():
            key = f"precision/{data.dataset_split_name}/{avg_name}"
            result[key] = avg_fn(data.labels_multihot, data.predictions_multihot)

        return result


class RecallMetricOp(NonTensorMetricOpBase):
    """
    Recall Metric.
    """
    _properties = {
        "is_for_summary": True,
        "is_for_best_keep": True,
        "is_for_log": True,
        "valid_input_data_parsers": [
            parser.SingleLabelClassificationDataParser,
            parser.MultiLabelClassificationDataParser,
            parser.MultiTaskClassificationDataParser,
            parser.SingleLabelDistillationDataParser,
            parser.MultiLabelDistillationDataParser,
            parser.AudioDataParser,
            parser.MultiLabelAudioDataParser,
        ],
        "summary_collection_key": BaseSummaries.KEY_TYPES.DEFAULT,
        "summary_value_type": BaseSummaries.VALUE_TYPES.PLACEHOLDER,
        "min_max_mode": "max",
    }

    _multilabel_average_fns = {
        "macro": lambda t, p: recall_score(t, p, average="macro"),
        "micro": lambda t, p: recall_score(t, p, average="micro"),
        "multilabel": lambda t, p: multilabel_recall(t, p),
    }

    def __str__(self):
        return "recall_metric"

    @overload
    def build_op(self,
                 data: parser.SingleLabelClassificationDataParser.OutputBuildData):
        result = dict()

        label_idxes = list(range(len(data.label_names)))

        for label_idx in label_idxes:
            label_name = data.label_names[label_idx]
            key = f"recall/{data.dataset_split_name}/{label_name}"
            result[key] = None

        return result

    @overload
    def evaluate(self,
                 data: parser.SingleLabelClassificationDataParser.OutputNonTensorData):
        result = dict()

        label_idxes = list(range(len(data.label_names)))
        recalls = recall_score(data.labels, data.predictions, average=None, labels=label_idxes)

        for label_idx in label_idxes:
            label_name = data.label_names[label_idx]
            key = f"recall/{data.dataset_split_name}/{label_name}"
            metric = recalls[label_idx]
            result[key] = metric

        return result

    @build_op.add
    def build_op(self,
                 data: parser.MultiLabelClassificationDataParser.OutputBuildData):
        result = dict()

        for avg_name, avg_fn in self._multilabel_average_fns.items():
            key = f"recall/{data.dataset_split_name}/{avg_name}"
            result[key] = None

        return result

    @evaluate.add
    def evaluate(self,
                 data: parser.MultiLabelClassificationDataParser.OutputNonTensorData):
        result = dict()

        for avg_name, avg_fn in self._multilabel_average_fns.items():
            key = f"recall/{data.dataset_split_name}/{avg_name}"
            result[key] = avg_fn(data.labels_multihot, data.predictions_multihot)

        return result

    @build_op.add
    def build_op(self,
                 data: parser.MultiTaskClassificationDataParser.OutputBuildData):
        result = dict()

        for task_name in data.task_names:
            if data.use_class_metrics[task_name]:
                for label_name in data.task_label_names[task_name]:
                    key = f"recall/{data.dataset_split_name}/{task_name}/{label_name}"
                    result[key] = None

        return result

    @evaluate.add
    def evaluate(self,
                 data: parser.MultiTaskClassificationDataParser.OutputNonTensorData):
        result = dict()

        for task_idx, task_name in enumerate(data.task_names):
            if data.use_class_metrics[task_name]:
                task_labels = data.labels[:, task_idx]
                task_predictions = data.predictions[:, task_idx]
                valid_idx = data.valid_indexes[:, task_idx]

                num_labels = data.num_task_labels[task_name]
                label_idxes = list(range(num_labels))

                recalls = recall_score(task_labels[valid_idx],
                                       task_predictions[valid_idx],
                                       average=None,
                                       labels=label_idxes)

                for label_idx, label_name in enumerate(data.task_label_names[task_name]):
                    key = f"recall/{data.dataset_split_name}/{task_name}/{label_name}"
                    metric = recalls[label_idx]
                    result[key] = metric

        return result

    @build_op.add
    def build_op(self,
                 data: parser.SingleLabelDistillationDataParser.OutputBuildData):
        result = dict()

        label_idxes = list(range(len(data.label_names)))

        for label_idx in label_idxes:
            label_name = data.label_names[label_idx]
            key = f"recall/{data.dataset_split_name}/{label_name}"
            result[key] = None

        return result

    @evaluate.add
    def evaluate(self,
                 data: parser.SingleLabelDistillationDataParser.OutputNonTensorData):
        result = dict()

        label_idxes = list(range(len(data.label_names)))
        recalls = recall_score(data.labels, data.predictions, average=None, labels=label_idxes)

        for label_idx in label_idxes:
            label_name = data.label_names[label_idx]
            key = f"recall/{data.dataset_split_name}/{label_name}"
            metric = recalls[label_idx]
            result[key] = metric

        return result

    @build_op.add
    def build_op(self,
                 data: parser.MultiLabelDistillationDataParser.OutputBuildData):
        result = dict()

        for avg_name, avg_fn in self._multilabel_average_fns.items():
            key = f"recall/{data.dataset_split_name}/{avg_name}"
            result[key] = None

        return result

    @evaluate.add
    def evaluate(self,
                 data: parser.MultiLabelDistillationDataParser.OutputNonTensorData):
        result = dict()

        for avg_name, avg_fn in self._multilabel_average_fns.items():
            key = f"recall/{data.dataset_split_name}/{avg_name}"
            result[key] = avg_fn(data.labels_multihot, data.predictions_multihot)

        return result

    @build_op.add
    def build_op(self,
                 data: parser.AudioDataParser.OutputBuildData):
        result = dict()

        label_idxes = list(range(len(data.label_names)))

        for label_idx in label_idxes:
            label_name = data.label_names[label_idx]
            key = f"recall/{data.dataset_split_name}/{label_name}"
            result[key] = None

        return result

    @evaluate.add
    def evaluate(self,
                 data: parser.AudioDataParser.OutputNonTensorData):
        result = dict()

        label_idxes = list(range(len(data.label_names)))
        recalls = recall_score(data.labels, data.predictions, average=None, labels=label_idxes)

        for label_idx in label_idxes:
            label_name = data.label_names[label_idx]
            key = f"recall/{data.dataset_split_name}/{label_name}"
            metric = recalls[label_idx]
            result[key] = metric

        return result

    @build_op.add
    def build_op(self,
                 data: parser.MultiLabelAudioDataParser.OutputBuildData):
        result = dict()

        for avg_name, avg_fn in self._multilabel_average_fns.items():
            key = f"recall/{data.dataset_split_name}/{avg_name}"
            result[key] = None

        return result

    @evaluate.add
    def evaluate(self,
                 data: parser.MultiLabelAudioDataParser.OutputNonTensorData):
        result = dict()

        for avg_name, avg_fn in self._multilabel_average_fns.items():
            key = f"recall/{data.dataset_split_name}/{avg_name}"
            result[key] = avg_fn(data.labels_multihot, data.predictions_multihot)

        return result


class F1ScoreMetricOp(NonTensorMetricOpBase):
    """
    Per class F1-Score Metric.
    """
    _properties = {
        "is_for_summary": True,
        "is_for_best_keep": True,
        "is_for_log": True,
        "valid_input_data_parsers": [
            parser.SingleLabelClassificationDataParser,
            parser.MultiTaskClassificationDataParser,
            parser.SingleLabelDistillationDataParser,
            parser.AudioDataParser,
        ],
        "summary_collection_key": BaseSummaries.KEY_TYPES.DEFAULT,
        "summary_value_type": BaseSummaries.VALUE_TYPES.PLACEHOLDER,
        "min_max_mode": "max",
    }

    def __str__(self):
        return "f1_score_metric"

    @overload
    def build_op(self,
                 data: parser.SingleLabelClassificationDataParser.OutputBuildData):
        result = dict()

        label_idxes = list(range(len(data.label_names)))

        for label_idx in label_idxes:
            label_name = data.label_names[label_idx]
            key = f"f1score/{data.dataset_split_name}/{label_name}"
            result[key] = None

        return result

    @overload
    def evaluate(self,
                 data: parser.SingleLabelClassificationDataParser.OutputNonTensorData):
        result = dict()

        label_idxes = list(range(len(data.label_names)))
        f1_scores = f1_score(data.labels, data.predictions, average=None, labels=label_idxes)

        for label_idx in label_idxes:
            label_name = data.label_names[label_idx]
            key = f"f1score/{data.dataset_split_name}/{label_name}"
            metric = f1_scores[label_idx]
            result[key] = metric

        return result

    @build_op.add
    def build_op(self,
                 data: parser.MultiTaskClassificationDataParser.OutputBuildData):
        result = dict()

        for task_name in data.task_names:
            if data.use_class_metrics[task_name]:
                for label_name in data.task_label_names[task_name]:
                    key = f"f1score/{data.dataset_split_name}/{task_name}/{label_name}"
                    result[key] = None

        return result

    @evaluate.add
    def evaluate(self,
                 data: parser.MultiTaskClassificationDataParser.OutputNonTensorData):
        result = dict()

        for task_idx, task_name in enumerate(data.task_names):
            if data.use_class_metrics[task_name]:
                task_labels = data.labels[:, task_idx]
                task_predictions = data.predictions[:, task_idx]
                valid_idx = data.valid_indexes[:, task_idx]

                num_labels = data.num_task_labels[task_name]
                label_idxes = list(range(num_labels))

                f1_scores = f1_score(task_labels[valid_idx],
                                     task_predictions[valid_idx],
                                     average=None,
                                     labels=label_idxes)

                for label_idx, label_name in enumerate(data.task_label_names[task_name]):
                    key = f"f1score/{data.dataset_split_name}/{task_name}/{label_name}"
                    metric = f1_scores[label_idx]
                    result[key] = metric

        return result

    @build_op.add
    def build_op(self,
                 data: parser.SingleLabelDistillationDataParser.OutputBuildData):
        result = dict()

        label_idxes = list(range(len(data.label_names)))

        for label_idx in label_idxes:
            label_name = data.label_names[label_idx]
            key = f"f1score/{data.dataset_split_name}/{label_name}"
            result[key] = None

        return result

    @evaluate.add
    def evaluate(self,
                 data: parser.SingleLabelDistillationDataParser.OutputNonTensorData):
        result = dict()

        label_idxes = list(range(len(data.label_names)))
        f1_scores = f1_score(data.labels, data.predictions, average=None, labels=label_idxes)

        for label_idx in label_idxes:
            label_name = data.label_names[label_idx]
            key = f"f1score/{data.dataset_split_name}/{label_name}"
            metric = f1_scores[label_idx]
            result[key] = metric

        return result

    @build_op.add
    def build_op(self,
                 data: parser.AudioDataParser.OutputBuildData):
        result = dict()

        label_idxes = list(range(len(data.label_names)))

        for label_idx in label_idxes:
            label_name = data.label_names[label_idx]
            key = f"f1score/{data.dataset_split_name}/{label_name}"
            result[key] = None

        return result

    @evaluate.add
    def evaluate(self,
                 data: parser.AudioDataParser.OutputNonTensorData):
        result = dict()

        label_idxes = list(range(len(data.label_names)))
        f1_scores = f1_score(data.labels, data.predictions, average=None, labels=label_idxes)

        for label_idx in label_idxes:
            label_name = data.label_names[label_idx]
            key = f"f1score/{data.dataset_split_name}/{label_name}"
            metric = f1_scores[label_idx]
            result[key] = metric

        return result


class APMetricOp(NonTensorMetricOpBase):
    """
    Per class Average Precision Metric.
    """
    _properties = {
        "is_for_summary": True,
        "is_for_best_keep": True,
        "is_for_log": True,
        "valid_input_data_parsers": [
            parser.SingleLabelClassificationDataParser,
            parser.MultiTaskClassificationDataParser,
            parser.SingleLabelDistillationDataParser,
            parser.AudioDataParser,
        ],
        "summary_collection_key": BaseSummaries.KEY_TYPES.DEFAULT,
        "summary_value_type": BaseSummaries.VALUE_TYPES.PLACEHOLDER,
        "min_max_mode": "max",
    }

    def __str__(self):
        return "ap_score_metric"

    @overload
    def build_op(self,
                 data: parser.SingleLabelClassificationDataParser.OutputBuildData):
        result = dict()

        label_idxes = list(range(len(data.label_names)))

        for label_idx in label_idxes:
            label_name = data.label_names[label_idx]
            key = f"ap/{data.dataset_split_name}/{label_name}"
            result[key] = None

        return result

    @overload
    def evaluate(self,
                 data: parser.SingleLabelClassificationDataParser.OutputNonTensorData):
        result = dict()

        label_idxes = list(range(len(data.label_names)))
        aps = average_precision_score(data.labels_onehot, data.predictions_onehot, average=None)

        for label_idx in label_idxes:
            label_name = data.label_names[label_idx]
            key = f"ap/{data.dataset_split_name}/{label_name}"
            metric = aps[label_idx]
            result[key] = metric

        return result

    @build_op.add
    def build_op(self,
                 data: parser.MultiTaskClassificationDataParser.OutputBuildData):
        result = dict()

        for task_name in data.task_names:
            if data.use_class_metrics[task_name]:
                for label_name in data.task_label_names[task_name]:
                    key = f"ap/{data.dataset_split_name}/{task_name}/{label_name}"
                    result[key] = None

        return result

    @evaluate.add
    def evaluate(self,
                 data: parser.MultiTaskClassificationDataParser.OutputNonTensorData):
        result = dict()

        for task_idx, task_name in enumerate(data.task_names):
            if data.use_class_metrics[task_name]:
                num_labels = data.num_task_labels[task_name]

                task_labels_onehot = data.labels_onehot[:, task_idx, :num_labels]
                task_predictions_onehot = data.predictions_onehot[:, task_idx, :num_labels]
                valid_idx = data.valid_indexes[:, task_idx]

                aps = average_precision_score(task_labels_onehot[valid_idx, :],
                                              task_predictions_onehot[valid_idx, :],
                                              average=None)

                for label_idx, label_name in enumerate(data.task_label_names[task_name]):
                    key = f"ap/{data.dataset_split_name}/{task_name}/{label_name}"
                    metric = aps[label_idx]
                    result[key] = metric

        return result

    @build_op.add
    def build_op(self,
                 data: parser.SingleLabelDistillationDataParser.OutputBuildData):
        result = dict()

        label_idxes = list(range(len(data.label_names)))

        for label_idx in label_idxes:
            label_name = data.label_names[label_idx]
            key = f"ap/{data.dataset_split_name}/{label_name}"
            result[key] = None

        return result

    @evaluate.add
    def evaluate(self,
                 data: parser.SingleLabelDistillationDataParser.OutputNonTensorData):
        result = dict()

        label_idxes = list(range(len(data.label_names)))
        aps = average_precision_score(data.labels_onehot, data.predictions_onehot, average=None)

        for label_idx in label_idxes:
            label_name = data.label_names[label_idx]
            key = f"ap/{data.dataset_split_name}/{label_name}"
            metric = aps[label_idx]
            result[key] = metric

        return result

    @build_op.add
    def build_op(self,
                 data: parser.AudioDataParser.OutputBuildData):
        result = dict()

        label_idxes = list(range(len(data.label_names)))

        for label_idx in label_idxes:
            label_name = data.label_names[label_idx]
            key = f"ap/{data.dataset_split_name}/{label_name}"
            result[key] = None

        return result

    @evaluate.add
    def evaluate(self,
                 data: parser.AudioDataParser.OutputNonTensorData):
        result = dict()

        label_idxes = list(range(len(data.label_names)))
        aps = average_precision_score(data.labels_onehot, data.predictions_onehot, average=None)

        for label_idx in label_idxes:
            label_name = data.label_names[label_idx]
            key = f"ap/{data.dataset_split_name}/{label_name}"
            metric = aps[label_idx]
            result[key] = metric

        return result



class ClassificationReportMetricOp(NonTensorMetricOpBase):
    """
    Accuracy Metric.
    """
    _properties = {
        "is_for_summary": False,
        "is_for_best_keep": False,
        "is_for_log": True,
        "valid_input_data_parsers": [
            parser.SingleLabelClassificationDataParser,
            parser.MultiTaskClassificationDataParser,
            parser.SingleLabelDistillationDataParser,
            parser.AudioDataParser,
        ],
        "summary_collection_key": None,
        "summary_value_type": None,
        "min_max_mode": None,
    }

    def __str__(self):
        return "classification_report_metric"

    @overload
    def build_op(self,
                 data: parser.SingleLabelClassificationDataParser.OutputBuildData):
        key = f"classification_report/{data.dataset_split_name}"

        return {
            key: None
        }

    @overload
    def evaluate(self,
                 data: parser.SingleLabelClassificationDataParser.OutputNonTensorData):
        key = f"classification_report/{data.dataset_split_name}"

        label_idxes = list(range(len(data.label_names)))
        metric = classification_report(data.labels,
                                       data.predictions,
                                       labels=label_idxes,
                                       target_names=data.label_names)
        metric = f"[ClassificationReport]\n{metric}"

        return {
            key: metric
        }

    @build_op.add
    def build_op(self,
                 data: parser.MultiTaskClassificationDataParser.OutputBuildData):
        result = dict()

        for task_name in data.task_names:
            if data.use_class_metrics[task_name]:
                key = f"classification_report/{data.dataset_split_name}/{task_name}"
                result[key] = None

        return result

    @evaluate.add
    def evaluate(self,
                 data: parser.MultiTaskClassificationDataParser.OutputNonTensorData):
        result = dict()

        for task_idx, task_name in enumerate(data.task_names):
            if data.use_class_metrics[task_name]:
                key = f"classification_report/{data.dataset_split_name}/{task_name}"

                task_labels = data.labels[:, task_idx]
                task_predictions = data.predictions[:, task_idx]
                valid_idx = data.valid_indexes[:, task_idx]

                num_labels = data.num_task_labels[task_name]
                label_idxes = list(range(num_labels))

                metric = classification_report(task_labels[valid_idx],
                                               task_predictions[valid_idx],
                                               labels=label_idxes,
                                               target_names=data.task_label_names[task_name])
                result[key] = f"[ClassificationReport - {task_name}]\n{metric}"

        return result

    @build_op.add
    def build_op(self,
                 data: parser.SingleLabelDistillationDataParser.OutputBuildData):
        key = f"classification_report/{data.dataset_split_name}"

        return {
            key: None
        }

    @evaluate.add
    def evaluate(self,
                 data: parser.SingleLabelDistillationDataParser.OutputNonTensorData):
        key = f"classification_report/{data.dataset_split_name}"

        label_idxes = list(range(len(data.label_names)))
        metric = classification_report(data.labels,
                                       data.predictions,
                                       labels=label_idxes,
                                       target_names=data.label_names)
        metric = f"[ClassificationReport]\n{metric}"

        return {
            key: metric
        }

    @build_op.add
    def build_op(self,
                 data: parser.AudioDataParser.OutputBuildData):
        key = f"classification_report/{data.dataset_split_name}"

        return {
            key: None
        }

    @evaluate.add
    def evaluate(self,
                 data: parser.AudioDataParser.OutputNonTensorData):
        key = f"classification_report/{data.dataset_split_name}"

        label_idxes = list(range(len(data.label_names)))
        metric = classification_report(data.labels,
                                       data.predictions,
                                       labels=label_idxes,
                                       target_names=data.label_names)
        metric = f"[ClassificationReport]\n{metric}"

        return {
            key: metric
        }


class HammingLossOp(NonTensorMetricOpBase):
    """
    Hamming loss Metric.
    """
    _properties = {
        "is_for_summary": True,
        "is_for_best_keep": True,
        "is_for_log": True,
        "valid_input_data_parsers": [
            parser.MultiLabelClassificationDataParser,
            parser.MultiLabelDistillationDataParser,
        ],
        "summary_collection_key": BaseSummaries.KEY_TYPES.DEFAULT,
        "summary_value_type": BaseSummaries.VALUE_TYPES.PLACEHOLDER,
        "min_max_mode": "min",
    }

    def __str__(self):
        return "hamming_loss_metric"

    @overload
    def build_op(self,
                 data: parser.MultiLabelClassificationDataParser.OutputBuildData):
        key = f"hamming_loss/{data.dataset_split_name}"

        return {
            key: None
        }

    @overload
    def evaluate(self,
                 data: parser.MultiLabelClassificationDataParser.OutputNonTensorData):
        key = f"hamming_loss/{data.dataset_split_name}"
        metric = hamming_loss(data.labels_multihot, data.predictions_multihot)

        return {
            key: metric
        }

    @build_op.add
    def build_op(self,
                 data: parser.MultiLabelDistillationDataParser.OutputBuildData):
        key = f"hamming_loss/{data.dataset_split_name}"

        return {
            key: None
        }

    @evaluate.add
    def evaluate(self,
                 data: parser.MultiLabelDistillationDataParser.OutputNonTensorData):
        key = f"hamming_loss/{data.dataset_split_name}"
        metric = hamming_loss(data.labels_multihot, data.predictions_multihot)

        return {
            key: metric
        }


# -- Used for picai
class DiversityMetricOp(NonTensorMetricOpBase):
    """
    Diversity Metric
    """
    _properties = {
        "is_for_summary": True,
        "is_for_best_keep": True,
        "is_for_log": True,
        "valid_input_data_parsers": [
            parser.PicaiDataParser,
        ],
        "summary_collection_key": BaseSummaries.KEY_TYPES.DEFAULT,
        "summary_value_type": BaseSummaries.VALUE_TYPES.PLACEHOLDER,
        "min_max_mode": "max",
    }

    def __str__(self):
        return "diversity_metric"

    @overload
    def build_op(self,
                 data: parser.PicaiDataParser.OutputBuildData):
        key = f"diversity/{data.dataset_split_name}"

        return {
            key: None
        }

    @overload
    def evaluate(self,
                 data: parser.PicaiDataParser.OutputNonTensorData):
        key = f"diversity/{data.dataset_split_name}"
        metric = diversity(data.model_scores, data.true_scores)

        return {
            key: metric
        }


class JaccardDistanceMetricOp(NonTensorMetricOpBase):
    """
    Jaccard Distance Metric
    """
    _properties = {
        "is_for_summary": True,
        "is_for_best_keep": True,
        "is_for_log": True,
        "valid_input_data_parsers": [
            parser.PicaiDataParser,
        ],
        "summary_collection_key": BaseSummaries.KEY_TYPES.DEFAULT,
        "summary_value_type": BaseSummaries.VALUE_TYPES.PLACEHOLDER,
        "min_max_mode": "max",
    }

    def __str__(self):
        return "jaccard_distance_metric"

    @overload
    def build_op(self,
                 data: parser.PicaiDataParser.OutputBuildData):
        key = f"jaccard_distance/{data.dataset_split_name}"

        return {
            key: None
        }

    @overload
    def evaluate(self,
                 data: parser.PicaiDataParser.OutputNonTensorData):
        key = f"jaccard_distance/{data.dataset_split_name}"
        metric = jaccard_distance(data.model_scores, data.true_scores)

        return {
            key: metric
        }


class NDCGAt5MetricOp(NonTensorMetricOpBase):
    """
    NDCG at K Metric
    """
    _properties = {
        "is_for_summary": True,
        "is_for_best_keep": True,
        "is_for_log": True,
        "valid_input_data_parsers": [
            parser.PicaiDataParser,
        ],
        "summary_collection_key": BaseSummaries.KEY_TYPES.DEFAULT,
        "summary_value_type": BaseSummaries.VALUE_TYPES.PLACEHOLDER,
        "min_max_mode": "max",
    }

    def __str__(self):
        return "ndcg_at_5_metric"

    @overload
    def build_op(self,
                 data: parser.PicaiDataParser.OutputBuildData):
        key = f"ndcg_at_5/{data.dataset_split_name}"

        return {
            key: None
        }

    @overload
    def evaluate(self,
                 data: parser.PicaiDataParser.OutputNonTensorData):
        key = f"ndcg_at_5/{data.dataset_split_name}"
        merged_scores = pd.concat(
            [data.true_scores.stack(), data.model_scores.stack()],
            axis=1
        ).rename(columns={0: "score_mean", 1: "score_pred"})
        df_img_relevance_scores = merged_scores.groupby("image_id").apply(
            lambda x: x.sort_values(by=["score_pred"], ascending=False)["score_mean"].values + 1
        )
        metric = df_img_relevance_scores.apply(lambda x: ndcg_at_k(x, 5, 1)).mean()

        return {
            key: metric
        }


class NDCGAtAllMetricOp(NonTensorMetricOpBase):
    """
    NDCG at all Metric
    """
    _properties = {
        "is_for_summary": True,
        "is_for_best_keep": True,
        "is_for_log": True,
        "valid_input_data_parsers": [
            parser.PicaiDataParser,
        ],
        "summary_collection_key": BaseSummaries.KEY_TYPES.DEFAULT,
        "summary_value_type": BaseSummaries.VALUE_TYPES.PLACEHOLDER,
        "min_max_mode": "max",
    }

    def __str__(self):
        return "ndcg_at_all_metric"

    @overload
    def build_op(self,
                 data: parser.PicaiDataParser.OutputBuildData):
        key = f"ndcg_at_all/{data.dataset_split_name}"

        return {
            key: None
        }

    @overload
    def evaluate(self,
                 data: parser.PicaiDataParser.OutputNonTensorData):
        key = f"ndcg_at_all/{data.dataset_split_name}"
        merged_scores = pd.concat(
            [data.true_scores.stack(), data.model_scores.stack()],
            axis=1
        ).rename(columns={0: "score_mean", 1: "score_pred"})
        df_img_relevance_scores = merged_scores.groupby("image_id").apply(
            lambda x: x.sort_values(by=["score_pred"], ascending=False)["score_mean"].values + 1
        )
        metric = df_img_relevance_scores.apply(lambda x: ndcg_at_k(x, len(data.label_names), 1)).mean()

        return {
            key: metric
        }


# Refactor AccAt1lMetricOp, AccAt2lMetricOp, AccAt5lMetricOp to remove duplicates
class AccAt1MetricOp(NonTensorMetricOpBase):
    _properties = {
        "is_for_summary": True,
        "is_for_best_keep": True,
        "is_for_log": True,
        "valid_input_data_parsers": [
            parser.PicaiDataParser,
        ],
        "summary_collection_key": BaseSummaries.KEY_TYPES.DEFAULT,
        "summary_value_type": BaseSummaries.VALUE_TYPES.PLACEHOLDER,
        "min_max_mode": "max",
    }

    def __str__(self):
        return "acc_at_1_metric"

    @overload
    def build_op(self,
                 data: parser.PicaiDataParser.OutputBuildData):
        key = f"acc_at_1/{data.dataset_split_name}"

        return {
            key: None
        }

    @overload
    def evaluate(self,
                 data: parser.PicaiDataParser.OutputNonTensorData):
        key = f"acc_at_1/{data.dataset_split_name}"
        merged_scores = pd.concat(
            [data.true_scores.stack(), data.model_scores.stack()],
            axis=1
        ).rename(columns={0: "score_mean", 1: "score_pred"})
        df_img_relevance_rank = merged_scores.groupby("image_id").apply(
            lambda x: x.sort_values(by=["score_pred"], ascending=False)["score_mean"].rank(
                method="dense", ascending=False
            ).values
        )
        metric = df_img_relevance_rank.apply(lambda x: 1.0 in x[:1]).mean()

        return {
            key: metric
        }


class AccAt2MetricOp(NonTensorMetricOpBase):
    _properties = {
        "is_for_summary": True,
        "is_for_best_keep": True,
        "is_for_log": True,
        "valid_input_data_parsers": [
            parser.PicaiDataParser,
        ],
        "summary_collection_key": BaseSummaries.KEY_TYPES.DEFAULT,
        "summary_value_type": BaseSummaries.VALUE_TYPES.PLACEHOLDER,
        "min_max_mode": "max",
    }

    def __str__(self):
        return "acc_at_2_metric"

    @overload
    def build_op(self,
                 data: parser.PicaiDataParser.OutputBuildData):
        key = f"acc_at_2/{data.dataset_split_name}"

        return {
            key: None
        }

    @overload
    def evaluate(self,
                 data: parser.PicaiDataParser.OutputNonTensorData):
        key = f"acc_at_2/{data.dataset_split_name}"
        merged_scores = pd.concat(
            [data.true_scores.stack(), data.model_scores.stack()],
            axis=1
        ).rename(columns={0: "score_mean", 1: "score_pred"})
        df_img_relevance_rank = merged_scores.groupby("image_id").apply(
            lambda x: x.sort_values(by=["score_pred"], ascending=False)["score_mean"].rank(
                method="dense", ascending=False
            ).values
        )
        metric = df_img_relevance_rank.apply(lambda x: 1.0 in x[:2]).mean()

        return {
            key: metric
        }


class AccAt5MetricOp(NonTensorMetricOpBase):
    _properties = {
        "is_for_summary": True,
        "is_for_best_keep": True,
        "is_for_log": True,
        "valid_input_data_parsers": [
            parser.PicaiDataParser,
        ],
        "summary_collection_key": BaseSummaries.KEY_TYPES.DEFAULT,
        "summary_value_type": BaseSummaries.VALUE_TYPES.PLACEHOLDER,
        "min_max_mode": "max",
    }

    def __str__(self):
        return "acc_at_5_metric"

    @overload
    def build_op(self,
                 data: parser.PicaiDataParser.OutputBuildData):
        key = f"acc_at_5/{data.dataset_split_name}"

        return {
            key: None
        }

    @overload
    def evaluate(self,
                 data: parser.PicaiDataParser.OutputNonTensorData):
        key = f"acc_at_5/{data.dataset_split_name}"
        merged_scores = pd.concat(
            [data.true_scores.stack(), data.model_scores.stack()],
            axis=1
        ).rename(columns={0: "score_mean", 1: "score_pred"})
        df_img_relevance_rank = merged_scores.groupby("image_id").apply(
            lambda x: x.sort_values(by=["score_pred"], ascending=False)["score_mean"].rank(
                method="dense", ascending=False
            ).values
        )
        metric = df_img_relevance_rank.apply(lambda x: 1.0 in x[:5]).mean()

        return {
            key: metric
        }
