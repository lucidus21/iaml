import numpy as np
import tensorflow as tf
from termcolor import colored

import common.utils as utils

_available_decay_methods = ["auto", "clr", "predefined", "fixed", "exponential"]


def factory(args, log, global_step_from_checkpoint=None, global_step=None, dataset=None):
    if args.decay_method == "auto":
        # This Hyperparameters are tested on step_evaluation = 500
        learning_rate_scheduler = ReduceLROnPlateau(
            args, log,
            factor=args.lr_decay,
            patience=args.patience,
            epsilon=1e-5,
            cooldown=args.cooldown,
            min_lr=args.min_lr,
            decay_val=0.9,
        )
    elif args.decay_method == "clr":
        learning_rate_scheduler = CyclicLR(
            args, log,
            global_step_from_checkpoint,
            base_lr=args.base_lr,
            max_lr=args.max_lr,
            final_lr=args.final_lr,
            step_size=args.step_size,
            mode=args.mode,
            gamma=args.lr_gamma,
        )
    elif args.decay_method == "predefined":
        learning_rate_scheduler = PredefinedLR(
            args, log, global_step_from_checkpoint, global_step, dataset
        )
    elif args.decay_method == "fixed":
        learning_rate_scheduler = FixedLR(args, log)
    elif args.decay_method == "exponential":
        learning_rate_scheduler = ExponentialDecayLR(args, log, global_step, dataset)
    else:
        raise ValueError(f"Encountered invalid decay method: {args.decay_method}")

    return learning_rate_scheduler


def add_arguments(parser):
    BaseLRScheduler.add_arguments(parser)
    ReduceLROnPlateau.add_arguments(parser)
    CyclicLR.add_arguments(parser)
    PredefinedLR.add_arguments(parser)
    FixedLR.add_arguments(parser)
    ExponentialDecayLR.add_arguments(parser)


class BaseLRScheduler(object):
    def __init__(self, args, logger):
        self.args = args
        self.logger = logger
        assert hasattr(self.args, "learning_rate") and isinstance(self.args.learning_rate, float)
        self.learning_rate = self.args.learning_rate

    def update_on_start_of_step(self, *args, **kwargs):
        raise NotImplementedError

    def update_on_end_of_step(self, *args, **kwargs):
        raise NotImplementedError

    def update_on_start_of_evaluation(self, *args, **kwargs):
        raise NotImplementedError

    def update_on_end_of_evaluation(self, *args, **kwargs):
        raise NotImplementedError

    def should_stop_training(self):
        # Whether to stop training after updating the learning rate.
        raise NotImplementedError

    @staticmethod
    def add_arguments(parser):
        g_lr = parser.add_argument_group("(BaseLRScheduler) Learning Rate Decay Arguments")
        g_lr.add_argument("--learning_rate", default=1e-4, type=float, help="Initial learning rate for gradient update")
        g_lr.add_argument("--decay_method", default="fixed", type=str,
                          choices=_available_decay_methods)


class ReduceLROnPlateau(BaseLRScheduler):
    def __init__(
        self, args, logger,
        factor=0.33, patience=10, epsilon=1e-5, cooldown=6, min_lr=1e-8, decay_val=0.9
    ):
        super().__init__(args, logger)
        self.placeholder = tf.placeholder(tf.float32, name="learning_rate_placeholder")
        self.should_feed_dict = True

        self.factor = factor
        self.patience = patience
        self.epsilon = epsilon
        self.cooldown = cooldown
        self.min_lr = min_lr
        self.decay_val = decay_val

        self.cooldown_counter = 0  # Cooldown counter.
        self.wait = 0  # Refactor as Class
        self.best = np.inf
        self.moving_monitor_val = None

    def update_on_start_of_step(self, *args, **kwargs):
        pass

    def update_on_end_of_step(self, *args, **kwargs):
        pass

    def update_on_start_of_evaluation(self, *args, **kwargs):
        pass

    def update_on_end_of_evaluation(self, monitor_val, *args, **kwargs):
        if self.moving_monitor_val is None:
            self.moving_monitor_val = monitor_val
        else:
            self.moving_monitor_val = self.decay_val * self.moving_monitor_val + (1 - self.decay_val) * monitor_val

        in_cooldown = self.cooldown_counter > 0
        if in_cooldown:
            self.cooldown_counter -= 1
            self.wait = 0

        # to only focus on significant changes
        if self.moving_monitor_val + self.epsilon < self.best:
            self.best = self.moving_monitor_val
            self.wait = 0
        elif not in_cooldown:
            if self.wait >= self.patience:
                if self.learning_rate > self.min_lr:
                    new_lr = self.learning_rate * self.factor
                    new_lr = max(new_lr, self.min_lr)
                    self.learning_rate = new_lr
                    self.cooldown_counter = self.cooldown
                    self.wait = 0
            self.wait += 1

        self.logger.debug("Learning rate updated")
        self.logger.debug(f"v {monitor_val} " +
                          f"moving_v {self.moving_monitor_val} " +
                          f"wait {self.wait} " +
                          f"cooldown_counter {self.cooldown_counter} " +
                          f"best {self.best} " +
                          f"learning_rate {self.learning_rate} ")

    def should_stop_training(self):
        return self.learning_rate <= self.min_lr

    @staticmethod
    def add_arguments(parser):
        g_lr_auto = parser.add_argument_group("(ReduceLROnPlateau) Auto Learning Rate Decay Arguments")
        g_lr_auto.add_argument("--lr_decay", default=0.33, type=float,
                               help=("decay_method: auto > factor by which the learning rate will be reduced. "
                                     "new_lr = lr * factor"))
        g_lr_auto.add_argument("--patience", default=15, type=utils.positive_int,
                               help=("decay_method: auto > patience * step_evaluation with no improvement "
                                     "after which learning rate will be reduced."))
        g_lr_auto.add_argument("--cooldown", default=10, type=utils.positive_int,
                               help=("decay_method: auto > cooldown * step_evaluation, "
                                     "number of epochs to wait before resuming normal operation "
                                     "after lr has been reduced."))
        g_lr_auto.add_argument("--min_lr", default=1e-8, type=float,
                               help="decay_method: auto > Minimum learning rate to stop training")


class CyclicLR(BaseLRScheduler):
    def __init__(
        self, args, logger, global_step,
        base_lr=0.001, max_lr=0.006, final_lr=0.00001,
        step_size=2000., mode="triangular", gamma=1., scale_fn=None
    ):
        super().__init__(args, logger)
        self.placeholder = tf.placeholder(tf.float32, name="learning_rate_placeholder")
        self.should_feed_dict = True
        self.learning_rate = base_lr

        self.global_step = global_step
        self.base_lr = base_lr
        self.max_lr = max_lr
        self.final_lr = final_lr
        self.step_size = step_size
        self.mode = mode
        self.gamma = gamma

        if scale_fn is None:
            if self.mode == "triangular" or self.mode == "one_cycle":
                self.scale_fn = lambda x: 1.
            elif self.mode == "triangular2":
                self.scale_fn = lambda x: 1/(2.**(x-1))
            elif self.mode == "exp_range":
                self.scale_fn = lambda x: gamma**(x)
        else:
            self.scale_fn = scale_fn

        self._reset()
        if global_step is not None:
            self.clr_iterations = global_step

    def _reset(self, new_base_lr=None, new_max_lr=None, new_step_size=None):
        """Resets cycle iterations.
        Optional boundary/step size adjustment.
        """
        if new_base_lr is not None:
            self.base_lr = new_base_lr
        if new_max_lr is not None:
            self.max_lr = new_max_lr
        if new_step_size is not None:
            self.step_size = new_step_size
        self.clr_iterations = 0.

    def _clr(self):
        cycle = np.floor(1+self.clr_iterations/(2*self.step_size))
        x = np.abs(self.clr_iterations/self.step_size - 2*cycle + 1)

        if cycle > 1 and self.mode == "one_cycle":
            return self.final_lr
        else:
            return self.base_lr + (self.max_lr-self.base_lr)*np.maximum(0, (1-x))*self.scale_fn(cycle)

    def update_on_start_of_step(self, *args, **kwargs):
        self.learning_rate = self._clr()
        self.clr_iterations += 1
        self.logger.debug("Learning rate updated")
        self.logger.debug(f"clr_iterations {self.clr_iterations} " +
                          f"learning_rate {self.learning_rate} ")

    def update_on_end_of_step(self, *args, **kwargs):
        pass

    def update_on_start_of_evaluation(self, *args, **kwargs):
        pass

    def update_on_end_of_evaluation(self, *args, **kwargs):
        pass

    def should_stop_training(self):
        return False

    @staticmethod
    def add_arguments(parser):
        g_lr_cyclic = parser.add_argument_group("(CyclicLR) Cyclic Learning Rate Decay Arguments")
        g_lr_cyclic.add_argument("--base_lr", default=0.0001, type=float,
                                 help=("decay_method: clr > initial learning rate "
                                       "which is the lower boundary in the cycle"))
        g_lr_cyclic.add_argument("--max_lr", default=0.001, type=float,
                                 help=("decay_method: clr > upper boundary in the cycle. Functionally,"
                                       "it defines the cycle amplitude (max_lr - base_lr)."
                                       "The lr at any cycle is the sum of base_lr"
                                       "and some scaling of the amplitude; therefore"
                                       "max_lr may not actually be reached depending on"
                                       "scaling function."))
        g_lr_cyclic.add_argument("--step_size", default=2000., type=float,
                                 help=("decay_method: clr > number of training iterations per half cycle. "
                                       "Authors suggest setting step_size 2-8 x training iterations in epoch."))
        g_lr_cyclic.add_argument("--lr_gamma", default=1.0, type=float,
                                 help="dacaying rate of max_lr")
        g_lr_cyclic.add_argument("--mode", default="triangular", type=str,
                                 choices=["triangular", "triangular2", "exp_range", "one_cycle"],
                                 help="decay_method: clr > one of {triangular, triangular2, exp_range, one_cycle}")
        g_lr_cyclic.add_argument("--final_lr", default=0.0001, type=float,
                                 help=("decay_method: clr > final rate for one cycle"))


class PredefinedLR(BaseLRScheduler):
    def __init__(
        self, args, logger, global_step_from_checkpoint, global_step, dataset
    ):
        super().__init__(args, logger)
        if self.args.boundaries_epoch:
            boundaries = [b * dataset.num_samples // dataset.batch_size for b in self.args.boundaries]
        else:
            boundaries = self.args.boundaries

        if self.args.relative:
            self.boundaries = [global_step_from_checkpoint + b for b in boundaries]
            self.logger.info(colored(
                (f"global_step starts with {global_step_from_checkpoint},"
                 f" so, update boundaries {boundaries} to {self.boundaries}"),
                "yellow",
                attrs=["underline"]))
        else:
            self.boundaries = boundaries

        self.placeholder = tf.train.piecewise_constant(
            global_step, self.boundaries, self.args.lr_list
        )
        self.should_feed_dict = False

    def update_on_start_of_step(self, *args, **kwargs):
        pass

    def update_on_end_of_step(self, *args, **kwargs):
        pass

    def update_on_start_of_evaluation(self, *args, **kwargs):
        pass

    def update_on_end_of_evaluation(self, *args, **kwargs):
        pass

    def should_stop_training(self):
        return False

    @staticmethod
    def add_arguments(parser):
        g_lr_predefined = parser.add_argument_group("(PredefinedLR) Predefined Learning Rate Decay Arguments")
        g_lr_predefined.add_argument("--boundaries", default=[100000, 200000], type=int, nargs="*",
                                     help=("decay_method: predefined > global_step boundaries"
                                           "for piecewise_constant decay. If restoring from checkpoint, "
                                           "boundaries is shifted by checkpoint's global_step"))
        g_lr_predefined.add_argument("--boundaries_epoch", dest="boundaries_epoch", action="store_true",
                                     help="Given boundaries are implicitly considered in epoch units. "
                                     "If you want to use step boundaries, apply --no-boundaries-epoch "
                                     "argument.")
        g_lr_predefined.add_argument("--no-boundaries_epoch", dest="boundaries_epoch", action="store_false")
        g_lr_predefined.add_argument("--lr_list", default=[1e-3, 1e-4, 1e-5], type=float, nargs="*",
                                     help="decay_method: predefined > learning rate values for each region")
        g_lr_predefined.add_argument("--relative_schedule", dest="relative", action="store_true")
        g_lr_predefined.add_argument("--absolute_schedule", dest="relative", action="store_false",
                                     help="decay_method: predefined > choose whether to use the "
                                          "boundary values in relative (to predefined global step) "
                                          "or absolute manner. For example, given boundaries=[10, 20] "
                                          "and global_step_from_checkpoint=10, with --relative_schedule "
                                          "the schedule will shifted to [20, 30] in global manner "
                                          "where with --absolute_schedule the boundary remains to [10, 20] "
                                          "so the first scheudle begins immediately.")
        g_lr_predefined.set_defaults(relative=True, boundaries_epoch=True)


class FixedLR(BaseLRScheduler):
    def __init__(
        self, args, logger
    ):
        super().__init__(args, logger)
        self.placeholder = self.learning_rate
        self.should_feed_dict = False

    def update_on_start_of_step(self, *args, **kwargs):
        pass

    def update_on_end_of_step(self, *args, **kwargs):
        pass

    def update_on_start_of_evaluation(self, *args, **kwargs):
        pass

    def update_on_end_of_evaluation(self, *args, **kwargs):
        pass

    def should_stop_training(self):
        return False

    @staticmethod
    def add_arguments(parser):
        pass


class ExponentialDecayLR(BaseLRScheduler):
    def __init__(self, args, logger, global_step, dataset):
        super().__init__(args, logger)

        self.global_step = global_step
        self.decay_steps = int(dataset.num_samples / dataset.batch_size * self.args.decay_epochs)

        self.placeholder = tf.train.exponential_decay(
            self.learning_rate,
            self.global_step,
            self.decay_steps,
            self.args.lr_decay_rate,
            staircase=self.args.staircase_decay,
            name="learning_rate_placeholder")

        self.should_feed_dict = False

    def update_on_start_of_step(self, *args, **kwargs):
        pass

    def update_on_end_of_step(self, *args, **kwargs):
        pass

    def update_on_start_of_evaluation(self, *args, **kwargs):
        pass

    def update_on_end_of_evaluation(self, *args, **kwargs):
        pass

    def should_stop_training(self):
        # Whether to stop training after updating the learning rate.
        return False

    @staticmethod
    def add_arguments(parser):
        g_lr_exponential = parser.add_argument_group("(ExponentialDecayLR) Exponential decay Arguments")
        g_lr_exponential.add_argument("--lr_decay_rate", default=0.94, type=float,
                                      help="lr = init_lr * lr_decay_rate ^ (step / decay_step)")
        g_lr_exponential.add_argument("--decay_epochs", default=2.5, type=float,
                                      help="Number of epochs to apply decay")
        g_lr_exponential.add_argument("--staircase_decay", dest="staircase_decay", action="store_true",
                                      help="Apply decay in staircase manner")
        g_lr_exponential.add_argument("--no-staircase_decay", dest="staircase_decay", action="store_false")
        g_lr_exponential.set_defaults(staircase_decay=True)
