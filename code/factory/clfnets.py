# -*- coding: utf-8 -*-
import sys
sys.path.append("../submodules/models/research/slim/")
from abc import ABC
from abc import abstractmethod
from types import SimpleNamespace
from typing import Dict
from typing import Tuple

import tensorflow as tf
import tensorflow.contrib.slim as slim
from tensorflow.contrib.layers import conv2d
from tensorflow.contrib.layers import batch_norm
from tensorflow.contrib.layers import avg_pool2d
from tensorflow.contrib.layers import max_pool2d
from tensorflow.contrib.framework.python.ops import variables
from termcolor import colored

import clfnets.resnet_utils as resnet_utils
import clfnets.resnext_utils as resnext_utils
import clfnets.inception_resnet_v2 as inception_resnet_v2
import clfnets.inception as inception
import clfnets.mobilenet_v1 as mobilenet_v1
import clfnets.mobilenet_v2 as mobilenet_v2
import common.tf_utils as tf_utils
import common.utils as utils
from factory.base import CNNModel
from clfnets.resnet_v1 import resnet_v1_152
from clfnets.resnet_v1 import resnet_v1_101
from clfnets.resnext import resnext_101
from clfnets.resnext import se_resnext_101
#from clfnets.slim_nets.mobilenet import mobilenet_v2 as mobilenet_v2_slim
from factory.tasks import SingleTask
from factory.tasks import SingleBCTask
from factory.tasks import MultiLabelTask
from factory.tasks import MultiTaskExclusive


_available_nets = [
    "InceptionV2",
    "InceptionResnetV2",
    "SafeMatchMobileV2",
    "Resnet152PlacesModel",
    "Resnet101",
    "Resnext101",
    "SEResnext101",
    "MobileNetV1",
    "MultiMobileNetV1",
    "MobileNetV2",
    "MultiMobileNetV2",
    "SlimMobileNetV2",
]

_available_tasks = {
    "single": SingleTask,
    "single_bc": SingleBCTask,
    "multilabel": MultiLabelTask,
    "multitask_exclusive": MultiTaskExclusive,
}


class ClassifierNetModel(CNNModel):
    def __init__(self, args, dataset=None):
        self.log = utils.get_logger("ClassifierNetModel")
        self.args = args
        self.dataset = dataset  # used to access data created in DataWrapper

        # output_type is defined in factory/tasks.py
        self.TASK_TYPE = SimpleNamespace(
            single="single",
            single_bc="single_bc",
            multilabel="multilabel",
            multitask_exclusive="multitask_exclusive",
        )

        self.task_type = self.args.task_type
        self.task = _available_tasks[self.task_type](self.args, self.dataset)

    def build(
        self,
        images_original: tf.Tensor,
        images: tf.Tensor,
        onehot_labels: tf.Tensor,
        is_training: tf.placeholder=None,
    ):
        self._images_original = images_original
        self._images = images
        self._onehot_labels = onehot_labels
        self.is_training = is_training

        # -- * -- build_output: it includes build_inference, it can be called in
        inputs, self._logits, self._scores, self.endpoints = self.task.build_output(
            self.build_output,
            self.images,
            self.is_training,
            self.args.output_name,
        )

        # -- * -- Build loss function: it can be different between each models
        self._total_loss, self._model_loss, self.endpoints_loss = self.build_loss(
            self.logits,
            self.scores,
            self.onehot_labels,
        )

        self.total_params = tf_utils.show_models(self.log)

        if self.args.verbosity >= 1:
            slim.model_analyzer.analyze_ops(
                tf.get_default_graph(), print_info=True
            )

    @property
    def total_loss(self):
        return self._total_loss

    @property
    def model_loss(self):
        return self._model_loss

    @property
    def scores(self):
        return self._scores

    @property
    def logits(self):
        return self._logits

    def build_output(
        self,
        images: tf.Tensor,
        is_training: tf.placeholder,
        output_name: str,
    ) -> [tf.Tensor, tf.Tensor, tf.Tensor, Dict]:
        """Called in `ClaassifierNetModel.build()` and in `add_inout_to_pbtxt` at `convert.py`.
        """
        inputs = self.preprocess_images(
            images,
            preprocess_method=self.args.preprocess_method,
        )
        logits, endpoints = self.build_inference(
            inputs,
            is_training=is_training,
        )
        outputs = self.task.convert(logits, output_name)
        return inputs, logits, outputs, endpoints

    def build_inference(self, inputs, is_training):
        raise NotImplementedError

    def build_summaries(self, dataset_name, collections):
        pass

    @property
    def onehot_labels(self):
        return self._onehot_labels

    @property
    def images_original(self):
        return self._images_original

    @property
    def images(self):
        return self._images

    @staticmethod
    def add_arguments(parser):
        parser.add_argument("--weight_loss", default=1.0, type=float)

    def build_loss(
        self,
        logits: tf.Tensor,
        scores: tf.Tensor,
        labels: tf.Tensor,
    ) -> Tuple[tf.Tensor, tf.Tensor, Dict]:
        """`build_loss` does not have to be implemented in descedant classes. It is expected to be
        used only in special cases when custom loss is necessary.
        """
        total_loss, model_loss, endpoints_loss = self.task.build_loss(
            logits,
            scores,
            labels,
        )
        return total_loss, model_loss, endpoints_loss


class Resnet152PlacesModel(ClassifierNetModel):
    """Tried to use caffe's parameter, but it failed.
    If we need, then let's make our own caffe converter by ourself.
    """
    def __init__(self, args, dataset=None):
        super().__init__(args, dataset)

    @staticmethod
    def add_arguments(parser):
        g_clf = parser.add_argument_group("(Resnet152PlacesModel) Arguments")
        g_clf.add_argument("--weight_decay", default=0.0001, type=float)
        g_clf.add_argument("--np_weight_path", default="", type=str,
                           help=(
                               "e.g. ../../caffe-zoo/resnet152_hybrid1365.npy, "
                               "if it is given, then it uses npy weight"
                           ))

    def build_inference(self, images, is_training):
        scope = "Resnet152PlacesModel"
        with slim.arg_scope(resnet_utils.resnet_arg_scope(weight_decay=self.args.weight_decay)):
            num_classes = 365
            assert 224 in images.get_shape().as_list(), "Use (width, height) = (224, 224) for Resnet152PlacesModel"
            logits, endpoints = resnet_v1_152(
                images,
                num_classes=num_classes,
                is_training=is_training,
                global_pool=True,
                reuse=tf.AUTO_REUSE,
                scope=scope
            )
            logits = tf.reshape(logits, [-1, num_classes])

        if self.args.np_weight_path != "":
            tf_vars_and_shape = [
                (v.name.split(":")[0], tuple(v.get_shape().as_list()))
                for v in tf.contrib.framework.get_variables(scope=scope)
            ]
            # {{{ !- Hardcoded preprocessing for caffe's weight parameters
            # Caffe's Resnet152 use [2048, 1365] but slim's Resnet152 use [1, 1, 2048, 1365]
            np_vars_to_value = tf_utils.load_from_caffe_array(self.args.np_weight_path)
            # np_vars_to_value["fc1365/weights"] = np.reshape(np_vars_to_value["fc1365/weights"], [1, 1, 2048, 1365])
            known_map = dict(zip(
                ["Resnet152PlacesModel/conv1/BatchNorm/moving_mean",
                 "Resnet152PlacesModel/conv1/BatchNorm/moving_variance",
                 "Resnet152PlacesModel/conv1/BatchNorm/beta",
                 "Resnet152PlacesModel/conv1/BatchNorm/gamma"],
                ["bn_conv1/mean", "bn_conv1/variance", "bn_conv1/offset", "bn_conv1/scale"]
            ))
            # - ! }}}
            np_vars_and_shape = [(k, v.shape) for k, v in np_vars_to_value.items()]
            name_match_map = tf_utils.guess_and_match_np_vars_and_tf_vars(
                np_vars_and_shape, tf_vars_and_shape, known_map
            )
            self.var_names_to_values = {
                tf_name: np_vars_to_value[np_name] for tf_name, np_name in name_match_map.items()
            }
        return logits, endpoints


class InceptionV2(ClassifierNetModel):
    def __init__(self, args, dataset=None):
        super().__init__(args, dataset)

    @staticmethod
    def add_arguments(parser):
        parser.add_argument("--no-use_batch_norm", dest="use_batch_norm", action="store_false")
        parser.add_argument("--use_batch_norm", dest="use_batch_norm", action="store_true")
        parser.set_defaults(use_batch_norm=True)
        parser.add_argument("--no-spatial_squeeze", dest="spatial_squeeze", action="store_false")
        parser.add_argument("--spatial_squeeze", dest="spatial_squeeze", action="store_true")
        parser.set_defaults(spatial_squeeze=True)
        parser.add_argument("--weight_decay", default=0.00004, type=float)
        parser.add_argument("--batch_norm_decay", default=0.9997, type=float)
        parser.add_argument("--batch_norm_epsilon", default=0.001, type=float)
        parser.add_argument("--dropout_keep_prob", default=0.8, type=float)
        parser.add_argument("--min_depth", default=16, type=int)
        parser.add_argument("--depth_multiplier", default=1.0, type=float)

    def build_inference(self, images, is_training):
        with slim.arg_scope(inception.inception_v2_arg_scope(
            weight_decay=self.args.weight_decay,
            use_batch_norm=self.args.use_batch_norm,
            batch_norm_decay=self.args.batch_norm_decay,
            batch_norm_epsilon=self.args.batch_norm_epsilon
        )):
            logits, endpoints = inception.inception_v2(
                images,
                num_classes=self.args.num_classes,
                is_training=is_training,
                dropout_keep_prob=self.args.dropout_keep_prob,
                min_depth=self.args.min_depth,  # 16
                depth_multiplier=self.args.depth_multiplier,  # 1.0,
                prediction_fn=slim.softmax,
                spatial_squeeze=self.args.spatial_squeeze,  # True
                reuse=None,
                scope=self.args.model
            )
            return logits, endpoints


class InceptionResnetV2(ClassifierNetModel):
    def __init__(self, args, dataset=None):
        super().__init__(args, dataset)

    @staticmethod
    def add_arguments(parser):
        parser.add_argument("--weight_decay", default=0.00004, type=float)
        parser.add_argument("--batch_norm_decay", default=0.9997, type=float)
        parser.add_argument("--batch_norm_epsilon", default=0.001, type=float)
        parser.add_argument("--dropout_keep_prob", default=0.8, type=float)

    def build_inference(self, images, is_training):
        with slim.arg_scope(inception_resnet_v2.inception_resnet_v2_arg_scope(
            weight_decay=self.args.weight_decay,
            batch_norm_decay=self.args.batch_norm_decay,
            batch_norm_epsilon=self.args.batch_norm_epsilon
        )):
            with slim.arg_scope([slim.batch_norm], is_training=is_training, fused=True):
                logits, endpoints = inception_resnet_v2.inception_resnet_v2(
                    images,
                    num_classes=self.args.num_classes,
                    is_training=is_training,
                    dropout_keep_prob=self.args.dropout_keep_prob,
                    reuse=None,
                    scope=self.args.model
                )
            return logits, endpoints

    def build_loss(
        self,
        logits: tf.Tensor,
        scores: tf.Tensor,
        onehot_labels: tf.Tensor,
    ):
        endpoints_loss = {}
        endpoints_loss["aux_loss"] = tf.losses.softmax_cross_entropy(
            logits=self.endpoints["AuxLogits"],
            onehot_labels=onehot_labels,
            label_smoothing=0,
            weights=0.4,
            scope="aux_loss",
        )
        endpoints_loss["classification_loss"] = tf.losses.softmax_cross_entropy(
            logits=logits,
            onehot_labels=onehot_labels,
            label_smoothing=0,
            weights=1.0,
        )
        total_loss = endpoints_loss["aux_loss"] + endpoints_loss["classification_loss"]

        # Get total_loss by Tensorflow collection
        total_loss = tf.losses.get_total_loss(add_regularization_losses=True)
        model_loss = tf.losses.get_total_loss(add_regularization_losses=False)

        return total_loss, model_loss, endpoints_loss

    def build_debug(self, summarize=20, first_n=30):
        """ Add adhoc debugging by summary & tf.Print """
        # Batch Norm..
        batch_norm_params = tf.get_collection("batch_norm_params")
        for batch_norm_param in (batch_norm_params[:3] + batch_norm_params[-3:]):
            var_name = batch_norm_param.name.split(":")[0]
            batch_norm_param = tf.Print(
                batch_norm_param,
                [batch_norm_param],
                "Debug value of %s: " % (var_name),
                summarize=summarize,
                first_n=first_n,
                name="print/" + var_name  # :0 is not allowed in name
            )
            # FIXME histogram 에 print 의 이름이 들어감..
            tf.summary.histogram("hist/" + var_name, batch_norm_param)
        # Logits..
        var_name = self.endpoints["Logits"].name.split(":")[0]
        self.endpoints["Logits"] = tf.Print(
            self.endpoints["Logits"],
            [self.endpoints["Logits"]],
            "Debug value of %s: " % (var_name),
            summarize=summarize,
            first_n=first_n,
            name="print/" + var_name
        )
        tf.summary.histogram("hist/" + var_name, self.endpoints["Logits"])


class SafeMatchMobileV2(ClassifierNetModel):
    def __init__(self, args, dataset=None):
        super().__init__(args, dataset)
        self._fire_module_count = 0

    @staticmethod
    def add_arguments(parser):
        parser.add_argument("--squeeze_ratio", default=0.75, type=float)
        parser.add_argument("--base_e", default=128, type=int)
        parser.add_argument("--incr", default=128, type=int)
        parser.add_argument("--freq", default=2, type=int)
        parser.add_argument("--decay", default=0.99, type=float)

    def calc_num_expand_filters(self, i):
        res = self.args.base_e + self.args.incr * int(i / self.args.freq)
        assert int(res) == res, "num of filters must be integer: %d" % res
        return res

    def fire_module_relu_auto(self, inputs, scope=None):
        # use relu instead of stacking batch normalization
        squeeze_depth = self.calc_num_expand_filters(self._fire_module_count) * self.args.squeeze_ratio
        expand_depth = self.calc_num_expand_filters(self._fire_module_count)
        assert int(squeeze_depth) == squeeze_depth, "squeeze_depth must be int: %f" % (squeeze_depth)
        assert int(expand_depth) == expand_depth, "expand_depth must be int: %f" % (expand_depth)
        squeeze_depth, expand_depth = int(squeeze_depth), int(expand_depth)
        self._fire_module_count += 1

        with tf.variable_scope(scope):
            net = self.squeeze_relu(inputs, squeeze_depth)
            outputs = self.expand_relu(net, expand_depth)
            return outputs

    @staticmethod
    def squeeze_relu(inputs, num_outputs):
        net = conv2d(inputs, num_outputs, kernel_size=1, stride=1,
                     padding="SAME", activation_fn=tf.nn.relu, scope="squeeze_relu")
        return net

    @staticmethod
    def expand_relu(inputs, num_outputs):
        assert num_outputs % 2 == 0, "num_outputs must be divided by 2 : %d" % (num_outputs)
        with tf.variable_scope("expand_relu"):
            e1x1 = conv2d(inputs, num_outputs // 2, kernel_size=1, stride=1,
                          padding="SAME", activation_fn=tf.nn.relu, scope="1x1")
            e3x3 = conv2d(inputs, num_outputs // 2, kernel_size=3, stride=1,
                          padding="SAME", activation_fn=tf.nn.relu, scope="3x3")
            return tf.concat(axis=3, values=[e1x1, e3x3])

    def build_inference(self, images, is_training):
        endpoints = {}
        with tf.variable_scope(self.args.model):
            # default option of tf.contrib.layers.conv2d padding is SAME
            with slim.arg_scope([slim.batch_norm],
                                is_training=is_training, fused=self.args.use_fused_batchnorm):
                self.net_conv1 = conv2d(images, 96, 7, stride=2, scope="conv1")
                self.net_conv1_bn = batch_norm(self.net_conv1,
                                               decay=self.args.decay, center=True, scale=True,
                                               activation_fn=tf.nn.relu, scope="conv1_bn")
                self.net_pool1 = max_pool2d(self.net_conv1_bn, 3, stride=2, padding="SAME", scope="pool1")
                self.net_fire2 = self.fire_module_relu_auto(self.net_pool1, scope="fire2")
                self.net_fire3 = self.fire_module_relu_auto(self.net_fire2, scope="fire3")
                self.net_fire3_bypass = tf.add(self.net_fire3, self.net_fire2, name="bypass_f2_to_f3")
                self.net_fire4 = self.fire_module_relu_auto(self.net_fire3_bypass, scope="fire4")
                self.net_maxpool4 = max_pool2d(self.net_fire4, 3, stride=2, padding="SAME", scope="maxpool4")
                self.net_fire5 = self.fire_module_relu_auto(self.net_maxpool4, scope="fire5")
                self.net_fire5_bypass = tf.add(self.net_fire5, self.net_maxpool4, name="bypass_p4_to_f5")
                self.net_fire6 = self.fire_module_relu_auto(self.net_fire5_bypass, scope="fire6")
                self.net_fire7 = self.fire_module_relu_auto(self.net_fire6, scope="fire7")
                self.net_fire7_bypass = tf.add(self.net_fire7, self.net_fire6, name="bypass_f6_to_f7")
                self.net_fire8 = self.fire_module_relu_auto(self.net_fire7_bypass, scope="fire8")
                self.net_maxpool8 = max_pool2d(self.net_fire8, 3, stride=2, padding="SAME", scope="maxpool8")
                self.net_fire9 = self.fire_module_relu_auto(self.net_maxpool8, scope="fire9")
                self.net_fire9_bypass = tf.add(self.net_fire9, self.net_maxpool8, name="bypass_p8_to_f9")
                self.net_conv10 = conv2d(self.net_fire9_bypass, self.args.num_classes, 1, stride=1, scope="conv10")
                net_conv10_shape = self.net_conv10.get_shape().as_list()
                self.net_avgpool10 = avg_pool2d(self.net_conv10,
                                                kernel_size=[net_conv10_shape[1], net_conv10_shape[2]], stride=1,
                                                padding="VALID", scope="avgpool10")
                logits = tf.reshape(self.net_avgpool10, [-1, self.args.num_classes], name="logits")
                return logits, endpoints

    def build_debug(self, summarize=20, first_n=30):
        assert False, "DEPRECATED! This method would probably crash anyway."
        """ Add adhoc debugging by summary & tf.Print """
        global_step = variables.get_or_create_global_step()
        _global_step = tf.Print(global_step, [global_step], "global_step : ",
                                summarize=summarize, first_n=first_n, name="print/global_step")
        tf.summary.histogram("global_step", _global_step)

        self.logits = tf.Print(self.logits, [self.logits], "Logits : ",
                               summarize=summarize, first_n=first_n, name="print/logits")
        tf.summary.histogram("logits", self.logits)

        self.images = tf.Print(self.images, [tf.reduce_mean(self.images, 1)], "images : ",
                               summarize=summarize, first_n=first_n, name="print/images")
        tf.summary.histogram("images", self.images)
        tf.summary.image("images", self.images, max_outputs=4, collections=None)

        self.images = tf.Print(self.images, [self.images], "images(part) : ",
                               summarize=summarize, first_n=first_n, name="print/images_part")
        tf.summary.histogram("images(part)", self.images)

        self.onehot_labels = tf.Print(self.onehot_labels, [self.onehot_labels], "onehot_labels : ",
                                      summarize=summarize, first_n=first_n, name="print/onehot_labels")
        tf.summary.histogram("onehot_labels", self.onehot_labels)

        self.loss = tf.Print(self.loss, [self.loss], "loss : ",
                             summarize=summarize, first_n=first_n, name="print/loss")
        tf.summary.histogram("loss", self.loss)

        varnames = [
            "SafeMatchMobileV2/conv1/weights",
            "SafeMatchMobileV2/conv1_bn/moving_mean",
            "SafeMatchMobileV2/conv1_bn/moving_variance",
            "SafeMatchMobileV2/conv1_bn/beta",
            "SafeMatchMobileV2/conv1_bn/gamma",
            "SafeMatchMobileV2/conv10/weights",
            "SafeMatchMobileV2/conv10/biases"
        ]
        for varname in varnames:
            tfvar = tf.contrib.framework.get_model_variables(varname)[0]
            tf.summary.histogram(varname, tf.Print(tfvar, [tfvar], "{} : ".format(varname),
                                                   summarize=summarize, first_n=first_n,
                                                   name="print/{}".format(varname)))

        activations = [
            self.net_fire2,
            self.net_fire4,
            self.net_maxpool4,
            self.net_fire6,
            self.net_fire8,
            self.net_maxpool8,
            self.net_fire9,
            self.net_fire9_bypass,
            self.net_avgpool10,
            self.net_conv10,
            self.logits
        ]

        for activation in activations:
            tf.summary.histogram(
                "histogram/" + activation.name[:-2],
                tf.Print(activation, [tf.reduce_mean(activation)], "{} : ".format(activation.name),
                         summarize=summarize, first_n=first_n, name="print/{}".format(activation.name[:-2]))
            )


class MobileNetV1(ClassifierNetModel):
    def __init__(self, args, dataset=None):
        super().__init__(args, dataset)

    @staticmethod
    def add_arguments(parser):
        parser.add_argument("--depth_multiplier", default=1.0, type=float)
        parser.add_argument("--weight_decay", default=0.00004, type=float)
        parser.add_argument("--batch_norm_decay", default=0.9997, type=float)
        parser.add_argument("--batch_norm_epsilon", default=0.001, type=float)
        parser.add_argument("--no-regularize_depthwise", dest="regularize_depthwise", action="store_false")
        parser.add_argument("--regularize_depthwise", dest="regularize_depthwise", action="store_true",
                            help="""Whether or not apply regularization on depthwise.
                            In A Quantization-Friendly Separable Convolution for MobileNets paper from qualcomm,
                            They apply l2 regularization for all depthwise layers
                            """)
        parser.set_defaults(regularize_depthwise=False)

    def build_inference(self, images, is_training):
        with slim.arg_scope(mobilenet_v1.mobilenet_v1_arg_scope(is_training=is_training,
                                                                use_fused_batchnorm=self.args.use_fused_batchnorm,
                                                                weight_decay=self.args.weight_decay,
                                                                batch_norm_decay=self.args.batch_norm_decay,
                                                                batch_norm_epsilon=self.args.batch_norm_epsilon,
                                                                regularize_depthwise=self.args.regularize_depthwise,
                                                                )):
            kwargs = {"num_classes": self.args.num_classes,
                      "is_training": is_training,
                      "depth_multiplier": self.args.depth_multiplier}

            if self.args.task_type == self.TASK_TYPE.multilabel:
                kwargs["prediction_fn"] = tf.sigmoid

            logits, endpoints = mobilenet_v1.mobilenet_v1(images, **kwargs)

            return logits, endpoints


class Resnet101(ClassifierNetModel):
    def __init__(self, args, dataset=None):
        super().__init__(args, dataset)

    @staticmethod
    def add_arguments(parser):
        pass

    def build_inference(self, images, is_training):
        with slim.arg_scope(resnet_utils.resnet_arg_scope()):
            logits, endpoints = resnet_v1_101(images,
                                              self.args.num_classes,
                                              is_training=is_training)

            logits = tf.squeeze(logits, name="SpatialSqueeze")

            return logits, endpoints


class Resnext101(ClassifierNetModel):
    def __init__(self, args, dataset=None):
        super().__init__(args, dataset)

    @staticmethod
    def add_arguments(parser):
        pass

    def build_inference(self, images, is_training):
        with slim.arg_scope(resnext_utils.resnext_arg_scope(batch_norm_fused=self.args.use_fused_batchnorm)):
            logits, endpoints = resnext_101(images,
                                            self.args.num_classes,
                                            is_training=is_training)

            return logits, endpoints


class SEResnext101(ClassifierNetModel):
    def __init__(self, args, dataset=None):
        super().__init__(args, dataset)

    @staticmethod
    def add_arguments(parser):
        pass

    def build_inference(self, images, is_training):
        with slim.arg_scope(resnext_utils.resnext_arg_scope(batch_norm_fused=self.args.use_fused_batchnorm)):
            logits, endpoints = se_resnext_101(images,
                                               self.args.num_classes,
                                               is_training=is_training)

            return logits, endpoints


class MultiMobileNetV1(ClassifierNetModel):
    def __init__(self, args, dataset=None):
        super().__init__(args, dataset)

    @staticmethod
    def add_arguments(parser):
        parser.add_argument("--depth_multiplier", default=1.0, type=float)
        parser.add_argument("--weight_decay", default=0.00004, type=float)
        parser.add_argument("--batch_norm_decay", default=0.9997, type=float)
        parser.add_argument("--batch_norm_epsilon", default=0.001, type=float)

    def build_inference(self, images, is_training):
        with slim.arg_scope(mobilenet_v1.mobilenet_v1_arg_scope(is_training=is_training,
                                                                use_fused_batchnorm=self.args.use_fused_batchnorm,
                                                                weight_decay=self.args.weight_decay,
                                                                batch_norm_decay=self.args.batch_norm_decay,
                                                                batch_norm_epsilon=self.args.batch_norm_epsilon)):
            kwargs = {"num_classes": self.args.max_num_labels,
                      "num_tasks": self.args.num_tasks,
                      "is_training": is_training,
                      "depth_multiplier": self.args.depth_multiplier,
                      "global_pool": True}

            logits, endpoints = mobilenet_v1.mobilenet_v1_multitask(images, **kwargs)

            return logits, endpoints


class MobileNetV2(ClassifierNetModel):
    """[Justin]
    This custom implementation is different from slim official implementation
    https://github.com/tensorflow/models/tree/master/research/slim/nets/mobilenet.
    The difference is that our's have 1x1 convolution at first bottleneck layer while
    official one does not.
    Since it looks like a bug of official one, I decoupled custom implementation from official one.
    """
    def __init__(self, args, dataset=None):
        super().__init__(args, dataset)

    @staticmethod
    def add_arguments(parser):
        parser.add_argument("--depth_multiplier", default=1.0, type=float)
        parser.add_argument("--weight_decay", default=0.00004, type=float)
        parser.add_argument("--dropout_keep_prob", default=0.5, type=float)
        parser.add_argument("--batch_norm_decay", default=0.9997, type=float)
        parser.add_argument("--batch_norm_epsilon", default=0.001, type=float)
        parser.add_argument("--no-finegrain_classification_mode",
                            dest="finegrain_classification_mode",
                            action="store_false")
        parser.add_argument("--finegrain_classification_mode",
                            dest="finegrain_classification_mode",
                            action="store_true",
                            help="""
                            finegrain_classification_mode: When set to True, the model
                            will keep the last layer large even for small multipliers. Following
                            https://arxiv.org/abs/1801.04381
                            suggests that it improves performance for ImageNet-type of problems.
                            *Note* ignored if final_endpoint makes the builder exit earlier.
                            """)
        parser.set_defaults(finegrain_classification_mode=True)

    def build_inference(self, images, is_training):
        with slim.arg_scope(mobilenet_v2.mobilenet_v2_arg_scope(is_training=is_training,
                                                                weight_decay=self.args.weight_decay,
                                                                dropout_keep_prob=self.args.dropout_keep_prob,
                                                                use_fused_batchnorm=self.args.use_fused_batchnorm,
                                                                batch_norm_decay=self.args.batch_norm_decay,
                                                                batch_norm_epsilon=self.args.batch_norm_epsilon)):
            NET_DEF = mobilenet_v2.get_net_def(depth_multiplier=self.args.depth_multiplier,
                                               finegrain_classification_mode=self.args.finegrain_classification_mode)

            kwargs = {
                "num_classes": self.args.num_classes,
                "net_def": NET_DEF,
            }

            if self.args.task_type == "multi":
                kwargs["prediction_fn"] = tf.sigmoid

            logits, endpoints = mobilenet_v2.mobilenet_v2(images, **kwargs)

            return logits, endpoints


class MultiMobileNetV2(ClassifierNetModel):
    def __init__(self, args, dataset=None):
        super().__init__(args, dataset)

    @staticmethod
    def add_arguments(parser):
        parser.add_argument("--depth_multiplier", default=1.0, type=float)
        parser.add_argument("--weight_decay", default=0.00004, type=float)
        parser.add_argument("--dropout_keep_prob", default=0.5, type=float)
        parser.add_argument("--batch_norm_decay", default=0.9997, type=float)
        parser.add_argument("--batch_norm_epsilon", default=0.001, type=float)
        parser.add_argument("--no-finegrain_classification_mode",
                            dest="finegrain_classification_mode",
                            action="store_false")
        parser.add_argument("--finegrain_classification_mode",
                            dest="finegrain_classification_mode",
                            action="store_true",
                            help="""
                            finegrain_classification_mode: When set to True, the model
                            will keep the last layer large even for small multipliers. Following
                            https://arxiv.org/abs/1801.04381
                            suggests that it improves performance for ImageNet-type of problems.
                            *Note* ignored if final_endpoint makes the builder exit earlier.
                            """)
        parser.set_defaults(finegrain_classification_mode=True)

    def build_inference(self, images, is_training):
        with slim.arg_scope(mobilenet_v2.mobilenet_v2_arg_scope(is_training=is_training,
                                                                weight_decay=self.args.weight_decay,
                                                                dropout_keep_prob=self.args.dropout_keep_prob,
                                                                use_fused_batchnorm=self.args.use_fused_batchnorm,
                                                                batch_norm_decay=self.args.batch_norm_decay,
                                                                batch_norm_epsilon=self.args.batch_norm_epsilon)):
            NET_DEF = mobilenet_v2.get_net_def(depth_multiplier=self.args.depth_multiplier,
                                               finegrain_classification_mode=self.args.finegrain_classification_mode)

            kwargs = {
                "num_classes": self.args.max_num_labels,
                "num_tasks": self.args.num_tasks,
                "net_def": NET_DEF,
            }

            if self.args.task_type == "multi":
                kwargs["prediction_fn"] = tf.sigmoid

            logits, endpoints = mobilenet_v2.mobilenet_v2_multitask(images, **kwargs)

            return logits, endpoints


class SlimMobileNetV2(ClassifierNetModel):
    def __init__(self, args, dataset=None):
        super().__init__(args, dataset)

    def build_inference(self, images, is_training):
        with tf.contrib.slim.arg_scope(mobilenet_v2_slim.training_scope(is_training=is_training)):
            with slim.arg_scope([slim.batch_norm], fused=self.args.use_fused_batchnorm):
                logits, endpoints = mobilenet_v2_slim.mobilenet(
                    images,
                    num_classes=self.args.num_classes,
                    depth_multiplier=self.args.depth_multiplier,
                    finegrain_classification_mode=self.args.finegrain_classification_mode,
                )
                prediction_fn = tf.sigmoid if self.args.task_type == "multi" else slim.softmax
                endpoints["Predictions"] = prediction_fn(logits)
                return logits, endpoints

    @staticmethod
    def add_arguments(parser):
        parser.add_argument("--depth_multiplier", default=1.0, type=float)
        parser.add_argument("--no-finegrain_classification_mode",
                            dest="finegrain_classification_mode",
                            action="store_false")
        parser.add_argument("--finegrain_classification_mode",
                            dest="finegrain_classification_mode",
                            action="store_true",
                            help="""
                            finegrain_classification_mode: When set to True, the model
                            will keep the last layer large even for small multipliers. Following
                            https://arxiv.org/abs/1801.04381
                            suggests that it improves performance for ImageNet-type of problems.
                            *Note* ignored if final_endpoint makes the builder exit earlier.
                            """)
        parser.set_defaults(finegrain_classification_mode=True)
