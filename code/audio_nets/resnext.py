"""Modified ResNeXt model implementation

Residual networks with next dimension (ResNeXt) were originally proposed in:
[1] Saining Xie, Ross Girshick, Piotr Dollar, Zhuowen Tu, Kaiming He
    Aggregated Residual Transformations for Deep Neural Networks. arXiv:1611.05431

The key difference of ResNeXt with ResNet is the concept of cardinality.

Typical use:

   from model.clfnets import resnext

ResNeXt-101 for SafeMatch:

   # inputs has shape [batch, 224, 224, 3]
   with slim.arg_scope(resnext.resnext_arg_scope()):
      net, end_points = resnext.resnext_101(inputs, ${num_class}, is_training=${is_training})
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf

from clfnets import resnext_utils

slim = tf.contrib.slim
resnext_arg_scope = resnext_utils.resnext_arg_scope


@slim.add_arg_scope
def bottleneck(inputs, depth, depth_bottleneck, cardinality, stride, rate=1,
               outputs_collections=None, scope=None):
    """Bottleneck residual unit variant with BN before convolutions.

    This is the full preactivation residual unit variant proposed in [2]. See
    Fig. 1(b) of [2] for its definition. Note that we use here the bottleneck
    variant which has an extra bottleneck layer.

    We adopt implementation (b), early concatenation, since it has less parameters
    and is faster on inference.
    https://hyperconnect.atlassian.net/wiki/spaces/ML/pages/409567298/ResNeXt+101

    When putting together two consecutive ResNeXt blocks that use this unit, one
    should use stride = 2 in the last unit of the first block.

    Args:
      inputs: A tensor of size [batch, height, width, channels].
      depth: The depth of the ResNeXt unit output.
      depth_bottleneck: The depth of the bottleneck layers.
      stride: The ResNeXt unit's stride. Determines the amount of downsampling of
        the units output compared to its input.
      rate: An integer, rate for atrous convolution.
      outputs_collections: Collection to add the ResNeXt unit output.
      scope: Optional variable_scope.

    Returns:
      The ResNeXt unit's output.
    """
    with tf.variable_scope(scope, 'bottleneck', [inputs]) as sc:
        depth_in = slim.utils.last_dimension(inputs.get_shape(), min_rank=4)
        preact = slim.batch_norm(inputs, activation_fn=tf.nn.relu, scope='preact')
        if depth == depth_in:
            shortcut = resnext_utils.subsample(inputs, stride, 'shortcut')
        else:
            shortcut = slim.conv2d(preact, depth, [1, 1], stride=stride,
                                   normalizer_fn=None, activation_fn=None,
                                   scope='shortcut')

        if depth_bottleneck % cardinality != 0:
            raise ValueError('The depth_bottleneck({}) needs to be a multiple of cardinality({}).'.format(
                depth_bottleneck, cardinality))
        depth_cardinality = depth_bottleneck // cardinality

        # implement Figure3(b); early concatenation
        residual_list = []
        for cidx in range(cardinality):
            with tf.variable_scope('card_{}'.format(cidx)):
                residual = slim.conv2d(preact, depth_cardinality, [1, 1], stride=1,
                                       scope='conv1')
                residual = resnext_utils.conv2d_same(residual, depth_cardinality, 3, stride,
                                                     rate=rate, scope='conv2')
                residual_list.append(residual)

        residual = tf.concat(residual_list, axis=3)

        residual = slim.conv2d(residual, depth, [1, 1], stride=1,
                               normalizer_fn=None, activation_fn=None,
                               scope='conv3')

        # # implement Figure3(a); original model
        # residual_list = []
        # for cidx in range(cardinality):
        #     with tf.variable_scope('card_{}'.format(cidx)):
        #         residual = slim.conv2d(preact, depth_cardinality, [1, 1], stride=1,
        #                                scope='conv1')
        #         residual = resnext_utils.conv2d_same(residual, depth_cardinality, 3, stride,
        #                                              rate=rate, scope='conv2')
        #         residual = slim.conv2d(residual, depth, [1, 1], stride=1,
        #                                normalizer_fn=None, activation_fn=None,
        #                                scope='conv3')
        #         residual_list.append(residual)

        # residual = tf.add_n(residual_list)

        output = shortcut + residual

        return slim.utils.collect_named_outputs(outputs_collections,
                                                sc.name,
                                                output)


@slim.add_arg_scope
def se_bottleneck(inputs, depth, depth_bottleneck, cardinality, reduce_ratio,
                  stride, rate=1, outputs_collections=None, scope=None):
    with tf.variable_scope(scope, 'se_bottleneck', [inputs]) as sc:
        depth_in = slim.utils.last_dimension(inputs.get_shape(), min_rank=4)
        preact = slim.batch_norm(inputs, activation_fn=tf.nn.relu, scope='preact')
        if depth == depth_in:
            shortcut = resnext_utils.subsample(inputs, stride, 'shortcut')
        else:
            shortcut = slim.conv2d(preact, depth, [1, 1], stride=stride,
                                   normalizer_fn=None, activation_fn=None,
                                   scope='shortcut')

        if depth_bottleneck % cardinality != 0:
            raise ValueError('The depth_bottleneck({}) needs to be a multiple of cardinality({}).'.format(
                depth_bottleneck, cardinality))
        depth_cardinality = depth_bottleneck // cardinality

        # resnext block
        residual_list = []
        for cidx in range(cardinality):
            with tf.variable_scope('card_{}'.format(cidx)):
                residual = slim.conv2d(preact, depth_cardinality, [1, 1], stride=1,
                                       scope='conv1')
                residual = resnext_utils.conv2d_same(residual, depth_cardinality, 3, stride,
                                                     rate=rate, scope='conv2')
                residual_list.append(residual)

        residual = tf.concat(residual_list, axis=3)

        residual = slim.conv2d(residual, depth, [1, 1], stride=1,
                               normalizer_fn=None, activation_fn=None,
                               scope='conv3')

        # SE block
        if depth % reduce_ratio != 0:
            raise ValueError('The depth({}) needs to be a multiple of reduce_ratio({}).'.format(
                depth, reduce_ratio))
        depth_embedding = depth // reduce_ratio
        # Follow the naming convention of the paper.
        with tf.variable_scope('se_block'):
            # squeeze
            z = tf.reduce_mean(residual, [1, 2], name='squeeze', keepdims=True)
            # excitation
            z = slim.fully_connected(z, depth_embedding,
                                     activation_fn=tf.nn.relu,
                                     normalizer_fn=None,
                                     weights_initializer=slim.variance_scaling_initializer(),
                                     biases_initializer=None,
                                     scope='fc_relu')
            scale = slim.fully_connected(z, depth,
                                         activation_fn=tf.nn.sigmoid,
                                         normalizer_fn=None,
                                         weights_initializer=slim.variance_scaling_initializer(),
                                         biases_initializer=None,
                                         scope='fc_sigmoid')
            # scaling
            residual = tf.multiply(residual, scale, name='scaled')

        output = shortcut + residual

        return slim.utils.collect_named_outputs(outputs_collections,
                                                sc.name,
                                                output)


def resnext(inputs,
            blocks,
            num_classes=None,
            is_training=True,
            global_pool=True,
            output_stride=None,
            include_root_block=True,
            spatial_squeeze=True,
            reuse=None,
            scope=None):
    """Generator for ResNeXt models.

    Training for image classification on Imagenet is usually done with [224, 224]
    inputs, resulting in [7, 7] feature maps at the output of the last ResNeXt
    block for the ResNeXts defined in [1] that have nominal stride equal to 32.
    However, for dense prediction tasks we advise that one uses inputs with
    spatial dimensions that are multiples of 32 plus 1, e.g., [321, 321]. In
    this case the feature maps at the ResNeXt output will have spatial shape
    [(height - 1) / output_stride + 1, (width - 1) / output_stride + 1]
    and corners exactly aligned with the input image corners, which greatly
    facilitates alignment of the features to the image. Using as input [225, 225]
    images results in [8, 8] feature maps at the output of the last ResNeXt block.

    For dense prediction tasks, the ResNeXt needs to run in fully-convolutional
    (FCN) mode and global_pool needs to be set to False. The ResNeXts in [1, 2] all
    have nominal stride equal to 32 and a good choice in FCN mode is to use
    output_stride=16 in order to increase the density of the computed features at
    small computational and memory overhead, cf. http://arxiv.org/abs/1606.00915.

    Args:
      inputs: A tensor of size [batch, height_in, width_in, channels].
      blocks: A list of length equal to the number of ResNeXt blocks. Each element
        is a resnext_utils.Block object describing the units in the block.
      num_classes: Number of predicted classes for classification tasks.
        If 0 or None, we return the features before the logit layer.
      is_training: whether batch_norm layers are in training mode.
      global_pool: If True, we perform global average pooling before computing the
        logits. Set to True for image classification, False for dense prediction.
      output_stride: If None, then the output will be computed at the nominal
        network stride. If output_stride is not None, it specifies the requested
        ratio of input to output spatial resolution.
      include_root_block: If True, include the initial convolution followed by
        max-pooling, if False excludes it. If excluded, `inputs` should be the
        results of an activation-less convolution.
      spatial_squeeze: if True, logits is of shape [B, C], if false logits is
          of shape [B, 1, 1, C], where B is batch_size and C is number of classes.
          To use this parameter, the input images must be smaller than 300x300
          pixels, in which case the output logit layer does not contain spatial
          information and can be removed.
      reuse: whether or not the network and its variables should be reused. To be
        able to reuse 'scope' must be given.
      scope: Optional variable_scope.


    Returns:
      net: A rank-4 tensor of size [batch, height_out, width_out, channels_out].
        If global_pool is False, then height_out and width_out are reduced by a
        factor of output_stride compared to the respective height_in and width_in,
        else both height_out and width_out equal one. If num_classes is 0 or None,
        then net is the output of the last ResNeXt block, potentially after global
        average pooling. If num_classes is a non-zero integer, net contains the
        pre-softmax activations.
      end_points: A dictionary from components of the network to the corresponding
        activation.

    Raises:
      ValueError: If the target output_stride is not valid.
    """
    with tf.variable_scope(scope, 'resnext', [inputs], reuse=reuse) as sc:
        end_points_collection = sc.original_name_scope + '_end_points'
        with slim.arg_scope([slim.conv2d, bottleneck,
                             resnext_utils.stack_blocks_dense],
                            outputs_collections=end_points_collection):
            with slim.arg_scope([slim.batch_norm], is_training=is_training):
                net = inputs
                if include_root_block:
                    if output_stride is not None:
                        if output_stride % 4 != 0:
                            raise ValueError('The output_stride needs to be a multiple of 4.')
                        output_stride /= 4
                    # We do not include batch normalization or activation functions in
                    # conv1 because the first ResNeXt unit will perform these. Cf.
                    # Appendix of [2].
                    with slim.arg_scope([slim.conv2d],
                                        activation_fn=None, normalizer_fn=None):
                        # [FOR AUDIO CLASSIFICATION] Modify from 64 to 32
                        # to match activation map size (833x32x32=852992) ~sim (112x112x64=802816)
                        net = resnext_utils.conv2d_same(net, 32, 7, stride=2, scope='conv1')
                    net = slim.max_pool2d(net, [3, 3], stride=2, scope='pool1')
                net = resnext_utils.stack_blocks_dense(net, blocks, output_stride)
                # This is needed because the pre-activation variant does not have batch
                # normalization or activation functions in the residual unit output. See
                # Appendix of [2].
                net = slim.batch_norm(net, activation_fn=tf.nn.relu, scope='postnorm')
                # Convert end_points_collection into a dictionary of end_points.
                end_points = slim.utils.convert_collection_to_dict(
                    end_points_collection)

                if global_pool:
                    # Global average pooling.
                    net = tf.reduce_mean(net, [1, 2], name='pool5', keep_dims=True)
                    end_points['global_pool'] = net
                if num_classes:
                    net = slim.conv2d(net, num_classes, [1, 1], activation_fn=None,
                                      normalizer_fn=None, scope='logits')
                    end_points[sc.name + '/logits'] = net
                    if spatial_squeeze:
                        net = tf.squeeze(net, [1, 2], name='SpatialSqueeze')
                        end_points[sc.name + '/spatial_squeeze'] = net
                    end_points['predictions'] = slim.softmax(net, scope='predictions')
                return net, end_points


def resnext_block(scope, base_depth, cardinality, num_units, stride):
    """Helper function for creating a resnext bottleneck block.

    Args:
      scope: The scope of the block.
      base_depth: The depth of the bottleneck layer for each unit.
      cardinality: The cardinality of the block.
      num_units: The number of units in the block.
      stride: The stride of the block, implemented as a stride in the last unit.
        All other units have stride=1.

    Returns:
      A resnext bottleneck block.
    """
    # [Fix by @justin] Depth of ResNeXt output is 2 while ResNet is 4.
    return resnext_utils.Block(scope, bottleneck, [{
        'cardinality': cardinality,
        'depth': base_depth * 2,
        'depth_bottleneck': base_depth,
        'stride': 1
    }] * (num_units - 1) + [{
        'cardinality': cardinality,
        'depth': base_depth * 2,
        'depth_bottleneck': base_depth,
        'stride': stride
    }])


def resnext_50(inputs,
               num_classes=None,
               is_training=True,
               global_pool=True,
               output_stride=None,
               spatial_squeeze=True,
               reuse=None,
               scope='resnext_50'):
    """ResNeXt-50."""
    blocks = [
        # [FOR AUDIO CLASSIFICATION] Divide by 2
        resnext_block('block1', base_depth=128//2, cardinality=32, num_units=3, stride=2),
        resnext_block('block2', base_depth=256//2, cardinality=32, num_units=4, stride=2),
        resnext_block('block3', base_depth=512//2, cardinality=32, num_units=6, stride=2),
        resnext_block('block4', base_depth=1024//2, cardinality=32, num_units=3, stride=1),
    ]
    return resnext(inputs, blocks, num_classes, is_training=is_training,
                   global_pool=global_pool, output_stride=output_stride,
                   include_root_block=True, spatial_squeeze=spatial_squeeze,
                   reuse=reuse, scope=scope)


def resnext_101(inputs,
                num_classes=None,
                is_training=True,
                global_pool=True,
                output_stride=None,
                spatial_squeeze=True,
                reuse=None,
                scope='resnext_101'):
    """ResNeXt-101."""
    blocks = [
        resnext_block('block1', base_depth=128, cardinality=32, num_units=3, stride=2),
        resnext_block('block2', base_depth=256, cardinality=32, num_units=4, stride=2),
        resnext_block('block3', base_depth=512, cardinality=32, num_units=23, stride=2),
        resnext_block('block4', base_depth=1024, cardinality=32, num_units=3, stride=1),
    ]
    return resnext(inputs, blocks, num_classes, is_training=is_training,
                   global_pool=global_pool, output_stride=output_stride,
                   include_root_block=True, spatial_squeeze=spatial_squeeze,
                   reuse=reuse, scope=scope)


def resnext_152(inputs,
                num_classes=None,
                is_training=True,
                global_pool=True,
                output_stride=None,
                spatial_squeeze=True,
                reuse=None,
                scope='resnext_152'):
    """ResNeXt-152."""
    blocks = [
        resnext_block('block1', base_depth=128, cardinality=32, num_units=3, stride=2),
        resnext_block('block2', base_depth=256, cardinality=32, num_units=8, stride=2),
        resnext_block('block3', base_depth=512, cardinality=32, num_units=36, stride=2),
        resnext_block('block4', base_depth=1024, cardinality=32, num_units=3, stride=1),
    ]
    return resnext(inputs, blocks, num_classes, is_training=is_training,
                   global_pool=global_pool, output_stride=output_stride,
                   include_root_block=True, spatial_squeeze=spatial_squeeze,
                   reuse=reuse, scope=scope)


def resnext_200(inputs,
                num_classes=None,
                is_training=True,
                global_pool=True,
                output_stride=None,
                spatial_squeeze=True,
                reuse=None,
                scope='resnext_200'):
    """ResNeXt-200."""
    blocks = [
        resnext_block('block1', base_depth=128, cardinality=32, num_units=3, stride=2),
        resnext_block('block2', base_depth=256, cardinality=32, num_units=24, stride=2),
        resnext_block('block3', base_depth=512, cardinality=32, num_units=36, stride=2),
        resnext_block('block4', base_depth=1024, cardinality=32, num_units=3, stride=1),
    ]
    return resnext(inputs, blocks, num_classes, is_training=is_training,
                   global_pool=global_pool, output_stride=output_stride,
                   include_root_block=True, spatial_squeeze=spatial_squeeze,
                   reuse=reuse, scope=scope)


def se_resnext(inputs,
               blocks,
               num_classes=None,
               is_training=True,
               global_pool=True,
               output_stride=None,
               include_root_block=True,
               spatial_squeeze=True,
               reuse=None,
               scope=None):
    with tf.variable_scope(scope, 'se_resnext', [inputs], reuse=reuse) as sc:
        end_points_collection = sc.original_name_scope + '_end_points'
        with slim.arg_scope([slim.conv2d, se_bottleneck,
                             resnext_utils.stack_blocks_dense],
                            outputs_collections=end_points_collection):
            with slim.arg_scope([slim.batch_norm], is_training=is_training):
                net = inputs
                if include_root_block:
                    if output_stride is not None:
                        if output_stride % 4 != 0:
                            raise ValueError('The output_stride needs to be a multiple of 4.')
                        output_stride /= 4
                    # We do not include batch normalization or activation functions in
                    # conv1 because the first ResNeXt unit will perform these. Cf.
                    # Appendix of [2].
                    with slim.arg_scope([slim.conv2d],
                                        activation_fn=None, normalizer_fn=None):
                        net = resnext_utils.conv2d_same(net, 64, 7, stride=2, scope='conv1')
                    net = slim.max_pool2d(net, [3, 3], stride=2, scope='pool1')
                net = resnext_utils.stack_blocks_dense(net, blocks, output_stride)
                # This is needed because the pre-activation variant does not have batch
                # normalization or activation functions in the residual unit output. See
                # Appendix of [2].
                net = slim.batch_norm(net, activation_fn=tf.nn.relu, scope='postnorm')
                # Convert end_points_collection into a dictionary of end_points.
                end_points = slim.utils.convert_collection_to_dict(
                    end_points_collection)

                if global_pool:
                    # Global average pooling.
                    net = tf.reduce_mean(net, [1, 2], name='pool5', keep_dims=True)
                    end_points['global_pool'] = net
                if num_classes:
                    net = slim.conv2d(net, num_classes, [1, 1], activation_fn=None,
                                      normalizer_fn=None, scope='logits')
                    end_points[sc.name + '/logits'] = net
                    if spatial_squeeze:
                        net = tf.squeeze(net, [1, 2], name='SpatialSqueeze')
                        end_points[sc.name + '/spatial_squeeze'] = net
                    end_points['predictions'] = slim.softmax(net, scope='predictions')
                return net, end_points


def se_resnext_block(scope, base_depth, cardinality, reduce_ratio, num_units, stride):
    return resnext_utils.Block(scope, se_bottleneck, [{
        'cardinality': cardinality,
        'reduce_ratio': reduce_ratio,
        'depth': base_depth * 2,
        'depth_bottleneck': base_depth,
        'stride': 1
    }] * (num_units - 1) + [{
        'cardinality': cardinality,
        'reduce_ratio': reduce_ratio,
        'depth': base_depth * 2,
        'depth_bottleneck': base_depth,
        'stride': stride
    }])


def se_resnext_101(inputs,
                   num_classes=None,
                   is_training=True,
                   global_pool=True,
                   output_stride=None,
                   spatial_squeeze=True,
                   reuse=None,
                   scope='se_resnext_101'):
    """SE-ResNeXt-101."""
    blocks = [
        se_resnext_block('block1', base_depth=128, cardinality=32, reduce_ratio=16, num_units=3, stride=2),
        se_resnext_block('block2', base_depth=256, cardinality=32, reduce_ratio=16, num_units=4, stride=2),
        se_resnext_block('block3', base_depth=512, cardinality=32, reduce_ratio=16, num_units=23, stride=2),
        se_resnext_block('block4', base_depth=1024, cardinality=32, reduce_ratio=16, num_units=3, stride=1),
    ]
    return se_resnext(inputs, blocks, num_classes, is_training=is_training,
                      global_pool=global_pool, output_stride=output_stride,
                      include_root_block=True, spatial_squeeze=spatial_squeeze,
                      reuse=reuse, scope=scope)
