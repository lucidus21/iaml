from abc import ABC
from abc import abstractmethod
from typing import Callable
from typing import Tuple
from common.utils import get_logger

import tensorflow as tf
import tensorflow.contrib.slim as slim

from factory.classification_converter import SoftmaxConverter
from factory.classification_converter import SigmoidConverter
from factory.classification_converter import ArgmaxConverter


_available_output_type = [
    "softmax",   # => probability vectors which column-wise sum 1
    "labels",  # => argmax(probability vectors)
    "sigmoid",
]


class Task(ABC):
    def __init__(
        self,
        args,
        name: str,
        converter,
        output_type: str,
        dataset,
    ) -> None:
        self.args = args
        self.converter = converter
        self.output_type = output_type
        assert self.output_type in _available_output_type, f"{self.output_type} does not exists"
        self.dataset = dataset

        self.log = get_logger(name)

    @property
    def output_type(self):
        try:
            return self._output_type
        except AttributeError:
            self._output_type = None

    @output_type.setter
    def output_type(self, output_type):
        self._output_type = output_type

    def convert(self, *args, **kwargs):
        return self.converter.convert(*args, **kwargs)

    def build_loss(
        self,
        logits: tf.Tensor,
        scores: tf.Tensor,
        labels: tf.Tensor,
    ) -> Tuple:
        """Every task defines its own loss.

        `build_loss` is called in `build` of `ClassificationNet`.
        """
        raise NotImplementedError(f"build_loss() not defined in {self.__class__.__name__}")

    def build_output(
        self,
        build_output_fn: Callable,
        raw_inputs,
        is_training,
        output_type_name: str
    ) -> Tuple:
        """
        """
        inputs, logits, outputs, endpoints = build_output_fn(
            raw_inputs,
            is_training,
            self.output_type,
        )
        return inputs, logits, outputs, endpoints


class SingleTask(Task):
    def __init__(
        self,
        args,
        dataset,
        name: str="SingleTask",
    ):
        converter = SoftmaxConverter
        output_type = "softmax"
        super().__init__(args, name, converter, output_type, dataset)

    def build_loss(
        self,
        logits: tf.Tensor,
        scores: tf.Tensor,
        labels: tf.Tensor,
    ):
        endpoints_loss = {}
        tf.losses.softmax_cross_entropy(
            logits=logits,
            onehot_labels=labels,
            label_smoothing=0,
            weights=1.0,
        )

        # Get total_loss by Tensorflow collection
        total_loss = tf.losses.get_total_loss(add_regularization_losses=True)
        model_loss = tf.losses.get_total_loss(add_regularization_losses=False)
        return total_loss, model_loss, endpoints_loss


class SingleBCTask(Task):
    def __init__(
        self,
        args,
        dataset,
        name: str="SingleBCTask",
    ):
        converter = SoftmaxConverter
        output_type = "softmax"
        super().__init__(args, name, converter, output_type, dataset)

    def build_loss(
        self,
        logits: tf.Tensor,
        scores: tf.Tensor,  # FIXME only BC requires scores, try to remove
        labels: tf.Tensor,
    ):
        endpoints_loss = {}
        model_loss = SingleBCTask.KL_divergence(
            scores,
            labels,
        )

        total_loss = model_loss
        return total_loss, model_loss, endpoints_loss

    @staticmethod
    def KL_divergence(
        P: tf.Tensor,
        Q: tf.Tensor,
        eps: float=1e-7,
    ) -> tf.Tensor:
        """Compute Kullback–Leibler divergence between P and Q.
        https://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence#Definition

        https://www.tensorflow.org/api_docs/python/tf/distributions/kl_divergence is not defined
        for Tensors.

        Args:
            P: nxm Tensor distribution.
            Q: nxm Tensor distribution.

        Returns:
            kld: Tensor of shape [] holding Kullback–Leibler divergence.
        """
        def log2(x):
            """Tensorflow does not support log with base 2.
            https://github.com/tensorflow/tensorflow/issues/1666
            """
            numerator = tf.log(x)
            denominator = tf.log(tf.constant(2, dtype=numerator.dtype))
            return numerator / denominator

        P = tf.clip_by_value(P, eps, tf.reduce_max(P))
        Q = tf.clip_by_value(Q, eps, tf.reduce_max(Q))

        kld = tf.reduce_mean(
            tf.reduce_sum(
                P * log2(P / Q),
                axis=1,
            ))
        return kld


class MultiLabelTask(Task):
    def __init__(
        self,
        args,
        dataset,
        name: str="MultiLabelTask",
    ):
        converter = SigmoidConverter
        output_type = "sigmoid"
        super().__init__(args, name, converter, output_type, dataset)

    def build_loss(
        self,
        logits: tf.Tensor,
        scores: tf.Tensor,
        labels: tf.Tensor,
    ):
        endpoints_loss = {}
        sigmoid_loss = tf.nn.sigmoid_cross_entropy_with_logits(
            labels=labels,
            logits=logits,
        )
        model_loss = tf.reduce_mean(tf.reduce_sum(sigmoid_loss, axis=1), axis=0)
        tf.add_to_collection(tf.GraphKeys.LOSSES, model_loss)
        endpoints_loss["regularization_loss"] = tf.losses.get_regularization_loss()
        total_loss = tf.losses.get_total_loss(add_regularization_losses=True)
        return total_loss, model_loss, endpoints_loss


class MultiTaskExclusive(Task):
    def __init__(
        self,
        args,
        dataset,
        name: str="MultiTaskExclusive",
    ):
        converter = SoftmaxConverter
        output_type = "softmax"
        super().__init__(args, name, converter, output_type, dataset)

    def build_loss(
        self,
        logits,
        scores,
        labels,
    ):
        endpoints_loss = {}
        task_logits = tf.split(logits, self.dataset.num_tasks, axis=1, name="split_logits")
        task_labels = tf.split(labels, self.dataset.num_tasks, axis=1, name="split_labels")

        self.log.info(f"Len of task_logits: {len(task_logits)}")
        self.log.info(f"Len of task_labels: {len(task_labels)}")

        losses = []

        assert len(self.dataset.task_names) == len(task_logits) == len(task_labels), "Different number of task!"
        for t_name, t_logits, t_labels in zip(self.dataset.task_names, task_logits, task_labels):
            with tf.variable_scope(t_name):

                loss = tf.nn.softmax_cross_entropy_with_logits_v2(
                    logits=tf.squeeze(t_logits, axis=1),
                    labels=tf.stop_gradient(tf.squeeze(t_labels, axis=1)),
                )

                losses.append(loss)
                endpoints_loss[t_name] = tf.reduce_mean(loss)

        softmax_loss_all_task = tf.stack(losses, axis=-1)  # (N, t)

        # task_weights specifies which task labels are available for each row(data).
        # For multitask_exclusive setup, only a single task is given.
        # Therefore, if we apply reduce_sum on onehot_labels,
        # only given task is encded as 1 at task_weights.
        # By multiplying task_weights to softmax_loss_all_task, we can prevent backprogation
        # to be applied for not given task.
        task_weights = tf.reduce_sum(labels, axis=-1)

        softmax_loss = tf.reduce_sum(tf.multiply(softmax_loss_all_task, task_weights), axis=-1)

        model_loss = tf.reduce_mean(softmax_loss, axis=0)

        reg_loss = tf.add_n(tf.losses.get_regularization_losses())

        total_loss = model_loss + reg_loss

        return total_loss, model_loss, endpoints_loss
