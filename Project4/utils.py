import subprocess
import pickle
import shutil
import time
import os
from typing import Dict
from itertools import groupby
from functools import reduce
from pathlib import Path
from operator import mul

from tensorflow.contrib.training import checkpoints_iterator
import tensorflow as tf
import pandas as pd
import numpy as np
import deprecation
from termcolor import colored


def show_models():
    trainable_variables = set(tf.contrib.framework.get_variables(collection=tf.GraphKeys.TRAINABLE_VARIABLES))
    all_variables = tf.contrib.framework.get_variables()
    trainable_vars = tf.trainable_variables()
    total_params = 0
    print(colored(f">> Start of showing all variables", "cyan", attrs=["bold"]))
    for v in all_variables:
        is_trainable = v in trainable_variables
        count_params = reduce(mul, v.get_shape().as_list(), 1)
        total_params += count_params
        color = "cyan" if is_trainable else "green"
        print(colored((
            f">>    {v.name} {v.dtype} : {v.get_shape().as_list()}, {count_params} ... {total_params} "
            f"(is_trainable: {is_trainable})"
        ), color))
    print(colored(
        f">> End of showing all variables // Number of variables: {len(all_variables)}, "
        f"Number of trainable variables : {len(trainable_vars)}, "
        f"Total prod + sum of shape: {total_params}",
        "cyan", attrs=["bold"]))
    return total_params


def ckpt_iterator(checkpoint_dir, min_interval_secs=0, timeout=None, timeout_fn=None, logger=None):
    for ckpt_path in checkpoints_iterator(checkpoint_dir, min_interval_secs, timeout, timeout_fn):
        yield ckpt_path
