import argparse
import atexit
from typing import List

import tensorflow as tf

import const
import factory.audio_nets as audio_nets
import common.utils as utils
from datasets.data_wrapper_base import DataWrapperBase
from datasets.audio_data_wrapper import AudioDataWrapper
from datasets.audio_data_wrapper import SingleLabelAudioDataWrapper
from datasets.audio_data_wrapper import MultiLabelAudioDataWrapper
from helper.base import Base
from helper.base import AudioBase
from helper.trainer import TrainerBase
from helper.trainer import SingleLabelAudioTrainer
from helper.trainer import MultiLabelAudioTrainer
from factory.base import CNNModel
from metrics.base import MetricManagerBase


def train(args, trainer_cls=None):
    is_training = True
    dataset_name = args.dataset_split_name[0]
    session = tf.Session(config=const.TF_SESSION_CONFIG)

    if args.task_type == "audio_single":
        dataset = SingleLabelAudioDataWrapper(
            args,
            session,
            dataset_name,
            is_training,
        )
    elif args.task_type == "audio_multilabel":
        dataset = MultiLabelAudioDataWrapper(
            args,
            session,
            dataset_name,
            is_training,
            args.num_classes,
        )
    else:
        print("Need to choose task_type!")
        return
    wavs, spectrograms, fingerprints, labels, genres = dataset.get_input_and_output_op()

    model = eval(f"audio_nets.{args.model}")(args, dataset)
    model.build(
        wavs=wavs,
        spectrograms=spectrograms,
        fingerprints=fingerprints,
        labels=labels,
        is_training=is_training,
        genres=genres,
    )

    if trainer_cls is not None:
        trainer = trainer_cls(
            model,
            session,
            args,
            dataset,
            dataset_name,
        )
    elif args.task_type == "audio_single":
        trainer = SingleLabelAudioTrainer(
            model,
            session,
            args,
            dataset,
            dataset_name,
        )
    elif args.task_type == "audio_multilabel":
        trainer = MultiLabelAudioTrainer(
            model,
            session,
            args,
            dataset,
            dataset_name,
        )

    trainer.train()


def parse_arguments(arguments: List[str]=None):
    parser = argparse.ArgumentParser(description=__doc__)
    subparsers = parser.add_subparsers(title="Model", description="")

    # -- * -- Add common classification network's arguments -- * --
    CNNModel.add_arguments(parser, default_type="classification")
    audio_nets.AudioNetModel.add_arguments(parser)

    # -- * -- Add each classification model's arguments -- * --
    for class_name in audio_nets._available_nets:
        subparser = subparsers.add_parser(class_name)
        subparser.add_argument("--model", default=class_name, type=str, help="DO NOT FIX ME")
        add_audio_net_arguments = eval(f"audio_nets.{class_name}.add_arguments")
        add_audio_net_arguments(subparser)

    # -- * -- Add trainer's arguments -- * --
    DataWrapperBase.add_arguments(parser)
    AudioDataWrapper.add_arguments(parser)
    Base.add_arguments(parser)
    TrainerBase.add_arguments(parser)
    SingleLabelAudioTrainer.add_arguments(parser)
    MetricManagerBase.add_arguments(parser)

    # -- Parse arguments
    args = parser.parse_args(arguments)
    return args


if __name__ == "__main__":
    # -- Parse arguments & save
    args = parse_arguments()
    log = utils.get_logger("Trainer")

    utils.update_train_dir(args)
    utils.dump_configuration(args.train_dir, args)

    if args.testmode:
        atexit.register(utils.exit_handler, args.train_dir)

    if args.step1_mode:
        utils.setup_step1_mode(args)

    log.info(args)
    train(args)
