import csv
import sys
from pathlib import Path
from types import SimpleNamespace
from abc import abstractmethod

import numpy as np
import tensorflow as tf
from scipy.misc import imsave
from tqdm import tqdm

import const
import common.tf_utils as tf_utils
import metrics.manager as metric_manager
from common.model_loader import Ckpt
from common.model_loader import Pb
from common.utils import format_text
from common.utils import get_logger
from helper.base import Base
from helper.base import AudioBase
from metrics.summaries import BaseSummaries
from metrics.summaries import Summaries


class Evaluator(object):
    _available_inference_output = None

    def __init__(self, name, model, session, args, dataset, dataset_name):
        self.log = get_logger(name)

        self.model = model
        self.session = session
        self.args = args
        self.dataset = dataset
        self.dataset_name = dataset_name

        if Path(self.args.checkpoint_path).is_dir():
            latest_checkpoint = tf.train.latest_checkpoint(self.args.checkpoint_path)
            if latest_checkpoint is not None:
                self.args.checkpoint_path = latest_checkpoint
            self.log.info(f"Get latest checkpoint and update to it: {self.args.checkpoint_path}")

        self.watch_path = self._build_watch_path()

        self.variables_to_restore = Base.get_variables_to_restore(args=self.args, log=self.log, debug=False)
        self.session.run(tf.global_variables_initializer())
        self.session.run(tf.local_variables_initializer())
        self.ckpt_loader = Ckpt(
            session=session,
            variables_to_restore=self.variables_to_restore,
            include_scopes=args.checkpoint_include_scopes,
            exclude_scopes=args.checkpoint_exclude_scopes,
            ignore_missing_vars=args.ignore_missing_vars,
        )

    @abstractmethod
    def setup_metric_manager(self):
        raise NotImplementedError

    @abstractmethod
    def setup_metric_ops(self):
        raise NotImplementedError

    @abstractmethod
    def build_non_tensor_data_from_eval_dict(self, eval_dict, **kwargs):
        raise NotImplementedError

    @abstractmethod
    def setup_dataset_iterator(self):
        raise NotImplementedError

    @abstractmethod
    def save_inference_result(self, eval_dict, checkpoint_path):
        raise NotImplementedError

    def _build_watch_path(self):
        if Path(self.args.checkpoint_path).is_dir():
            return Path(self.args.checkpoint_path)
        else:
            return Path(self.args.checkpoint_path).parent

    def build_evaluation_step(self, checkpoint_path):
        if "-" in checkpoint_path and checkpoint_path.split("-")[-1].isdigit():
            return int(checkpoint_path.split("-")[-1])
        else:
            return 0

    def build_checkpoint_paths(self, checkpoint_path):
        checkpoint_glob = Path(checkpoint_path + "*")
        checkpoint_path = Path(checkpoint_path)

        return checkpoint_glob, checkpoint_path

    def build_miscellaneous_path(self, name):
        target_dir = self.watch_path / "miscellaneous" / self.dataset_name / name

        if not target_dir.exists():
            target_dir.mkdir(parents=True)

        return target_dir

    def build_inference_path(self, checkpoint_path):
        if not isinstance(checkpoint_path, Path):
            checkpoint_path = Path(checkpoint_path)
        if checkpoint_path.is_dir():
            root_dir = checkpoint_path
        else:
            root_dir = checkpoint_path.parent

        output_parent_dir = root_dir / "inference" / checkpoint_path.name
        if self.args.inference_output_dirname is not None:
            output_dir = output_parent_dir / self.args.inference_output_dirname
        else:
            output_dir = output_parent_dir / self.args.inference_output

        return output_dir

    def setup_best_keeper(self):
        metric_with_modes = self.metric_manager.get_best_keep_metric_with_modes()
        self.log.debug(metric_with_modes)
        self.best_keeper = tf_utils.BestKeeper(metric_with_modes,
                                               self.dataset_name,
                                               self.watch_path,
                                               self.log)

    def log_miscellaneous_class_metrics(self, predictions, labels, step):
        if self.use_class_metrics:
            self.log.info("Start writing probcut & precisioncut metrics")

            probcut_save_fullpath = (
                self.build_miscellaneous_path("probcut_metrics") / f"prob_cut-{step}.txt"
            )
            precision_cut_save_fullpath = (
                self.build_miscellaneous_path("precision_cut_metrics") / f"precision_cut-{step}.txt"
            )

            self.metrics.save_probcut_precision_cut_metrics(predictions, labels,
                                                            probcut_save_fullpath,
                                                            self.args.probcut_threshold_list,
                                                            precision_cut_save_fullpath,
                                                            self.args.precision_threshold_list,
                                                            )

    def inference(self, checkpoint_path):
        assert not self.args.shuffle, "Current implementation of `inference` requires non-shuffled dataset"
        _data_num = self.dataset.num_samples
        if _data_num % self.args.batch_size != 0:
            with format_text("red", attrs=["bold"]) as fmt:
                self.log.warning(fmt(f"Among {_data_num} data, last {_data_num%self.dataset.batch_size} items will not"
                                     f" be processed during inferential procedure."))

        if self.args.inference_output not in self._available_inference_output:
            raise ValueError(f"Inappropriate inference_output type for "
                             f"{self.__class__.__name__}: {self.args.inference_output}.\n"
                             f"Available outputs are {self._available_inference_output}")

        self.log.info("Inference started")
        self.setup_dataset_iterator()
        self.ckpt_loader.load(checkpoint_path)

        step = self.build_evaluation_step(checkpoint_path)
        checkpoint_glob, checkpoint_path = self.build_checkpoint_paths(checkpoint_path)
        self.session.run(tf.local_variables_initializer())

        eval_dict = self.run_inference(step, is_training=False, do_eval=False)

        self.save_inference_result(eval_dict, checkpoint_path)

    def evaluate_once(self, checkpoint_path):
        self.log.info("Evaluation started")
        self.setup_dataset_iterator()
        self.ckpt_loader.load(checkpoint_path)

        step = self.build_evaluation_step(checkpoint_path)
        checkpoint_glob, checkpoint_path = self.build_checkpoint_paths(checkpoint_path)
        self.session.run(tf.local_variables_initializer())

        eval_metric_dict = self.run_evaluation(step, is_training=False)
        best_keep_metric_dict = self.metric_manager.filter_best_keep_metric(eval_metric_dict)
        is_keep, metrics_keep = self.best_keeper.monitor(self.dataset_name, best_keep_metric_dict)

        # if is_keep and self.args.save_best_keeper:
        if self.args.save_best_keeper:  # FIXME
            meta_info = {
                "step": step,
                "model_size": self.model.total_params,
            }
            self.best_keeper.remove_old_best(self.dataset_name, metrics_keep)
            self.best_keeper.save_best(self.dataset_name, metrics_keep, checkpoint_glob)
            self.best_keeper.remove_temp_dir()
            self.best_keeper.save_scores(self.dataset_name, metrics_keep, best_keep_metric_dict, meta_info)

        self.metric_manager.write_evaluation_summaries(step=step,
                                                       collection_keys=[BaseSummaries.KEY_TYPES.DEFAULT])
        self.metric_manager.log_metrics(step=step)

        self.log.info("Evaluation finished")

        if step >= self.args.max_step_from_restore:
            self.log.info("Evaluation stopped")
            sys.exit()

        return step

    def evaluate_once_pb(self, pb_path, step):
        # FIXME: not worked yet
        _, image_op, label_op = self.get_data_from_graph()

        graph = Pb.load(pb_path)

        any_label_list = []
        prediction_list = []

        # self.session holds dataset which means we should use self.session to get data,
        # while new session will hold pb graph.
        with tf.Session(config=const.TF_SESSION_CONFIG, graph=graph) as session:
            output_tensor = tf.get_default_graph().get_tensor_by_name(self.args.output_name+":0")
            session.run(tf.local_variables_initializer())

            # Make prediction for each image in validation dataset
            while True:
                try:
                    image_batch, label_batch = self.session.run([image_op, label_op])
                    for idx in range(image_batch.shape[0]):
                        image = image_batch[np.newaxis, idx, :, :, :]
                        feed_dict = {self.args.input_name+":0": image}
                        prediction = session.run(output_tensor, feed_dict=feed_dict)
                        prediction_list.append(prediction)

                    any_label_list.append(label_batch)
                except tf.errors.OutOfRangeError:
                    break

        all_predictions = np.vstack(prediction_list)
        all_any_labels = np.vstack(any_label_list)

        return self.metrics.evaluate_pb(all_predictions, all_any_labels)

    def get_data_from_graph(self):
        """ When we evaluate model saved in protobuf format, we need to use graph that was created
        before training started. Data are loaded through this original graph, but evaluated on
        graph created from loaded protobuf model.
        """
        self.dataset.setup_dataset(
            (self.dataset.filenames_placeholder, self.dataset.labels_placeholder),
            batch_size=1,
        )
        self.setup_dataset_iterator()
        return self.dataset.get_input_and_output_op()

    def build_train_directory(self):
        if Path(self.args.checkpoint_path).is_dir():
            return str(self.args.checkpoint_path)
        else:
            return str(Path(self.args.checkpoint_path).parent)

    def build_probcut_precision_cut_args(self, step):
        probcut_save_fullpath = self.build_miscellaneous_path("probcut_metrics") / f"prob_cut-{step}.txt"
        precision_cut_save_fullpath = \
            self.build_miscellaneous_path("precision_cut_metrics") / f"precision_cut-{step}.txt"

        probcut_precision_cut_args = SimpleNamespace(
            probcut_threshold_list=self.args.probcut_threshold_list,
            precision_threshold_list=self.args.precision_threshold_list,
            probcut_save_fullpath=probcut_save_fullpath,
            precision_cut_save_fullpath=precision_cut_save_fullpath,
        )

        return probcut_precision_cut_args

    @staticmethod
    def add_arguments(parser):
        g = parser.add_argument_group("(Evaluator) arguments")

        g.add_argument(
            "--inference_output",
            type=str,
            default="none",
        )
        # FIXME: `choice` is removed. To enforce the choices, `add_arguments` should be inhererited
        g.add_argument(
            "--inference_output_dirname",
            type=str,
            default=None,
        )
        g.add_argument("--inference_output_fullpath", type=str, default=None)

        g.add_argument("--valid_type", default="loop", type=str, choices=["loop", "once"])
        g.add_argument("--max_outputs", default=5, type=int)
        g.add_argument("--sigmoid_threshold", default=0.3, type=float)

        # -- * -- Conversion to TFLITE format -- * --
        g.add_argument("--no-save_tflite", dest="save_tflite", action="store_false")
        g.add_argument("--save_tflite", dest="save_tflite", action="store_true")
        g.set_defaults(save_tflite=False)

        # -- * -- Conversion to NCNN format -- * --
        g.add_argument("--no-save_ncnn", dest="save_ncnn", action="store_false")
        g.add_argument("--save_ncnn", dest="save_ncnn", action="store_true")
        g.set_defaults(save_ncnn=False)

        g.add_argument("--maximum_num_labels_for_metric", default=10, type=int,
                       help="Maximum number of labels for using class-specific metrics(e.g. precision/recall/f1score)")

        g.add_argument("--no-convert_to_pb", dest="convert_to_pb", action="store_false")
        g.add_argument("--convert_to_pb", dest="convert_to_pb", action="store_true")
        g.set_defaults(convert_to_pb=True)

        g.add_argument("--no-save_evaluation_image", dest="save_evaluation_image", action="store_false")
        g.add_argument("--save_evaluation_image", dest="save_evaluation_image", action="store_true")
        g.set_defaults(save_evaluation_image=False)

        g.add_argument("--probcut_threshold_list", default=[0.34]+[x/100 for x in range(85, 100)],
                       type=float, nargs="*",
                       help="Threshold for probcut. DO NOT INCLUDE 1.0")
        g.add_argument("--precision_threshold_list", default=[0.85, 0.9, 0.95, 0.97, 0.99],
                       type=float, nargs="*",
                       help="Threshold for precision cut. DO NOT INCLUDE 1.0")

        g.add_argument("--no-save_best_keeper", dest="save_best_keeper", action="store_false")
        g.add_argument("--save_best_keeper", dest="save_best_keeper", action="store_true")
        g.set_defaults(save_best_keeper=True)

        g.add_argument("--no-flatten_output", dest="flatten_output", action="store_false")
        g.add_argument("--flatten_output", dest="flatten_output", action="store_true")
        g.set_defaults(flatten_output=False)

        g.add_argument("--max_step_from_restore", default=1e20, type=int)


class SingleLabelAudioEvaluator(Evaluator, AudioBase):
    _available_inference_output = ["prob", "label"]
    def __init__(self, model, session, args, dataset, dataset_name):
        super().__init__("SingleLabelAudioEvaluator", model, session, args, dataset, dataset_name)
        self.setup_dataset_related_attr()
        self.setup_metric_manager()
        self.setup_metric_ops()
        self.setup_best_keeper()

    def setup_dataset_related_attr(self):
        assert len(self.dataset.label_names) == self.args.num_classes
        self.use_class_metrics = len(self.dataset.label_names) < self.args.maximum_num_labels_for_metric

    def setup_metric_manager(self):
        self.metric_manager = metric_manager.AudioMetricManager(
            is_training=False,
            use_class_metrics=self.use_class_metrics,
            exclude_metric_names=self.args.exclude_metric_names,
            summary=Summaries(
                session=self.session,
                train_dir=self.build_train_directory(),
                is_training=False,
                max_image_outputs=self.args.max_image_outputs
            ),
        )

    def setup_metric_ops(self):
        losses = self.build_basic_loss_ops()
        spectrograms = self.build_spectrogram_ops()
        self.metric_tf_op = self.metric_manager.build_metric_ops({
            "dataset_split_name": self.dataset_name,
            "label_names": self.dataset.label_names,
            "losses": losses,
            "wavs": self.model.wavs,
            "spectrograms": spectrograms,
        })

    def build_non_tensor_data_from_eval_dict(self, eval_dict, **kwargs):
        return {
            "dataset_split_name": self.dataset.dataset_split_name,
            "label_names": self.dataset.label_names,
            "predictions_onehot": eval_dict["predictions_onehot"],
            "labels_onehot": eval_dict["labels_onehot"],
        }

    def setup_dataset_iterator(self):
        self.dataset.setup_iterator(
            self.session,
            (self.dataset.filenames_placeholder, self.dataset.labels_placeholder, self.dataset.genre_idxs_placeholder),
            (self.dataset.filenames, self.dataset.labels, self.dataset.genre_idxs),
        )

    def save_inference_result(self, eval_dict, checkpoint_path):
        # FIXME: remove if_else stuff by overriding.
        predictions = eval_dict["predictions_onehot"]

        if self.args.inference_output == "prob":
            outputs = predictions
        elif self.args.inference_output == "label" and self.args.task_type == "multitask_exclusive":
            outputs = []
            for itr, task_name in enumerate(self.dataset.task_names):
                outputs.append(
                    np.array(self.dataset.task_label_names[task_name])[predictions[:, itr, :].argmax(axis=1)]
                )
        elif self.args.inference_output == "label" and self.args.task_type != "multitask_exclusive":
            outputs = np.array(self.dataset.label_names)[predictions.argmax(axis=1)]

        if not checkpoint_path.exists():
            checkpoint_path.mkdir()
            self.log.info(f"Make directory {checkpoint_path}")

        if self.args.inference_output_fullpath is not None:
            output_file_fullpath = Path(self.args.inference_output_fullpath)
        else:
            output_file_fullpath = (
                self.build_inference_path(checkpoint_path) / f"{self.args.inference_output}.csv"
            )

        with output_file_fullpath.open("w", newline="") as f:
            writer = csv.writer(f, delimiter=",", quotechar="\"", quoting=csv.QUOTE_MINIMAL)
            if self.args.task_type == "multitask_exclusive":
                _header_row = ["filename"]
                for task_name in self.dataset.task_names:
                    _header_row.append(f"{task_name}")
                writer.writerow(_header_row)
                for filename, output in tqdm(zip(self.dataset.filenames, zip(*outputs))):
                    _row = [filename, *output]
                    writer.writerow(_row)
            elif self.args.inference_output == "prob":
                _header_row = ["filename", *self.dataset.label_names]
                writer.writerow(_header_row)
                for filename, output in tqdm(zip(self.dataset.filenames, outputs)):
                    _row = [filename, *output]
                    writer.writerow(_row)
            elif self.args.inference_output == "label":
                _header_row = ["filename", "label"]
                writer.writerow(_header_row)
                for filename, output in tqdm(zip(self.dataset.filenames, outputs)):
                    _row = [filename, output]
                    writer.writerow(_row)
            else:
                raise ValueError(f"Unsupported pair of task type: {self.args.task_type} "
                                 f"/ inference_output: {self.args.inference_output}")
            f.close()


class MultiLabelAudioEvaluator(Evaluator, AudioBase):
    def __init__(self, model, session, args, dataset, dataset_name):
        super().__init__("MultiLabelAudioEvaluator", model, session, args, dataset, dataset_name)
        self.setup_dataset_related_attr()
        self.setup_metric_manager()
        self.setup_metric_ops()
        self.setup_best_keeper()

    def setup_dataset_related_attr(self):
        assert len(self.dataset.label_names) == self.args.num_classes
        self.use_class_metrics = len(self.dataset.label_names) < self.args.maximum_num_labels_for_metric

    def setup_metric_manager(self):
        self.metric_manager = metric_manager.MultiLabelAudioMetricManager(
            is_training=False,
            use_class_metrics=self.use_class_metrics,
            exclude_metric_names=self.args.exclude_metric_names,
            summary=Summaries(
                session=self.session,
                train_dir=self.build_train_directory(),
                is_training=False,
                max_image_outputs=self.args.max_image_outputs
            ),
        )

    def setup_metric_ops(self):
        losses = self.build_basic_loss_ops()
        spectrograms = self.build_spectrogram_ops()
        self.metric_tf_op = self.metric_manager.build_metric_ops({
            "dataset_split_name": self.dataset_name,
            "label_names": self.dataset.label_names,
            "losses": losses,
            "wavs": self.model.wavs,
            "spectrograms": spectrograms,
        })

    def build_non_tensor_data_from_eval_dict(self, eval_dict, **kwargs):
        return {
            "dataset_split_name": self.dataset.dataset_split_name,
            "label_names": self.dataset.label_names,
            "sigmoid_threshold": self.args.sigmoid_threshold,

            "predictions_multihot": eval_dict["predictions_onehot"],
            "labels_multihot": eval_dict["labels_onehot"],
        }

    def setup_dataset_iterator(self):
        self.dataset.setup_iterator(
            self.session,
            (self.dataset.filenames_placeholder, self.dataset.labels_placeholder),
            (self.dataset.filenames, self.dataset.labels),
        )

    def save_inference_result(self, eval_dict, checkpoint_path):
        # TODO
        pass
