from PIL import Image
from functools import reduce
from operator import mul
from typing import Tuple
from typing import Dict
from types import SimpleNamespace
import types

import numpy as np
import tensorflow as tf
import tensorflow.contrib.slim as slim
from termcolor import colored

import common.tf_utils as tf_utils
import common.utils as utils
from factory.base import CNNModel
from factory.tasks import SingleTask
from factory.tasks import MultiLabelTask
import clfnets.resnext_utils as resnext_utils
import clfnets.mobilenet_v2 as mobilenet_v2
from audio_nets.resnext import resnext_101
from audio_nets.resnext import resnext_50
from audio_nets import kws
from audio_nets import hyper_sound
from audio_nets import audio_rnn


_available_nets = [
    "KWSModel",  #  http://www.isca-speech.org/archive/interspeech_2015/papers/i15_1478.pdf
    "Resnext101Model",
    "Resnext50Model",
    "MobileNetV2",
    "HyperSoundModel",
    "RNNModel",
    "RNNModel2",
]
_available_tasks = {
    "audio_single": SingleTask,
    "audio_multilabel": MultiLabelTask,
}


class AudioNetModel(CNNModel):
    def __init__(self, args, dataset):
        self.log = utils.get_logger("AudioNetModel", None)
        self.dataset = dataset
        self.args = args

        self.TASK_TYPE = SimpleNamespace(
            single="audio_single",
            multilabel="audio_multilabel",
        )
        self.task = _available_tasks[self.args.task_type](args, dataset)

        self.OUTPUT_TYPES = types.SimpleNamespace(
            softmax="softmax",
            sigmoid="sigmoid",
        )

    def build(self, wavs, spectrograms, fingerprints, labels, is_training, genres):
        self.wavs = wavs
        self.spectrograms = spectrograms
        self.fingerprints = fingerprints
        self.is_training = is_training
        self.genres = genres
        self.labels = labels  # will be used in build_evaluation_fetch_ops

        # -- * -- build_output: it includes build_inference
        self.inputs, self.logits, self._outputs, self.endpoints = self.build_output(
            self.fingerprints,
            self.is_training,
            self.args.output_name
        )

        # -- * -- Build loss function: it can be different between each models
        self._total_loss, self._model_loss, self.endpoints_loss = self.build_loss(self.logits, self.outputs, self.labels)

        self.total_params = tf_utils.show_models(self.log)

    @property
    def model_loss(self):
        return self._model_loss

    @property
    def total_loss(self):
        return self._total_loss

    @property
    def images_original(self):
        return self.spectrograms

    @property
    def images(self):
        return self.fingerprints

    @property
    def image_ops(self):
        return self._image_ops

    @property
    def outputs(self):
        return self._outputs

    def build_output(
        self,
        fingerprints: tf.Tensor,
        is_training: tf.placeholder,
        output_name: str
    ) -> [tf.Tensor, tf.Tensor, tf.Tensor, Dict]:
        inputs = self.preprocess_images(
            fingerprints,
            preprocess_method=self.args.preprocess_method,
        )
        logits, endpoints = self.build_inference(inputs, is_training=is_training)
        outputs = self.task.convert(logits, output_name)
        return inputs, logits, outputs, endpoints

    def build_images(self, images):
        images_preprocessed = self.preprocess_images(
            images,
            preprocess_method=self.args.preprocess_method,
        )

        return images_preprocessed

    def build_ground_truths(self, gts):
        return self.preprocess_images(gts, preprocess_method="preprocess_normalize")

    def build_inference(self, fingerprints, is_training=True):
        raise NotImplementedError

    def build_loss(
        self,
        logits: tf.Tensor,
        scores: tf.Tensor,
        labels: tf.Tensor,
    ) -> Tuple[tf.Tensor, tf.Tensor, Dict]:
        total_loss, model_loss, endpoints_loss = self.task.build_loss(
            logits,
            scores,
            labels,
        )
        return total_loss, model_loss, endpoints_loss

    @staticmethod
    def add_arguments(parser):
        pass


class GoogleKWS():
    def __init__(self, args):
        self.args = args

    def build_model_settings(self, inputs):
        model_settings = {}
        model_settings["fingerprint_width"] = inputs.shape.as_list()[2]
        model_settings["spectrogram_length"] = inputs.shape.as_list()[1]
        model_settings["fingerprint_size"] = model_settings["spectrogram_length"] * model_settings["fingerprint_width"]
        model_settings["label_count"] = self.args.num_classes
        model_settings["sample_rate"] = self.args.sample_rate
        model_settings["window_stride_samples"] = int(self.args.sample_rate * self.args.window_stride_ms / 1000)
        return model_settings


class KWSModel(AudioNetModel):
    def __init__(self, args, dataset=None):
        super().__init__(args, dataset)
        self.google_kws = GoogleKWS(args)

    def build_inference(self, inputs, is_training=True):
        endpoints = {}
        logits = kws.create_model(
            inputs,
            model_settings=self.google_kws.build_model_settings(inputs),
            model_architecture=self.args.architecture,
            is_training=is_training,
        )
        return logits, endpoints

    @staticmethod
    def add_arguments(parser):
        parser.add_argument("--architecture", default="conv",
                            choices=["single_fc", "conv", "low_latency_conv", "low_latency_svdf", "tiny_conv"])


class Resnext101Model(AudioNetModel):
    def __init__(self, args, dataset=None):
        super().__init__(args, dataset)

    @staticmethod
    def add_arguments(parser):
        pass

    def build_inference(self, images, is_training):
        with slim.arg_scope(resnext_utils.resnext_arg_scope(batch_norm_fused=self.args.use_fused_batchnorm)):
            logits, endpoints = resnext_101(images,
                                            self.args.num_classes,
                                            is_training=is_training)

            return logits, endpoints


class Resnext50Model(AudioNetModel):
    def __init__(self, args, dataset=None):
        super().__init__(args, dataset)

    @staticmethod
    def add_arguments(parser):
        parser.add_argument("--weight_decay", default=0.0001, type=float)

    def build_inference(self, images, is_training):
        with slim.arg_scope(resnext_utils.resnext_arg_scope(
            batch_norm_fused=self.args.use_fused_batchnorm,
            weight_decay=self.args.weight_decay)
        ):
            logits, endpoints = resnext_50(images,
                                           self.args.num_classes,
                                           is_training=is_training)

            return logits, endpoints


class MobileNetV2(AudioNetModel):
    """[Justin]
    This custom implementation is different from slim official implementation
    https://github.com/tensorflow/models/tree/master/research/slim/nets/mobilenet.
    The difference is that our's have 1x1 convolution at first bottleneck layer while
    official one does not.
    Since it looks like a bug of official one, I decoupled custom implementation from official one.
    """
    def __init__(self, args, dataset=None):
        super().__init__(args, dataset)

    @staticmethod
    def add_arguments(parser):
        parser.add_argument("--depth_multiplier", default=1.0, type=float)
        parser.add_argument("--weight_decay", default=0.00004, type=float)
        parser.add_argument("--dropout_keep_prob", default=0.5, type=float)
        parser.add_argument("--batch_norm_decay", default=0.997, type=float)
        parser.add_argument("--batch_norm_epsilon", default=0.001, type=float)
        parser.add_argument("--no-finegrain_classification_mode",
                            dest="finegrain_classification_mode",
                            action="store_false")
        parser.add_argument("--finegrain_classification_mode",
                            dest="finegrain_classification_mode",
                            action="store_true",
                            help="""
                            finegrain_classification_mode: When set to True, the model
                            will keep the last layer large even for small multipliers. Following
                            https://arxiv.org/abs/1801.04381
                            suggests that it improves performance for ImageNet-type of problems.
                            *Note* ignored if final_endpoint makes the builder exit earlier.
                            """)
        parser.set_defaults(finegrain_classification_mode=False)

    def build_inference(self, images, is_training):
        with slim.arg_scope(mobilenet_v2.mobilenet_v2_arg_scope(is_training=is_training,
                                                                weight_decay=self.args.weight_decay,
                                                                dropout_keep_prob=self.args.dropout_keep_prob,
                                                                use_fused_batchnorm=self.args.use_fused_batchnorm,
                                                                batch_norm_decay=self.args.batch_norm_decay,
                                                                batch_norm_epsilon=self.args.batch_norm_epsilon)):
            NET_DEF = mobilenet_v2.get_net_def(depth_multiplier=self.args.depth_multiplier,
                                               finegrain_classification_mode=self.args.finegrain_classification_mode)

            kwargs = {
                "num_classes": self.args.num_classes,
                "net_def": NET_DEF,
            }

            if self.args.task_type == "audio_multilabel":
                kwargs["prediction_fn"] = tf.sigmoid

            logits, endpoints = mobilenet_v2.mobilenet_v2(images, **kwargs)

            return logits, endpoints


class HyperSoundModel(AudioNetModel):
    def __init__(self, args, dataset=None):
        super().__init__(args, dataset)

    def build_inference(self, images, is_training):
        with slim.arg_scope(hyper_sound.hyper_sound_arg_scope(
            is_training=is_training,
            weight_decay=self.args.weight_decay,
            use_fused_batchnorm=self.args.use_fused_batchnorm,
            batch_norm_decay=self.args.batch_norm_decay,
            batch_norm_epsilon=self.args.batch_norm_epsilon)
        ):
            logits, endpoints = hyper_sound.hyper_sound(
                images,
                self.args.num_classes,
            )
            return logits, endpoints

    @staticmethod
    def add_arguments(parser):
        parser.add_argument("--weight_decay", default=0.00004, type=float)
        parser.add_argument("--batch_norm_decay", default=0.9997, type=float)
        parser.add_argument("--batch_norm_epsilon", default=0.001, type=float)


class RNNModel(AudioNetModel):
    def __init__(self, args, dataset=None):
        super().__init__(args, dataset)

    def build_inference(self, images, is_training):
        with slim.arg_scope(audio_rnn.audio_rnn_arg_scope(
            is_training=is_training,
            weight_decay=self.args.weight_decay,
            use_fused_batchnorm=self.args.use_fused_batchnorm,
            batch_norm_decay=self.args.batch_norm_decay,
            batch_norm_epsilon=self.args.batch_norm_epsilon)
        ):
            if not is_training:
                self.args.v3_keep_prob = 1.0
                self.args.v5_keep_prob = 1.0
                self.args.v8_keep_prob = 1.0
                self.args.v9_keep_prob = 1.0
                self.args.final_keep_prob = 1.0
                print("Update dropout to 1.0 for evaluation")
            logits, endpoints = eval(f"audio_rnn.{self.args.audio_rnn}")(
                tf.squeeze(images, axis=3),
                self.args.num_classes,
                self.args,
            )
            return logits, endpoints

    @staticmethod
    def add_arguments(parser):
        parser.add_argument("--weight_decay", default=0.00004, type=float)
        parser.add_argument("--batch_norm_decay", default=0.997, type=float)
        parser.add_argument("--batch_norm_epsilon", default=0.001, type=float)

        parser.add_argument("--audio_rnn", default="audio_rnn_v1", type=str)

        # rnn_v1
        parser.add_argument("--v1_hidden_state", default=40, type=int)
        parser.add_argument("--v1_cell_type", default="BasicRNNCell", type=str)

        # rnn_v2
        parser.add_argument("--v2_hidden_state", default=64, type=int)
        parser.add_argument("--v2_cell_type", default="LSTMCell", type=str)

        # rnn_v3
        parser.add_argument("--v3_hidden_state", default=64, type=int)
        parser.add_argument("--v3_cell_type", default="LSTMCell", type=str)
        parser.add_argument("--v3_keep_prob", default=0.6, type=float)

        # rnn_v4: For layer normalization
        parser.add_argument("--v4_hidden_state", default=64, type=int)
        parser.add_argument("--v4_cell_type", default="tf.contrib.rnn.LayerNormBasicLSTMCell", type=str)

        # rnn_v5: Bidirectional + dropout
        parser.add_argument("--v5_hidden_state", default=64, type=int)
        parser.add_argument("--v5_keep_prob", default=0.6, type=float)
        parser.add_argument("--v5_num_layers", default=1, type=int)
        parser.add_argument("--v5_cell_type", default="tf.contrib.rnn.GRUCell", type=str)

        # rnn_v6: Bidirectional + LayerNormBasicLSTMCell
        parser.add_argument("--v6_hidden_state", default=64, type=int)
        parser.add_argument("--v6_num_layers", default=1, type=int)

        # rnn_v7: Bidirectional + LayerNormBasicLSTMCell + Attention
        parser.add_argument("--v7_hidden_state", default=64, type=int)
        parser.add_argument("--v7_num_layers", default=1, type=int)
        parser.add_argument("--v7_cell_type", default="tf.contrib.rnn.GRUCell", type=str)

        # rnn_v8: Bidirectional + Dropout + ~~Attention~~
        parser.add_argument("--v8_hidden_state", default=64, type=int)
        parser.add_argument("--v8_num_layers", default=1, type=int)
        parser.add_argument("--v8_cell_type", default="tf.contrib.rnn.GRUCell", type=str)
        parser.add_argument("--v8_keep_prob", default=0.6, type=float)

        # rnn_v9: Bidirectional + Dropout + Attention
        parser.add_argument("--v9_hidden_state", default=64, type=int)
        parser.add_argument("--v9_num_layers", default=1, type=int)
        parser.add_argument("--v9_cell_type", default="tf.contrib.rnn.GRUCell", type=str)
        parser.add_argument("--v9_keep_prob", default=0.6, type=float)

        # rnn_final: Bidirectional + Dropout + Attention
        parser.add_argument("--final_hidden_state", default=100, type=int)
        parser.add_argument("--final_cell_type", default="tf.contrib.rnn.GRUCell", type=str)
        parser.add_argument("--final_dropout", default=1, type=int)
        parser.add_argument("--final_keep_prob", default=1.0, type=float)
        parser.add_argument("--final_attention", default=1, type=int)

        # rnn_final2: Bidirectional + Dropout + Average
        parser.add_argument("--final2_hidden_state", default=100, type=int)
        parser.add_argument("--final2_cell_type", default="tf.contrib.rnn.GRUCell", type=str)
        parser.add_argument("--final2_dropout", default=1, type=int)
        parser.add_argument("--final2_keep_prob", default=1.0, type=float)


class RNNModel2(AudioNetModel):
    def __init__(self, args, dataset=None):
        super().__init__(args, dataset)

    def build_inference(self, images, is_training):
        with slim.arg_scope(audio_rnn.audio_rnn_arg_scope(
            is_training=is_training,
            weight_decay=self.args.weight_decay,
            use_fused_batchnorm=self.args.use_fused_batchnorm,
            batch_norm_decay=self.args.batch_norm_decay,
            batch_norm_epsilon=self.args.batch_norm_epsilon)
        ):
            if not is_training:
                self.args.final3_keep_prob = 1.0
                print("Update dropout to 1.0 for evaluation")
            logits, endpoints = eval(f"audio_rnn.{self.args.audio_rnn}")(
                tf.squeeze(images, axis=3),
                self.args.num_classes,
                self.args,
            )
            endpoints["logits"] = logits

            tf_popularity_matrix = tf.constant(self.dataset.popularity_matrix, dtype=tf.float32)
            popularity_logits = tf.nn.embedding_lookup(tf_popularity_matrix, self.genres)
            endpoints["popularity_logits"] = popularity_logits

            embeddings = tf.Variable(tf.random_uniform(
                [len(self.dataset.genre_categories), self.args.num_classes], -1.0, 1.0
            ))
            trained_logits = tf.nn.embedding_lookup(embeddings, self.genres)
            endpoints["genres_logits"] = trained_logits

            if self.args.final3_merge_method == 0:
                concat_logits = tf.concat([logits, popularity_logits, trained_logits], axis=1)
                final_logits = tf.contrib.layers.fully_connected(concat_logits, self.args.num_classes)
                endpoints["final_logits"] = final_logits
            elif self.args.final3_merge_method == 1:
                stacked_logits = tf.stack([logits, popularity_logits, trained_logits], axis=2)
                final_logits = tf.contrib.layers.fully_connected(stacked_logits, 1)
                final_logits = tf.squeeze(final_logits, axis=2)

            return final_logits, endpoints

    @staticmethod
    def add_arguments(parser):
        parser.add_argument("--weight_decay", default=0.00004, type=float)
        parser.add_argument("--batch_norm_decay", default=0.997, type=float)
        parser.add_argument("--batch_norm_epsilon", default=0.001, type=float)

        parser.add_argument("--audio_rnn", default="audio_rnn_v1", type=str)

        # rnn_final3: Bidirectional + Dropout + Attention + Popularity
        parser.add_argument("--final3_hidden_state", default=100, type=int)
        parser.add_argument("--final3_cell_type", default="tf.contrib.rnn.GRUCell", type=str)
        parser.add_argument("--final3_dropout", default=1, type=int)
        parser.add_argument("--final3_keep_prob", default=1.0, type=float)
        parser.add_argument("--final3_attention", default=1, type=int)
        parser.add_argument("--final3_merge_method", default=0, type=int)
