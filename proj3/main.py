from pathlib import Path
import tensorflow as tf
import pickle
import numpy as np
import argparse
import shutil

from dataloader import get_wav_list
from dataloader import get_label_list
from dataloader import wav_list_to_feature_list
from dataloader import encode_label
from tensorflow.python.platform import gfile
from time import strftime, localtime, time

from official.transformer.model import transformer
from official.transformer.model.model_params import TINY_PARAMS
import utils

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('--epoch_num', default=10000, type=int)
parser.add_argument('--model', default="model11", type=str)
parser.add_argument('--checkpoint_path', default="checkpoint/run-1126-0134", type=str)
parser.add_argument('--num_melbins', default=128, type=int)
parser.add_argument('--dropout_keep_prob', default=0.6, type=float)

parser.add_argument("--no-test_from_ckpt", dest="test_from_ckpt", action="store_false")
parser.add_argument("--test_from_ckpt", dest="test_from_ckpt", action="store_true")
parser.set_defaults(test_from_ckpt=False)
args = parser.parse_args()



# hyperparameters
# TODO : declare additional hyperparameters
# not fixed (change or add hyperparameter as you like)
batch_size = 32
epoch_num = args.epoch_num
learning_rate = 0.001

NUM_WINDOWS = 96
NUM_MEL_BINS = args.num_melbins
NUM_CLASSES = 25


# fixed
data_path = 'data'
feature_path = data_path + f'/features_mel{args.num_melbins}.pkl'
chord_path = data_path + '/chords.pkl'
# True if you want to train, False if you already trained your model
### TODO : IMPORTANT !!! Please change it to False when you submit your code
is_train_mode = not args.test_from_ckpt
### TODO : IMPORTANT !!! Please specify the path where your best model is saved
### example : checkpoint/run-0925-0348
checkpoint_path = args.checkpoint_path


# make xs and ys
wav_list = get_wav_list(data_path, chord_path)

# if there is no feature file, make it
if gfile.Exists(feature_path):
    print('Load feature file')
    with open(feature_path, 'rb') as f:
        valid_wav_list, features_list = pickle.load(f)
else:
    print('Make feature file')
    valid_wav_list, features_list = wav_list_to_feature_list(wav_list, args.num_melbins)
    with open(feature_path, 'wb') as f:
        pickle.dump((valid_wav_list, features_list), f)

# onehot encoding (total 25 chords)
label_list = get_label_list(chord_path, valid_wav_list)
encoded_label_list = [encode_label(labels) for labels in label_list]
encoded_label_list = np.array(encoded_label_list)

# split train and test
split = int(len(wav_list)*0.8)

# build dataset
dataset = tf.data.Dataset.from_tensor_slices((np.array(features_list), encoded_label_list))
train_dataset = dataset.take(split)
test_dataset = dataset.skip(split)

train_dataset = train_dataset.shuffle(buffer_size=len(wav_list) - split)
train_dataset = train_dataset.batch(batch_size)
train_dataset = train_dataset.prefetch(batch_size)
train_dataset = train_dataset.repeat(epoch_num)

# test_dataset = test_dataset.shuffle(buffer_size=split)
test_dataset = test_dataset.batch(batch_size)
test_dataset = test_dataset.prefetch(batch_size)
test_dataset = test_dataset.repeat(1)

iter = tf.data.Iterator.from_structure(train_dataset.output_types, train_dataset.output_shapes)
train_init = iter.make_initializer(train_dataset)
test_init = iter.make_initializer(test_dataset)

# batch of features, batch of labels
X, Y = iter.get_next()


# TODO : build your model here
global_step = tf.Variable(0, trainable=False, name='global_step')


def simple_cnn(X, Y, depth=5):
    X = tf.cast(X, tf.float32)
    X = tf.reshape(X, [-1, NUM_WINDOWS, NUM_MEL_BINS, 1])

    conv = tf.layers.conv2d(X, filters=depth, kernel_size=[1, depth], strides=1, padding="VALID")
    conv = tf.layers.batch_normalization(conv)
    conv = tf.nn.relu(conv)

    conv = tf.layers.conv2d(X, filters=depth, kernel_size=[3, 3], strides=1, padding="SAME")
    conv = tf.layers.batch_normalization(conv)
    conv = tf.nn.relu(conv)

    conv = tf.layers.conv2d(X, filters=depth, kernel_size=[3, 3], strides=1, padding="SAME")
    conv = tf.layers.batch_normalization(conv)
    conv = tf.nn.relu(conv)

    conv = tf.reshape(conv, [-1, NUM_WINDOWS, NUM_MEL_BINS * depth])
    dense = tf.layers.dense(conv, NUM_CLASSES)
    dropout = tf.layers.dropout(dense, rate=0.5, training=is_train_mode)
    logits = tf.reshape(dropout, [-1, NUM_WINDOWS, NUM_CLASSES])

    return logits


def model1(X, Y):
    X = tf.cast(X, tf.float32)
    X = tf.reshape(X, [-1, NUM_WINDOWS, NUM_MEL_BINS])
    Y = tf.cast(Y, tf.float32)

    model = transformer.Transformer(
        TINY_PARAMS,
        is_train_mode,
    )
    logits = model(X, Y)
    return logits


def model2(X, Y):
    X = tf.cast(X, tf.float32)
    X = tf.reshape(X, [-1, NUM_WINDOWS, NUM_MEL_BINS])
    num_layers = 1

    output = X
    for layer in range(num_layers):
        with tf.variable_scope('encoder_{}'.format(layer),reuse=tf.AUTO_REUSE):
            cell_type = tf.contrib.rnn.GRUCell
            cell_fw = cell_type(128)
            cell_bw = cell_type(128)
            outputs, states = tf.nn.bidirectional_dynamic_rnn(cell_fw, cell_bw, output, dtype=tf.float32)
            output = tf.concat(outputs, 2)
    output = tf.layers.dense(output, NUM_CLASSES)
    dropout = tf.layers.dropout(output, rate=0.5, training=is_train_mode)
    logits = tf.reshape(dropout, [-1, NUM_WINDOWS, NUM_CLASSES])
    return logits


def model3(X, Y):
    X = tf.cast(X, tf.float32)
    X = tf.reshape(X, [-1, NUM_WINDOWS, NUM_MEL_BINS])
    num_layers = 1

    output = X
    for layer in range(num_layers):
        with tf.variable_scope('encoder_{}'.format(layer),reuse=tf.AUTO_REUSE):
            cell_type = tf.contrib.rnn.GRUCell
            cell_fw = cell_type(128)
            cell_bw = cell_type(128)
            outputs, states = tf.nn.bidirectional_dynamic_rnn(cell_fw, cell_bw, output, dtype=tf.float32)
            output = tf.concat(outputs, 2)

    unnormalized_attention = tf.contrib.layers.fully_connected(output, NUM_WINDOWS)  # (batch_size, input_time_length, output_time_length), input_time_length=output_time_length
    attention = tf.nn.softmax(unnormalized_attention, axis=1)

    output_trans = tf.transpose(output, [0, 2, 1])  # (batch_size, time_length, hidden) -> (batch_size, hidden, time_length)
    context = tf.matmul(output_trans, attention)
    context = tf.transpose(context, [0, 2, 1])
    output = tf.layers.dense(context, NUM_CLASSES)
    dropout = tf.layers.dropout(output, rate=0.5, training=is_train_mode)
    logits = dropout
    return logits


def model4(X, Y):
    X = tf.cast(X, tf.float32)
    X = tf.reshape(X, [-1, NUM_WINDOWS, NUM_MEL_BINS])
    num_layers = 1
    dropout_keep_prob = args.dropout_keep_prob if is_train_mode else 1.0

    output = X
    for layer in range(num_layers):
        with tf.variable_scope('encoder_{}'.format(layer),reuse=tf.AUTO_REUSE):
            cell_type = tf.contrib.rnn.GRUCell
            cell_fw = cell_type(128)
            cell_fw = tf.contrib.rnn.DropoutWrapper(cell_fw, input_keep_prob=dropout_keep_prob)
            cell_bw = cell_type(128)
            cell_bw = tf.contrib.rnn.DropoutWrapper(cell_bw, input_keep_prob=dropout_keep_prob)
            outputs, states = tf.nn.bidirectional_dynamic_rnn(cell_fw, cell_bw, output, dtype=tf.float32)
            output = tf.concat(outputs, 2)

    unnormalized_attention = tf.contrib.layers.fully_connected(output, NUM_WINDOWS)  # (batch_size, input_time_length, output_time_length), input_time_length=output_time_length
    attention = tf.nn.softmax(unnormalized_attention, axis=1)

    output_trans = tf.transpose(output, [0, 2, 1])  # (batch_size, time_length, hidden) -> (batch_size, hidden, time_length)
    context = tf.matmul(output_trans, attention)
    context = tf.transpose(context, [0, 2, 1])
    output = tf.layers.dense(context, NUM_CLASSES)
    dropout = tf.layers.dropout(output, rate=1-dropout_keep_prob, training=is_train_mode)
    logits = dropout
    return logits


def model5(X, Y):
    X = tf.cast(X, tf.float32)
    X = tf.reshape(X, [-1, NUM_WINDOWS, NUM_MEL_BINS])
    num_layers = 2
    dropout_keep_prob = args.dropout_keep_prob if is_train_mode else 1.0

    output = X
    for layer in range(num_layers):
        with tf.variable_scope('encoder_{}'.format(layer),reuse=tf.AUTO_REUSE):
            cell_type = tf.contrib.rnn.GRUCell
            cell_fw = cell_type(128)
            cell_fw = tf.contrib.rnn.DropoutWrapper(cell_fw, input_keep_prob=dropout_keep_prob)
            cell_bw = cell_type(128)
            cell_bw = tf.contrib.rnn.DropoutWrapper(cell_bw, input_keep_prob=dropout_keep_prob)
            outputs, states = tf.nn.bidirectional_dynamic_rnn(cell_fw, cell_bw, output, dtype=tf.float32)
            output = tf.concat(outputs, 2)

    unnormalized_attention = tf.contrib.layers.fully_connected(output, NUM_WINDOWS)  # (batch_size, input_time_length, output_time_length), input_time_length=output_time_length
    attention = tf.nn.softmax(unnormalized_attention, axis=1)

    output_trans = tf.transpose(output, [0, 2, 1])  # (batch_size, time_length, hidden) -> (batch_size, hidden, time_length)
    context = tf.matmul(output_trans, attention)
    context = tf.transpose(context, [0, 2, 1])
    output = tf.layers.dense(context, NUM_CLASSES)
    dropout = tf.layers.dropout(output, rate=1-dropout_keep_prob, training=is_train_mode)
    logits = dropout
    return logits


def model6(X, Y):
    X = tf.cast(X, tf.float32)
    X = tf.reshape(X, [-1, NUM_WINDOWS, NUM_MEL_BINS])
    num_layers = 1
    hidden_size = 256
    dropout_keep_prob = args.dropout_keep_prob if is_train_mode else 1.0

    output = X
    for layer in range(num_layers):
        with tf.variable_scope('encoder_{}'.format(layer),reuse=tf.AUTO_REUSE):
            cell_type = tf.contrib.rnn.GRUCell
            cell_fw = cell_type(hidden_size)
            cell_fw = tf.contrib.rnn.DropoutWrapper(cell_fw, input_keep_prob=dropout_keep_prob)
            cell_bw = cell_type(hidden_size)
            cell_bw = tf.contrib.rnn.DropoutWrapper(cell_bw, input_keep_prob=dropout_keep_prob)
            outputs, states = tf.nn.bidirectional_dynamic_rnn(cell_fw, cell_bw, output, dtype=tf.float32)
            output = tf.concat(outputs, 2)

    unnormalized_attention = tf.contrib.layers.fully_connected(output, NUM_WINDOWS)  # (batch_size, input_time_length, output_time_length), input_time_length=output_time_length
    attention = tf.nn.softmax(unnormalized_attention, axis=1)
    attention = tf.layers.dropout(attention, rate=1-dropout_keep_prob, training=is_train_mode)

    output_trans = tf.transpose(output, [0, 2, 1])  # (batch_size, time_length, hidden) -> (batch_size, hidden, time_length)
    context = tf.matmul(output_trans, attention)
    context = tf.transpose(context, [0, 2, 1])
    output = tf.layers.dense(context, NUM_CLASSES)
    dropout = tf.layers.dropout(output, rate=1-dropout_keep_prob, training=is_train_mode)
    logits = dropout
    return logits


def model7(X, Y):
    X = tf.cast(X, tf.float32)
    X = tf.reshape(X, [-1, NUM_WINDOWS, NUM_MEL_BINS])
    Y = tf.cast(Y, tf.float32)
    TINY_PARAMS.update(
        default_batch_size=1024,
        default_batch_size_tpu=1024,
        hidden_size=25,  # 25!!
        num_heads=5,
        filter_size=25,
    )
    model = transformer.Transformer(
        TINY_PARAMS,
        is_train_mode,
    )
    logits = model(X, Y)
    return logits


def model8(X, Y):
    X = tf.cast(X, tf.float32)
    X = tf.reshape(X, [-1, NUM_WINDOWS, NUM_MEL_BINS])
    Y = tf.cast(Y, tf.float32)
    TINY_PARAMS.update(
        default_batch_size=1024,
        default_batch_size_tpu=1024,
        hidden_size=128,
        num_heads=4,
        filter_size=128,
    )
    model = transformer.Transformer(
        TINY_PARAMS,
        is_train_mode,
    )
    Y = tf.layers.dense(Y, NUM_MEL_BINS)
    logits = model(X, Y)
    logits = tf.layers.dense(logits, NUM_CLASSES)
    return logits


def model9(X, Y):
    X = tf.cast(X, tf.float32)
    X = tf.reshape(X, [-1, NUM_WINDOWS, NUM_MEL_BINS])
    Y = tf.cast(Y, tf.float32)
    TINY_PARAMS.update(
        default_batch_size=1024,
        default_batch_size_tpu=1024,
        hidden_size=128,
        num_heads=16,
        filter_size=512,
    )
    model = transformer.Transformer(
        TINY_PARAMS,
        is_train_mode,
    )
    Y = tf.layers.dense(Y, NUM_MEL_BINS)
    logits = model(X, Y)
    logits = tf.layers.dense(logits, NUM_CLASSES)
    return logits


def model10(X, Y):
    X = tf.cast(X, tf.float32)
    X = tf.reshape(X, [-1, NUM_WINDOWS, NUM_MEL_BINS])
    Y = tf.cast(Y, tf.float32)
    TINY_PARAMS.update(
        default_batch_size=1024,
        default_batch_size_tpu=1024,
        hidden_size=128,
        num_heads=16,
        filter_size=512,
        num_hidden_layers=3,
    )
    model = transformer.Transformer(
        TINY_PARAMS,
        is_train_mode,
    )
    Y = tf.layers.dense(Y, NUM_MEL_BINS)
    logits = model(X, Y)
    logits = tf.layers.dense(logits, NUM_CLASSES)
    return logits


def model11(X, Y):
    X = tf.cast(X, tf.float32)
    X = tf.reshape(X, [-1, NUM_WINDOWS, NUM_MEL_BINS])
    Y = tf.cast(Y, tf.float32)
    TINY_PARAMS.update(
        default_batch_size=1024,
        default_batch_size_tpu=1024,
        hidden_size=128,
        num_heads=16,
        filter_size=512,
        num_hidden_layers=3,
        layer_postprocess_dropout=0.25,
        attention_dropout=0.25,
        relu_dropout=0.25,
    )
    model = transformer.Transformer(
        TINY_PARAMS,
        is_train_mode,
    )
    Y = tf.layers.dense(Y, NUM_MEL_BINS)
    logits = model(X, Y)
    logits = tf.layers.dense(logits, NUM_CLASSES)
    return logits


def model12(X, Y):
    X = tf.cast(X, tf.float32)
    X = tf.reshape(X, [-1, NUM_WINDOWS, NUM_MEL_BINS])
    Y = tf.cast(Y, tf.float32)
    TINY_PARAMS.update(
        default_batch_size=1024,
        default_batch_size_tpu=1024,
        hidden_size=128,
        num_heads=16,
        filter_size=512,
        num_hidden_layers=6,
        layer_postprocess_dropout=0.40,
        attention_dropout=0.40,
        relu_dropout=0.40,
    )
    model = transformer.Transformer(
        TINY_PARAMS,
        is_train_mode,
    )
    Y = tf.layers.dense(Y, NUM_MEL_BINS)
    logits = model(X, Y)
    logits = tf.layers.dense(logits, NUM_CLASSES)
    return logits


def model13(X, Y):
    X = tf.cast(X, tf.float32)
    X = tf.reshape(X, [-1, NUM_WINDOWS, NUM_MEL_BINS])
    Y = tf.cast(Y, tf.float32)
    TINY_PARAMS.update(
        default_batch_size=1024,
        default_batch_size_tpu=1024,
        hidden_size=128,
        num_heads=4,
        filter_size=128,
        num_hidden_layers=6,
        layer_postprocess_dropout=0.40,
        attention_dropout=0.40,
        relu_dropout=0.40,
    )
    model = transformer.Transformer(
        TINY_PARAMS,
        is_train_mode,
    )
    Y = tf.layers.dense(Y, NUM_MEL_BINS)
    logits = model(X, Y)
    logits = tf.layers.dense(logits, NUM_CLASSES)
    return logits


def model14(X, Y):
    X = tf.cast(X, tf.float32)
    X = tf.reshape(X, [-1, NUM_WINDOWS, NUM_MEL_BINS])
    Y = tf.cast(Y, tf.float32)
    TINY_PARAMS.update(
        default_batch_size=1024,
        default_batch_size_tpu=1024,
        hidden_size=128,
        num_heads=4,
        filter_size=128,
        num_hidden_layers=6,
        layer_postprocess_dropout=0.75,
        attention_dropout=0.75,
        relu_dropout=0.75,
    )
    model = transformer.Transformer(
        TINY_PARAMS,
        is_train_mode,
    )
    Y = tf.layers.dense(Y, NUM_MEL_BINS)
    logits = model(X, Y)
    logits = tf.layers.dense(logits, NUM_CLASSES)
    return logits


def model15(X, Y):
    if is_train_mode:
        drop_rate = 1.0 - args.dropout_keep_prob
    else:
        drop_rate = 0.0
    print(f"drop_rate: {drop_rate} dropout_keep_prob: {args.dropout_keep_prob}")

    X = tf.cast(X, tf.float32)
    X = tf.reshape(X, [-1, NUM_WINDOWS, NUM_MEL_BINS])
    Y = tf.cast(Y, tf.float32)
    TINY_PARAMS.update(
        default_batch_size=1024,
        default_batch_size_tpu=1024,
        hidden_size=128,
        num_heads=4,
        filter_size=128,
        num_hidden_layers=6,
        layer_postprocess_dropout=drop_rate,
        attention_dropout=drop_rate,
        relu_dropout=drop_rate,
    )
    model = transformer.Transformer(
        TINY_PARAMS,
        is_train_mode,
    )
    Y = tf.layers.dense(Y, NUM_MEL_BINS)
    logits = model(X, Y)
    logits = tf.layers.dense(logits, NUM_CLASSES)
    return logits


def model16(X, Y):
    if is_train_mode:
        drop_rate = 1.0 - args.dropout_keep_prob
    else:
        drop_rate = 0.0
    print(f"drop_rate: {drop_rate} dropout_keep_prob: {args.dropout_keep_prob}")

    X = tf.cast(X, tf.float32)
    X = tf.reshape(X, [-1, NUM_WINDOWS, NUM_MEL_BINS])
    Y = tf.cast(Y, tf.float32)
    TINY_PARAMS.update(
        default_batch_size=1024,
        default_batch_size_tpu=1024,
        hidden_size=128,
        num_heads=8,
        filter_size=128,
        num_hidden_layers=6,
        layer_postprocess_dropout=drop_rate,
        attention_dropout=drop_rate,
        relu_dropout=drop_rate,
    )
    model = transformer.Transformer(
        TINY_PARAMS,
        is_train_mode,
    )
    Y = tf.layers.dense(Y, NUM_MEL_BINS)
    logits = model(X, Y)
    logits = tf.layers.dense(logits, NUM_CLASSES)
    return logits


def model17(X, Y):
    if is_train_mode:
        drop_rate = 1.0 - args.dropout_keep_prob
    else:
        drop_rate = 0.0
    print(f"drop_rate: {drop_rate} dropout_keep_prob: {args.dropout_keep_prob}")

    X = tf.cast(X, tf.float32)
    X = tf.reshape(X, [-1, NUM_WINDOWS, NUM_MEL_BINS])
    Y = tf.cast(Y, tf.float32)
    TINY_PARAMS.update(
        default_batch_size=1024,
        default_batch_size_tpu=1024,
        hidden_size=256,
        num_heads=8,
        filter_size=512,
        num_hidden_layers=6,
        layer_postprocess_dropout=drop_rate,
        attention_dropout=drop_rate,
        relu_dropout=drop_rate,
    )
    model = transformer.Transformer(
        TINY_PARAMS,
        is_train_mode,
    )
    Y = tf.layers.dense(Y, NUM_MEL_BINS)
    logits = model(X, Y)
    logits = tf.layers.dense(logits, NUM_CLASSES)
    return logits


logits = eval(args.model)(X, Y)

cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=logits, labels=Y, dim=2))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost, global_step=global_step)

infer = tf.argmax(logits, axis=2)
answer = tf.argmax(Y, axis=2)

# calculate accuracy
correct_prediction = tf.equal(infer, answer)
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# train and evaluate
tfconfig = tf.ConfigProto(
    gpu_options=tf.GPUOptions(allow_growth=True),
    log_device_placement=False,
    device_count={"GPU": 1})
if is_train_mode:
    if checkpoint_path == "checkpoint":
        final_path = checkpoint_path + '/run-%02d%02d-%02d%02d/' % tuple(localtime(time()))[1:5] + args.model
    else:
        final_path = checkpoint_path
    with tf.Session(config=tfconfig) as sess:
        utils.show_models()
        saver = tf.train.Saver()
        ckpt = tf.train.get_checkpoint_state(checkpoint_path)
        if ckpt and tf.train.checkpoint_exists(ckpt.model_checkpoint_path):
            print('Load model from : %s' % ckpt)
            saver.restore(sess, ckpt.model_checkpoint_path)
        else:
            sess.run(tf.global_variables_initializer())

        print('Start training')
        train_total_batch = int(split / batch_size)
        total_epoch = 0
        for epoch in range(epoch_num):
            sess.run(train_init)
            try:
                # TODO: do some train step code here
                print('------------------- epoch:', epoch, ' -------------------')
                for _ in range(train_total_batch):
                    c, _, acc = sess.run([cost, optimizer, accuracy])
                    print('Step: %5d, ' % sess.run(global_step), ' Cost: %.4f ' % c,
                        ' Accuracy: %.4f ' % acc)
            except KeyboardInterrupt:
                break

            # TODO : do accuracy test
            test_total_batch = int((len(wav_list) - split) / batch_size)
            sess.run(test_init)
            acc = 0.0
            for _ in range(test_total_batch):
                acc += sess.run(accuracy)

            print('Epoch: %d  Test accuracy: %.4f' % (epoch, acc/test_total_batch))

            # save checkpoint
            if not gfile.Exists(final_path):
                gfile.MakeDirs(final_path)
            saver.save(sess, final_path, global_step=global_step)
            print('Model saved in file : %s' % final_path)

        print('Training finished!')

        # TODO : do accuracy test
        test_total_batch = int((len(wav_list) - split) / batch_size)
        sess.run(test_init)
        acc = 0.0
        for _ in range(test_total_batch):
            acc += sess.run(accuracy)

        print('Final Test accuracy: %.4f' % (acc/test_total_batch))

        # save checkpoint
        if not gfile.Exists(final_path):
            gfile.MakeDirs(final_path)
        saver.save(sess, final_path, global_step=global_step)
        print('Model saved in file : %s' % final_path)

else:
    final_path = checkpoint_path
    print(f"Monitor {final_path}")
    best_dir = Path(checkpoint_path) / "best"
    best = 0
    for ckpt_path in utils.ckpt_iterator(final_path):
        with tf.Session(config=tfconfig) as sess:
            saver = tf.train.Saver()
            saver.restore(sess, ckpt_path)

            test_total_batch = int((len(wav_list) - split) / batch_size)
            sess.run(test_init)
            acc = 0.0
            y_list = []
            for _ in range(test_total_batch):
                _acc, y = sess.run([accuracy, Y])
                acc += _acc
                y_list.append(y)
            y = np.concatenate(y_list, axis=0)
            # print(y.argmax(axis=2))  # To check data duplicates

            total_acc = acc/test_total_batch
            print(f'{ckpt_path}|Test accuracy from separate process: %.4f' % (total_acc))

            if total_acc > best:
                if best_dir.exists():
                    shutil.rmtree(best_dir)
                best_dir.mkdir(parents=True, exist_ok=True)
                ckpt_path = Path(ckpt_path)
                for ckpt_file in ckpt_path.parent.glob(ckpt_path.name + "*"):
                    shutil.copy(ckpt_file, str(best_dir))
                    print(f"[Keep Best] Copy from {ckpt_file} to {best_dir}")
                with open(str(best_dir / f"acc-{total_acc}"), "w") as f:
                    f.write("")
                best = total_acc
