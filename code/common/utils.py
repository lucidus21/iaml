from pathlib import Path
import sys
import humanfriendly as hf
import contextlib
import argparse
import logging
import getpass
import shutil
import json
import uuid
import time
import copy
import math
from types import SimpleNamespace
from typing import List
from typing import Dict
from datetime import datetime

import tensorflow as tf
import numpy as np
import click
from termcolor import colored


def divisor_generator(n):
    large_divisors = []
    for i in range(1, int(math.sqrt(n) + 1)):
        if n % i == 0:
            yield i
            if i*i != n:
                assert n % i == 0
                large_divisors.append(n // i)
    for divisor in reversed(large_divisors):
        yield divisor


def get_largest_divisor(n, less_than):
    prev = 1
    assert less_than > 1
    for divisor in divisor_generator(n):
        if divisor > less_than:
            return prev
        prev = divisor
    return 1


def calculate_avg_std_efficently(gen, shape, axis=3):
    avg = np.zeros(shape=shape[1:])
    avg_square = np.zeros(shape=shape[1:])
    for n, tensor_batch in enumerate(gen, start=1):
        for idx in range(tensor_batch.shape[0]):  # shape[0] is batch_size
            tensor = tensor_batch[idx]
            avg_square = (n - 1) / n * avg_square + (tensor ** 2) / n
            avg = (n-1) / n * avg + tensor / n
    # avg_square has shape without batch_size
    mean_avg = np.mean(avg, axis=(0, 1))
    var = np.mean(avg_square, axis=(0, 1)) - mean_avg ** 2
    std = np.sqrt(var)
    return mean_avg, std


def check_external_paths(paths, log):
    for p in paths:
        if not Path(p).exists():
            log.info(colored("{} path does not exist!".format(p), "red"))
            time.sleep(1)


def floats_to_string(l):
    return ",".join(["{:.3f}".format(v) for v in l])


def get_model_variables(exclude_prefix):
    if exclude_prefix == [""]:
        return tf.contrib.framework.get_model_variables()
    assert "" not in exclude_prefix

    model_variables = []
    for var in tf.contrib.framework.get_model_variables():
        is_include = True
        for substring in exclude_prefix:
            if substring in var.name:
                is_include = False
        if is_include:
            model_variables.append(var)

    return model_variables


def dump_configuration(train_log_dir, config, filename="config.json"):
    if not Path(train_log_dir).exists():
        Path(train_log_dir).mkdir(parents=True)

    if isinstance(config, argparse.Namespace):
        config = vars(config)
    elif isinstance(config, dict):
        config = config
    else:
        raise ValueError("Unsupported type for configuration: {}".format(type(config)))

    with Path(train_log_dir, filename).open("w") as f:
        json.dump(config, f)


def update_train_dir(args):
    def replace_func(base_string, a, b):
        replaced_string = base_string.replace(a, b)
        print(colored("[update_train_dir] replace {} : {} -> {}".format(a, base_string, replaced_string),
                      "yellow"))
        return replaced_string

    def make_placeholder(s: str, circumfix: str="%"):
        return circumfix + s.upper() + circumfix

    placeholder_mapping = {
        make_placeholder("DATE"): datetime.now().strftime("%y%m%d%H%M%S"),
        make_placeholder("USER"): getpass.getuser(),
    }

    for key, value in placeholder_mapping.items():
        args.train_dir = replace_func(args.train_dir, key, value)

    unknown = "UNKNOWN"
    for key, value in vars(args).items():
        key_placeholder = make_placeholder(key)
        if key_placeholder in args.train_dir:
            replace_value = value
            if isinstance(replace_value, str):
                if "/" in replace_value:
                    replace_value = unknown
            elif isinstance(replace_value, list):
                replace_value = ",".join(map(str, replace_value))
            elif isinstance(replace_value, float) or isinstance(replace_value, int):
                replace_value = str(replace_value)
            elif isinstance(replace_value, bool):
                replace_value = str(replace_value)
            else:
                replace_value = unknown
            args.train_dir = replace_func(args.train_dir, key_placeholder, replace_value)

    print(colored("[update_train_dir] final train_dir {}".format(args.train_dir),
                  "yellow", attrs=["bold", "underline"]))


def exit_handler(train_dir):
    if click.confirm("Do you want to delete {}?".format(train_dir), abort=True):
        shutil.rmtree(train_dir)
        print("... delete {} done!".format(train_dir))


def str2bool(v):
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise argparse.ArgumentTypeError("Boolean value expected.")


def positive_int(value):
    ivalue = int(value)
    if ivalue <= 0:
        raise argparse.ArgumentTypeError("%s is an invalid positive int value" % value)
    return ivalue


def get_subparser_argument_list(parser, subparser_name):
    # Hack argparse and get subparser's arguments
    from argparse import _SubParsersAction
    argument_list = []
    for sub_parser_action in filter(lambda x: isinstance(x, _SubParsersAction), parser._subparsers._actions):
        for action in sub_parser_action.choices[subparser_name]._actions:
            arg = action.option_strings[-1].replace("--", "")
            if arg == "help":
                continue
            if arg.startswith("no-"):
                continue
            argument_list.append(arg)
    return argument_list


def extract_arguments(parser):
    from argparse import _StoreAction, _StoreTrueAction
    argument_list = []
    for action in parser._actions:
        if isinstance(action, _StoreAction) or isinstance(action, _StoreTrueAction):
            option_string = action.option_strings
            assert len(option_string) == 1
            argument_list.append(option_string[0].replace("--"))
    return argument_list


def reconstruct_argv(args, keys):
    argv = ""
    for key in keys:
        val = getattr(args, key)
        if isinstance(val, list):
            argv += "--{} {} ".format(key, " ".join(map(lambda x: str(x), val)))
        elif val == "":
            argv += "--{} {} ".format(key, "''")
        elif isinstance(val, bool):
            if val:
                argv += "--{} ".format(key)
            else:
                argv += "--no-{} ".format(key)
        else:
            argv += "--{} {} ".format(key, val)
    return argv


def get_logger(logger_name, log_file: Path=None, level=logging.DEBUG):
    # "log/data-pipe-{}.log".format(datetime.now().strftime("%Y-%m-%d-%H-%M-%S"))
    logger = logging.getLogger(logger_name)

    if not logger.hasHandlers():
        formatter = logging.Formatter("[%(levelname)s|%(filename)s:%(lineno)s] %(asctime)s > %(message)s")

        logger.setLevel(level)

        if log_file is not None:
            log_file.parent.mkdir(parents=True, exist_ok=True)
            fileHandler = logging.FileHandler(log_file, mode="w")
            fileHandler.setFormatter(formatter)
            logger.addHandler(fileHandler)

        streamHandler = logging.StreamHandler()
        streamHandler.setFormatter(formatter)
        logger.addHandler(streamHandler)

    return logger


def format_timespan(duration):
    if duration < 10:
        readable_duration = "{:.1f} (ms)".format(duration * 1000)
    else:
        readable_duration = hf.format_timespan(duration)
    return readable_duration


@contextlib.contextmanager
def timer(name):
    st = time.time()
    yield
    print("<Timer> {} : {}".format(name, format_timespan(time.time() - st)))


def timeit(method):
    def timed(*args, **kw):
        hf_timer = hf.Timer()
        result = method(*args, **kw)
        print("<Timeit> {!r} ({!r}, {!r}) {}".format(method.__name__, args, kw, hf_timer.rounded))
        return result
    return timed


class Timer(object):
    def __init__(self, log):
        self.log = log

    @contextlib.contextmanager
    def __call__(self, name, log_func=None):
        """
        Example.
            timer = Timer(log)
            with timer("Some Routines"):
                routine1()
                routine2()
        """
        if log_func is None:
            log_func = self.log.info

        start = time.clock()
        yield
        end = time.clock()
        duration = end - start
        readable_duration = format_timespan(duration)
        log_func(f"{name} :: {readable_duration}")


class TextFormatter(object):
    def __init__(self, color, attrs):
        self.color = color
        self.attrs = attrs

    def __call__(self, string):
        return colored(string, self.color, attrs=self.attrs)


class LogFormatter(object):
    def __init__(self, log, color, attrs):
        self.log = log
        self.color = color
        self.attrs = attrs

    def __call__(self, string):
        return self.log(colored(string, self.color, attrs=self.attrs))


@contextlib.contextmanager
def format_text(color, attrs=None):
    yield TextFormatter(color, attrs)


def format_text_fun(color, attrs=None):
    return TextFormatter(color, attrs)


def format_log(log, color, attrs=None):
    return LogFormatter(log, color, attrs)


@contextlib.contextmanager
def smart_log(log, msg):
    log.info(f"{msg} started.")
    yield
    log.info(f"{msg} finished.")


def get_val_from_dict(name, dict_map, dict_name):
    if name not in dict_map:
        raise ValueError(f"Unknown name of {dict_name}: {name}")
    return dict_map[name]


class CustomMultiSubparser(object):
    """
    CustomMultiSubparser helps with defining several mutually inclusive/exclusive subparsers.

    # Let's say we want to train using distillation and both models accept different
    # input image sizes, preprocessing and augmentation method. However, there are
    # several arguments that are shared between models.

    python3 distill.py \
    --dataset_split_name train \
    --dataset_path ../data/openimages/images/train \
    --input_file ../data/openimages/annotations/train/annotations.csv \
    --description_file ../data/openimages/annotations/description.csv \
    --train_dir ../working/distillation \
    --num_classes 5000 \
    --batch_size 2 \
    --task_type multilabel \
    --input_name input/image \
    --output_name output/sigmoid \
    --output_type sigmoid \
    --teacher \
        --checkpoint_path oidv2-resnet_v1_101.ckpt \
        --height 299 \
        --width 299 \
        --augmentation_method open_images_aug \
        --preprocess_method no_preprocessing \
        --prefix_scope resnet_v1_101 \
        --model Resnet101 \
    --student \
        --checkpoint_path mobilenet_v1_1.0_224.ckpt \
        --height 224 \
        --width 224 \
        --augmentation_method min_edge_random_crop \
        --preprocess_method preprocess_inception \
        --prefix_scope MobilenetV1 \
        --model MobileNetV1

    # `ArgumentParser` is defined as usually without any restrictions.
    parser = argparse.ArgumentParser()
    parser.add_argument("--width", type=int)
    parser.add_argument("--height", type=int)
    ...
    subparsers = parser.add_subparsers(title="Model", description="")
    argparse_subparser = "--model"

    for class_name in clfnets._available_nets:
        subparser = subparsers.add_parser(class_name)
        subparser.add_argument(argparse_subparser, default=class_name,
                               type=str, help="DO NOT FIX ME")

    # However, parsing should be done using `CustomMultiSubparser`.
    # `CustomMultiSubparser` is initialized with list of subparser names
    # ["--student", "--teacher"] that will be used to split argument list
    # and `ArgumentParser`'s subparser.
    subparser = CustomMultiSubparser(parser,
                                     ["--student", "--teacher"],
                                     argparse_subparser)
    args = subparser.parse()

    # `args` now contain 2-level hierarchy of argument values.
    # The first level can be accessed through the name of subparsers.
    args.student
    args.teacher

    # The second level contains the same argument names for any subparser, but the values
    # differ for argument that were defined within subparser (e.g. "--teacher").

    # Different values
    args.student.height # 224
    args.teacher.height # 299

    args.student.model # MobileNetV1
    args.teacher.model # Resnet101

    # The same values
    args.student.num_classes # 5000
    args.teacher.num_classes # 5000
    """
    def __init__(
        self,
        parser: argparse.ArgumentParser,
        subparsers_args: List[str],
        argparse_subparser: str=""
    ):
        self.parser = parser
        self.subparser_args = subparsers_args
        self.argparse_subparser = f" {argparse_subparser} "
        self.namespace = SimpleNamespace()

    def _split_by_subparser(
        self,
        subarguments: Dict[str, str],
        subparser: str
    ):
        subarguments_tmp = copy.deepcopy(subarguments)
        for key, val in subarguments_tmp.items():
            out = subarguments[key].split(subparser)
            if len(out) > 1:
                unknown_out, subparser_out = out
                subarguments[key] = unknown_out.strip()
                subarguments[subparser] = subparser_out.strip()

        return subarguments

    def _remove_double_dash(
        self,
        string: str
    ):
        if string.startswith("--"):
            return string[2:]

    def _remove_empty_strings(
        self,
        l: List[str]
    ):
        return list(filter(lambda x: len(x) > 0, l))

    def parse(
        self,
        arguments: str=None
    ):
        if arguments is None:
            arguments = " ".join(sys.argv[1:])
        # TODO: need to handle not only "--help" case
        if arguments == "-h" or arguments == "--help":
            self.parser.parse_args([arguments])

        assert arguments.strip() != ""

        main_arg = "main"
        subparsers = {main_arg: arguments}

        for s_arg in self.subparser_args:
            subparsers = self._split_by_subparser(subparsers, s_arg)

        for s_arg in self.subparser_args:
            if subparsers.get(s_arg) is not None:
                subparsers[s_arg] = f"{subparsers[main_arg]} {subparsers[s_arg]}"
                if self.argparse_subparser != " ":
                    subparsers[s_arg] = subparsers[s_arg].replace(self.argparse_subparser, " ")

                setattr(self.namespace,
                        self._remove_double_dash(s_arg),
                        self.parser.parse_args(self._remove_empty_strings(subparsers[s_arg].split(" "))))

        return self.namespace


def setup_step1_mode(args):
    args.step_evaluation = 1
    args.step_validation = 1
    args.step_minimum_save = 0
    args.step_save_checkpoint = 1
    args.step_save_summaries = 1

    log = get_logger("utils")
    log.info(colored("Update step_evaluation, step_validation, step_save_checkpoint ... to 1",
                     "yellow"))


def unpack_metric_keys(metric_keys):
    def _parse(key):
        if isinstance(key, str):
            return [key]
        elif isinstance(key, list):
            merged = []
            for k in key:
                merged.extend(_parse(k))
            return merged
        else:
            raise ValueError(f"Unsupported metric key type {type(key)} ({key})")

    return _parse(list(vars(metric_keys).values()))


def wait(message, stop_checker_closure):
    assert callable(stop_checker_closure)
    st = time.time()
    while True:
        try:
            time_pass = hf.format_timespan(int(time.time() - st))
            sys.stdout.write(colored((
                f"{message}. Do you wanna wait? If not, then ctrl+c! :: waiting time: {time_pass}\r"
            ), "yellow", attrs=["bold"]))
            sys.stdout.flush()
            time.sleep(1)
            if stop_checker_closure():
                break
        except KeyboardInterrupt:
            break


class MLNamespace(SimpleNamespace):
    def __init__(self, *args, **kwargs):
        for kwarg in kwargs.keys():
            assert kwarg not in dir(self)
        super().__init__(*args, **kwargs)

    def unordered_values(self):
        return list(vars(self).values())

    def __setitem__(self, key, value):
        setattr(self, key, value)


def extract_kwargs(
    args: List,
    **kwargs: Dict,
) -> Dict:
    """Create a dictionary by extracting values from `kwargs` dictionary. Keep only values
    that are in `args`.

    Args:
        args: (List) list of desired arguments
        **kwargs: (Dict)

    Returns:
        final_kwargs: (Dict)
    """
    final_kwargs = {}
    for key in args:
        assert key in kwargs.keys(), f"{key} is not found in kwargs"
        final_kwargs[key] = kwargs[key]
    return final_kwargs
