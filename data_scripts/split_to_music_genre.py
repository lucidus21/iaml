from pathlib import Path
import subprocess
import shutil

from tqdm import tqdm
import pandas as pd
import argparse

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('--source_dir', type=str)
parser.add_argument('--target_dir', type=str)
parser.add_argument('--metadata', type=str)
parser.add_argument('--label', type=str, default="track_genre")
args = parser.parse_args()


df = pd.read_csv(args.metadata)
df["track_id"] = df.track_id.apply(lambda tid: "{:06d}".format(tid))


noexists = 0
for index, row in df[["track_id", args.label, "split"]].iterrows():
    track_id, label, split = row["track_id"], args.label, row["split"]
    track_dir = track_id[:3]
    source_path = f"{args.source_dir}/{track_dir}/{track_id}.mp3"
    noexists += (not Path(source_path).exists())
    print(source_path, Path(source_path).exists())
assert noexists == 0, f"noexists: {noexists}"


for index, row in tqdm(df[["track_id", args.label, "split"]].iterrows()):
    track_id, label, split = row["track_id"], row[args.label], row["split"]
    label = label.replace(" ", "-")
    track_dir = track_id[:3]
    source_path = f"{args.source_dir}/{track_dir}/{track_id}.mp3"
    target_path = f"{args.target_dir}/{split}/{label}/{track_id}.wav"
    Path(target_path).parent.mkdir(parents=True, exist_ok=True)

    subprocess.call(f"ffmpeg -loglevel panic -y -i {source_path} -ar 16000 {target_path}", shell=True)
