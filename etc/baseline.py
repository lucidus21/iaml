import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import OneHotEncoder
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score

enc1 = OneHotEncoder(handle_unknown='ignore')
enc2 = OneHotEncoder(handle_unknown='ignore')

df = pd.read_csv("track_metadata.csv")
train = df[df.split == "training"]
validation = df[df.split == "validation"]

enc1.fit(train["track_genre"].values[:, np.newaxis])
enc2.fit(train["track_listens"].values[:, np.newaxis])

train_X = enc1.transform(train["track_genre"].values[:, np.newaxis])
train_y = enc2.transform(train["track_listens"].values[:, np.newaxis])

validation_X = enc1.transform(validation["track_genre"].values[:, np.newaxis])
validation_y = enc2.transform(validation["track_listens"].values[:, np.newaxis])

clf = LogisticRegression(multi_class="multinomial", solver='lbfgs')
clf.fit(train_X, train_y)
validation_pred = clf.predict(validation_X)

print(accuracy_score(validation_y, validation_pred))
