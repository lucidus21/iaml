from collections import namedtuple

import numpy as np
from PIL import Image
from overload import overload

from metrics.ops.base_ops import NonTensorMetricOpBase
from metrics.ops.base_ops import TensorMetricOpBase
from metrics.summaries import BaseSummaries
import metrics.parser as parser


class InferenceTimeMetricOp(NonTensorMetricOpBase):
    """
    Inference Time Metric.
    """
    _properties = {
        "is_for_summary": True,
        "is_for_best_keep": False,
        "is_for_log": True,
        "valid_input_data_parsers": [
            parser.SingleLabelClassificationDataParser,
            parser.SegmentationDataParser,
            parser.ImageGenerateDataParser,
        ],
        "summary_collection_key": BaseSummaries.KEY_TYPES.DEFAULT,
        "summary_value_type": BaseSummaries.VALUE_TYPES.PLACEHOLDER,
        "min_max_mode": None,
    }

    def __str__(self):
        return "inference_time_metric"

    def build_op(self,
                 data):
        return {
            f"misc/batch_infer_time/{data.dataset_split_name}": None,
            f"misc/unit_infer_time/{data.dataset_split_name}": None,
        }

    def evaluate(self,
                 data):
        return {
            f"misc/batch_infer_time/{data.dataset_split_name}": np.mean(data.batch_infer_time),
            f"misc/unit_infer_time/{data.dataset_split_name}": np.mean(data.unit_infer_time),
        }


class ProbPrecisionCutMetricOp(NonTensorMetricOpBase):
    """
    Per class Precision & Probability Cut Metric.
    The result is saved to file.
    """
    _properties = {
        "is_for_summary": False,
        "is_for_best_keep": False,
        "is_for_log": True,
        "valid_input_data_parsers": [
            parser.SingleLabelClassificationDataParser,
            parser.MultiTaskClassificationDataParser,
        ],
        "summary_collection_key": None,
        "summary_value_type": None,
        "min_max_mode": None,
    }

    def __str__(self):
        return "prob_precision_cut_metric"

    def build_op(self,
                 data):
        return {
            f"prob_cut/{data.dataset_split_name}": None,
            f"precision_cut/{data.dataset_split_name}": None,
        }

    @overload
    def evaluate(self,
                 data: parser.SingleLabelClassificationDataParser.OutputNonTensorData):
        probcut_result = []
        precision_cut_result = []

        for label_idx, label_name in enumerate(data.label_names):
            gt_positive = data.labels_onehot[:, label_idx]
            pred_prob = data.predictions_onehot[:, label_idx]

            probcut_label_result, precision_cut_label_result = self._calc_by_probcut_and_precision_cut(
                gt_positive, pred_prob,
                data.probcut_precision_cut_args.probcut_threshold_list,
                data.probcut_precision_cut_args.precision_threshold_list)

            probcut_result.append(probcut_label_result)
            precision_cut_result.append(precision_cut_label_result)

        # formatting
        probcut_string, precision_cut_string = \
            self._formatting_single_task(probcut_result,
                                         precision_cut_result,
                                         data.label_names,
                                         data.probcut_precision_cut_args.probcut_threshold_list,
                                         data.probcut_precision_cut_args.precision_threshold_list)

        # write
        self._write(data.probcut_precision_cut_args.probcut_save_fullpath,
                    probcut_string,
                    data.probcut_precision_cut_args.precision_cut_save_fullpath,
                    precision_cut_string)

        return {
            f"prob_cut/{data.dataset_split_name}": probcut_string,
            f"precision_cut/{data.dataset_split_name}": precision_cut_string,
        }

    @evaluate.add
    def evaluate(self,
                 data: parser.MultiTaskClassificationDataParser.OutputNonTensorData):
        probcut_result = dict()
        precision_cut_result = dict()

        for task_idx, task_name in enumerate(data.task_names):
            if data.use_class_metrics[task_name]:
                num_labels = data.num_task_labels[task_name]
                valid_idx = data.valid_indexes[:, task_idx]

                probcut_task_result = []
                precision_cut_task_result = []

                for label_idx in range(num_labels):
                    gt_positive = data.labels_onehot[valid_idx, task_idx, label_idx]
                    pred_prob = data.predictions_onehot[valid_idx, task_idx, label_idx]

                    probcut_label_result, precision_cut_label_result = self._calc_by_probcut_and_precision_cut(
                        gt_positive, pred_prob,
                        data.probcut_precision_cut_args.probcut_threshold_list,
                        data.probcut_precision_cut_args.precision_threshold_list)

                    probcut_task_result.append(probcut_label_result)
                    precision_cut_task_result.append(precision_cut_label_result)

                probcut_result[task_name] = probcut_task_result
                precision_cut_result[task_name] = precision_cut_task_result

        # formatting
        probcut_string, precision_cut_string = \
            self._formatting_multi_task(probcut_result,
                                        precision_cut_result,
                                        data.task_names,
                                        data.task_label_names,
                                        data.probcut_precision_cut_args.probcut_threshold_list,
                                        data.probcut_precision_cut_args.precision_threshold_list)

        # write
        self._write(data.probcut_precision_cut_args.probcut_save_fullpath,
                    probcut_string,
                    data.probcut_precision_cut_args.precision_cut_save_fullpath,
                    precision_cut_string)

        return {
            f"prob_cut/{data.dataset_split_name}": probcut_string,
            f"precision_cut/{data.dataset_split_name}": precision_cut_string,
        }

    @staticmethod
    def _calc_by_probcut_and_precision_cut(gt_positive, pred_prob,
                                           probcut_threshold_list, precision_threshold_list):
        Metric = namedtuple("Metric", ["precision",
                                       "recall",
                                       "f1score",
                                       "predicted_positive_ratio",
                                       "prob"])
        NULL_METRIC = Metric(0., 0., 0., 0., 0.)
        NULL_CUT = 2.

        def precision2key(precision):
            assert 0. <= precision <= 1.
            return int(precision * 100)

        def key2precision(precision_key):
            assert 0 <= precision_key
            return precision_key * 0.01

        def get_precision(tp, fp):
            if tp == 0:
                return 0.0
            v = tp / (tp + fp)
            assert 0. <= v <= 1.
            return v

        def get_recall(tp, fn):
            if tp == 0:
                return 0.0
            v = tp / (tp + fn)
            assert 0. <= v <= 1.
            return v

        def get_f1_score(precision, recall):
            assert precision >= 0
            assert recall >= 0

            if precision == 0.0 or recall == 0.0:
                return 0.0
            return 2 * (precision * recall) / (precision + recall)

        def get_predicted_positive_ratio(tp, fp, total):
            v = (tp + fp) / total
            assert 0. <= v <= 1.
            return v

        def get_metric(tp, fp, fn, total, prob):
            precision = get_precision(tp, fp)
            recall = get_recall(tp, fn)
            f1score = get_f1_score(precision, recall)
            predicted_positive_ratio = get_predicted_positive_ratio(tp, fp, total)
            return Metric(precision=precision, recall=recall, f1score=f1score,
                          predicted_positive_ratio=predicted_positive_ratio, prob=prob)

        num_data = gt_positive.shape[0]
        tp = np.sum(gt_positive == 1.0)
        fp = num_data - tp
        fn, tn = 0, 0

        metric = get_metric(tp, fp, fn, num_data, prob=0.0)
        precision_key = precision2key(metric.precision)

        prec2recall = {precision_key: metric}  # key -> (precision, recall, f1score, predicted_positive_ratio, prob)

        probcut_iter = iter(sorted(probcut_threshold_list))
        prob_cut = next(probcut_iter, NULL_CUT)

        probcut_label_result = []
        for gt, prob in sorted(zip(gt_positive, pred_prob), key=lambda x: x[1])[:-1]:
            while prob_cut < prob:
                probcut_label_result.append(metric)
                prob_cut = next(probcut_iter, NULL_CUT)

            if gt == 1.0:
                tp -= 1
                fn += 1
            else:
                fp -= 1
                tn += 1

            metric = get_metric(tp, fp, fn, num_data, prob)
            precision_key = precision2key(metric.precision)
            old_metric = prec2recall.get(precision_key, NULL_METRIC)

            if metric.recall >= old_metric.recall:
                prec2recall[precision_key] = metric

        while prob_cut < NULL_CUT:
            # max prob is lower than current prob_cut
            probcut_label_result.append(NULL_METRIC)
            prob_cut = next(probcut_iter, NULL_CUT)

        # [Justin]
        # precision cut cannot be done at the upper loop
        # since precision is not monotonic to the probability.
        precision_cut_label_result = []
        precision_cut_iter = iter(sorted(precision_threshold_list))
        pr_cut = next(precision_cut_iter, NULL_CUT)
        for precision_key, metric in sorted(prec2recall.items(), key=lambda x: x[0]):
            bucket_prec = key2precision(precision_key)
            while pr_cut <= bucket_prec:
                precision_cut_label_result.append(metric)
                pr_cut = next(precision_cut_iter, NULL_CUT)

        while pr_cut < NULL_CUT:
            # max prec is lower than current pr_cut
            precision_cut_label_result.append(NULL_METRIC)
            pr_cut = next(precision_cut_iter, NULL_CUT)

        return probcut_label_result, precision_cut_label_result

    @staticmethod
    def _write(probcut_save_fullpath,
               probcut_string,
               precision_cut_save_fullpath,
               precision_cut_string):
        with open(probcut_save_fullpath, "w") as fw:
            fw.write(probcut_string)

        with open(precision_cut_save_fullpath, "w") as fw:
            fw.write(precision_cut_string)

    @staticmethod
    def _formatting_single_task(probcut_result,
                                precision_cut_result,
                                label_names,
                                probcut_threshold_list,
                                precision_threshold_list):
        # prob cut
        probcut_string = "[Probcut Metric Reports]\n"

        for label_idx, label_name in enumerate(label_names):
            probcut_string += f"[{label_idx}. {label_name}]\n"
            probcut_string += "\t".join([
                f"{'threshold':>10}",
                f"{'precision':>10}",
                f"{'recall':>10}",
                f"{'f1-score':>10}",
                f"{'ppr':>10}\n",
            ])
            for thres_idx, threshold in enumerate(probcut_threshold_list):
                metric = probcut_result[label_idx][thres_idx]
                probcut_string += "\t".join([
                    f"{threshold:>10.2f}",
                    f"{metric.precision:>10.4f}",
                    f"{metric.recall:>10.4f}",
                    f"{metric.f1score:>10.4f}",
                    f"{metric.predicted_positive_ratio:>10.4f}\n",
                ])
            probcut_string += "\n"

        # precision cut
        precision_cut_string = "[Precision Cut Metric Reports]\n"

        for label_idx, label_name in enumerate(label_names):
            precision_cut_string += f"[{label_idx}. {label_name}]\n"
            precision_cut_string += "\t".join([
                f"{'threshold':>10}",
                f"{'precision':>10}",
                f"{'recall':>10}",
                f"{'prob':>10}",
                f"{'f1-score':>10}",
                f"{'ppr':>10}\n",
            ])
            for thres_idx, threshold in enumerate(precision_threshold_list):
                metric = precision_cut_result[label_idx][thres_idx]
                precision_cut_string += "\t".join([
                    f"{threshold:>10.2f}",
                    f"{metric.precision:>10.4f}",
                    f"{metric.recall:>10.4f}",
                    f"{metric.prob:>10.4f}",
                    f"{metric.f1score:>10.4f}",
                    f"{metric.predicted_positive_ratio:>10.4f}\n",
                ])
            precision_cut_string += "\n"

        return probcut_string, precision_cut_string

    @staticmethod
    def _formatting_multi_task(probcut_result,
                               precision_cut_result,
                               task_names,
                               task_label_names,
                               probcut_threshold_list,
                               precision_threshold_list):
        # prob cut
        probcut_string = "[Probcut Metric Reports]\n"

        for task_idx, task_name in enumerate(task_names):
            if task_name in probcut_result:
                for label_idx, label_name in enumerate(task_label_names[task_name]):
                    probcut_string += f"[{task_idx}-{label_idx}. {task_name} > {label_name}]\n"
                    probcut_string += "\t".join([
                        f"{'threshold':>10}",
                        f"{'precision':>10}",
                        f"{'recall':>10}",
                        f"{'f1-score':>10}",
                        f"{'ppr':>10}\n",
                    ])
                for thres_idx, threshold in enumerate(probcut_threshold_list):
                    metric = probcut_result[task_name][label_idx][thres_idx]
                    probcut_string += "\t".join([
                        f"{threshold:>10.2f}",
                        f"{metric.precision:>10.4f}",
                        f"{metric.recall:>10.4f}",
                        f"{metric.f1score:>10.4f}",
                        f"{metric.predicted_positive_ratio:>10.4f}\n",
                    ])
                probcut_string += "\n"

        # precision cut
        precision_cut_string = "[Precision Cut Metric Reports]\n"

        for task_idx, task_name in enumerate(task_names):
            if task_name in precision_cut_result:
                for label_idx, label_name in enumerate(task_label_names[task_name]):
                    precision_cut_string += f"[{task_idx}-{label_idx}. {task_name} > {label_name}]\n"
                    precision_cut_string += "\t".join([
                        f"{'threshold':>10}",
                        f"{'precision':>10}",
                        f"{'recall':>10}",
                        f"{'prob':>10}",
                        f"{'f1-score':>10}",
                        f"{'ppr':>10}\n",
                    ])
                    for thres_idx, threshold in enumerate(precision_threshold_list):
                        metric = precision_cut_result[task_name][label_idx][thres_idx]
                        precision_cut_string += "\t".join([
                            f"{threshold:>10.2f}",
                            f"{metric.precision:>10.4f}",
                            f"{metric.recall:>10.4f}",
                            f"{metric.prob:>10.4f}",
                            f"{metric.f1score:>10.4f}",
                            f"{metric.predicted_positive_ratio:>10.4f}\n",
                        ])
                    precision_cut_string += "\n"

        return probcut_string, precision_cut_string


class MiscImageRetrieveOp(TensorMetricOpBase):
    """ Image not recoreded on summary but to be retrieved.
    """
    _properties = {
        "is_for_summary": False,
        "is_for_best_keep": False,
        "is_for_log": False,
        "valid_input_data_parsers": [
            parser.SegmentationDataParser,
        ],
        "summary_collection_key": None,
        "summary_value_type": None,
        "min_max_mode": None,
    }

    def __str__(self):
        return "misc_image_retrieve"

    def build_op(self, data):
        result = dict()

        for name, op in data.misc_images.items():
            key = f"misc_images/{data.dataset_split_name}/{name}"
            result[key] = op

        return result

    def expectation_of(self, data):
        # we don't aggregate this output
        pass


class MiscImageSaveOp(NonTensorMetricOpBase):
    """ Image save
    """
    _properties = {
        "is_for_summary": False,
        "is_for_best_keep": False,
        "is_for_log": True,
        "valid_input_data_parsers": [
            parser.SegmentationDataParser,
        ],
        "summary_collection_key": None,
        "summary_value_type": None,
        "min_max_mode": None,
    }

    def __str__(self):
        return "misc_image_save"

    def build_op(self, data):
        return {
            f"save_images/{data.dataset_split_name}": None
        }

    def evaluate(self, data):
        keys = []
        images = []
        for key, image in data.misc_images.items():
            keys.append(key)
            images.append(image)

        # meta info
        num_data = images[0].shape[0]
        h, w = images[0].shape[1:3]

        for nidx in range(num_data):
            _images = [Image.fromarray(img[nidx]) for img in images]

            merged_image = Image.new("RGB", (h * ((len(_images)-1) // 3 + 1), w * 3))
            for i, img in enumerate(_images):
                row = i // 3
                col = i % 3

                merged_image.paste(img, (row*h, col*w, (row+1)*h, (col+1)*w))
            merged_image.save(data.image_save_dir / f"img_{nidx}.jpg")

        # msg
        msg = f"{num_data} images are saved under {data.image_save_dir.resolve()}"

        return {
            f"save_images/{data.dataset_split_name}": msg
        }
