# Tensorflow mandates these.
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from collections import namedtuple
from functools import partial

import tensorflow as tf

slim = tf.contrib.slim


_DEFAULT = {
    "type": None,  # ["conv", "bottleneck"]
    "kernel": 3,
    "stride": 1,
    "depth": None,
    "expansion": 1,
    "repeats": 1,
    "scope": None,
}

_Block = namedtuple("Block", _DEFAULT.keys())
Block = partial(_Block, **_DEFAULT)

NET_DEF = [
    Block(type="conv", kernel=3, depth=32, stride=2, scope="init_block"),
    Block(type="bottleneck", expansion=1, depth=16, repeats=1, stride=1, scope="bottleneck_1"),
    Block(type="bottleneck", expansion=6, depth=24, repeats=2, stride=2, scope="bottleneck_2"),
    Block(type="bottleneck", expansion=6, depth=32, repeats=3, stride=2, scope="bottleneck_3"),
    Block(type="bottleneck", expansion=6, depth=64, repeats=4, stride=2, scope="bottleneck_4"),
    Block(type="bottleneck", expansion=6, depth=96, repeats=3, stride=1, scope="bottleneck_5"),
    Block(type="bottleneck", expansion=6, depth=160, repeats=3, stride=2, scope="bottleneck_6"),
    Block(type="bottleneck", expansion=6, depth=320, repeats=1, stride=1, scope="bottleneck_7"),
    Block(type="conv", kernel=1, depth=1280, stride=1, scope="conv_8"),
]


def get_net_def(depth_multiplier, divisible_by=8, min_depth=8, finegrain_classification_mode=True):
    if depth_multiplier == 1.0:
        new_def = NET_DEF
    else:
        new_def = []
        for block in NET_DEF:
            args = block._asdict()
            new_depth = make_divisible(args["depth"] * depth_multiplier, divisible_by, min_depth)
            args["depth"] = new_depth
            new_def.append(_Block(**args))

        if finegrain_classification_mode and depth_multiplier < 1:
            new_def[-1] = NET_DEF[-1]
    return new_def


def make_divisible(depth, divisor, min_depth):
    new_depth = max(min_depth, int(depth + divisor / 2) // divisor * divisor)
    # Make sure that round down does not go down by more than 10%.
    if new_depth < 0.9 * depth:
        new_depth += divisor
    return new_depth


def parse_block(input_net, block, stride, scope):
    """
    Parse single block.
    """
    with tf.variable_scope(scope):
        if block.type is "conv":
            net = slim.conv2d(input_net, num_outputs=block.depth, kernel_size=block.kernel, stride=stride)
        elif block.type is "bottleneck":
            input_depth = input_net.get_shape().as_list()[-1]
            expansion_depth = block.expansion * input_depth

            net = slim.conv2d(input_net, num_outputs=expansion_depth, kernel_size=1, stride=1, scope="expand")

            net = slim.separable_conv2d(net, num_outputs=None, kernel_size=3, stride=stride, scope="depthwise")

            net = slim.conv2d(net, kernel_size=1, stride=1, num_outputs=block.depth, activation_fn=tf.identity,
                              scope="linear")

            # set residual if possible
            if stride == 1 and input_depth == block.depth:
                net = tf.add(net, input_net, name="residual")
        else:
            raise ValueError(f"Block type {block.type} is not supported!")

    return net


def mobilenet_v2_base(inputs,
                      net_def,
                      final_endpoint="conv_8"):
    end_points = {}
    net = inputs

    for block in net_def:
        for idx in range(block.repeats):
            stride = block.stride if idx == 0 else 1
            scope = block.scope if block.repeats == 1 else f"{block.scope}_{idx}"

            net = parse_block(net, block, stride, scope)

            end_points[scope] = net
            if scope == final_endpoint:
                return net, end_points

    raise ValueError("Unknown final endpoint %s" % final_endpoint)


def mobilenet_v2(inputs,
                 num_classes,
                 net_def=NET_DEF,
                 prediction_fn=partial(slim.softmax),
                 spatial_squeeze=True,
                 reuse=None,
                 scope="MobileNetV2"):
    input_shape = inputs.get_shape().as_list()
    if len(input_shape) != 4:
        raise ValueError("Invalid input tensor rank, expected 4, was: %d" %
                         len(input_shape))

    with tf.variable_scope(scope, [inputs], reuse=reuse):
        net, end_points = mobilenet_v2_base(inputs,
                                            net_def,
                                            final_endpoint="conv_8")

        with tf.variable_scope("Logits"):
            # tf.reduce_mean is not quantizable
            net = slim.avg_pool2d(net, net.get_shape().as_list()[1:3], padding="VALID", scope="avgpool")

            if not num_classes:
                return net, end_points

            net = slim.dropout(net)

            logits = slim.conv2d(net, num_classes, 1, activation_fn=None, normalizer_fn=None)

            if spatial_squeeze:
                shape = logits.get_shape().as_list()
                logits = tf.reshape(logits, shape=(-1, shape[3]), name="squeeze_logit")

            if prediction_fn:
                end_points["Predictions"] = prediction_fn(logits)

    return logits, end_points


def mobilenet_v2_multitask(inputs,
                           num_classes,
                           num_tasks,
                           net_def=NET_DEF,
                           prediction_fn=partial(slim.softmax),
                           spatial_squeeze=True,
                           reuse=None,
                           scope="MobileNetV2"):
    input_shape = inputs.get_shape().as_list()
    if len(input_shape) != 4:
        raise ValueError("Invalid input tensor rank, expected 4, was: %d" %
                         len(input_shape))

    with tf.variable_scope(scope, [inputs], reuse=reuse):
        net, end_points = mobilenet_v2_base(inputs,
                                            net_def,
                                            final_endpoint="conv_8")

        # tf.reduce_mean is not quantizable
        # FIXME: Tensorflow code is fixed to support recently.
        shared_net = slim.avg_pool2d(net, net.get_shape().as_list()[1:3], padding="VALID", scope="avgpool")

        task_logits = []
        for i in range(num_tasks):
            net = shared_net
            with tf.variable_scope(f"task{i}"):
                net = slim.dropout(net)

                logits = slim.conv2d(net, num_classes, 1, activation_fn=None, normalizer_fn=None)

                if spatial_squeeze:
                    shape = logits.get_shape().as_list()
                    logits = tf.reshape(logits, shape=(-1, 1, shape[3]), name="squeeze_logit")

                task_logits.append(logits)

        # TFlite doesn't support stack
        task_stacked_logits = tf.concat(task_logits, axis=1)  # (N, t, c)

        if prediction_fn:
            end_points["Predictions"] = prediction_fn(task_stacked_logits)

    return task_stacked_logits, end_points


def mobilenet_v2_arg_scope(is_training=True,
                           weight_decay=0.00004,
                           stddev=0.09,
                           dropout_keep_prob=0.8,
                           batch_norm_decay=0.997,
                           batch_norm_epsilon=0.001,
                           use_fused_batchnorm=True):
        # Note: do not introduce parameters that would change the inference
        # model here (for example whether to use bias), modify conv_def instead.
    batch_norm_params = {
        "is_training": is_training,
        "decay": batch_norm_decay,
        "epsilon": batch_norm_epsilon,
        "center": True,
        "scale": True,
        "fused": use_fused_batchnorm,
    }

    if stddev < 0:
        weight_intitializer = slim.initializers.xavier_initializer()
    else:
        weight_intitializer = tf.truncated_normal_initializer(stddev=stddev)

    # Set weight_decay for weights in Conv and FC layers.
    with slim.arg_scope([slim.conv2d, slim.separable_conv2d],
                        weights_initializer=weight_intitializer,
                        weights_regularizer=slim.l2_regularizer(weight_decay),
                        normalizer_fn=slim.batch_norm,
                        activation_fn=tf.nn.relu6,
                        padding="SAME"):
        with slim.arg_scope([slim.batch_norm], **batch_norm_params):
            with slim.arg_scope([slim.dropout], is_training=is_training, keep_prob=dropout_keep_prob):
                with slim.arg_scope([slim.separable_conv2d], depth_multiplier=1) as s:
                    return s
