# 기계학습 산업응용 Project1/2
## M1505.001300_001 2018 가을 Team5 , Project 1

| Team5 Members |||
| --- | --- | --- |
| 2011-11949 신범준 | 2011-11927 임능환 | 2012-11842 김진홍 |


## Requirements

- Python 3.6+ (f-string을 사용하기 때문에 반드시 3.6 이상이여야함)
- ffmpeg : mp3 -> wav 에 사용

## Clone and install

레포를 클론합니다.

```
git clone git@gitlab.com:lucidus21/iaml.git
```

## Install

```
$ (자신이 선호하는 방법으로 파이썬 가상환경을 만듭니다)
$ pip install -r requirements.txt  # cpu or gpu
$ pip install tensorflow-[cpu/gpu]
```

## test 데이터셋으로 테스트하기 (to. 조교)

1. 프로젝트 루트폴더의 `data/input/music_dataset/` 위치에 다음과 같이 데이터를 전부 넣는다

```
# (예시)
iaml/
└─data/input/music_dataset/000/
└─data/input/music_dataset/001/
└─...
```

2. 다음 커맨드를 사용하여 이미지넷 스타일로 디렉토리(`music_dataset_split`)로 데이터를 나눈다

- `track_metadata.csv`의 `split`을 기준으로 나뉘게 된다
- 조교님의 경우 뒤에 있을 테스트를 위해 `track_metadata.csv`에 `split` 이름이 `test`인 row가 추가적으로 있다고 가정합니다
- 이 커맨드에 의해 모든 `mp3` 파일이 `wav` 파일로 변환된다 (`ffmpeg` 사용)

```bash
cd data_scripts/
python split_to_music_genre.py \
    --source_dir ../data/input/music_dataset/ \
    --target_dir ../data/input/music_dataset_split/ \
    --metadata ../etc/track_metadata.csv
```

위 커맨드를 돌리고 나면 다음과 같은 폴더 구조가 생긴다

```
IAML/
└─data/input/music_dataset_split/training/Electronic/
└─data/input/music_dataset_split/training/Experimental/
└─...
└─data/input/music_dataset_split/validation/Experimental/
└─...
```

3. (project1)  `code/`에서 `ensemble.py`를 돌려서 원하는 데이터셋에 대해 예측한다.

- **`dataset_split_name`의 경우 track_metadata.csv의 split 에 해당하는 이름을 사용한다**
- validation 데이터에 대해 앙상블 하고 싶다면 `validation`을 넣어 돌리면 된다
- 조교님의 경우, 가지고 계신 테스트 데이터셋의 `split` 이름이 `test`인 경우 `dataset_split_name` 에 `test`를 넘기시면 됩니다
- 프로젝트 루트에 제출된 `selected_experiments.zip`의 압축을 푼다 ([selected_experiments 다운 링크](https://www.dropbox.com/s/v05uh5uouuq1bsi/20181028_selected_experiments.zip?dl=0))
- 만약 메모리 에러가 발생하면 `max_batch_size`를 줄인다
- **`dataset_split_name`과 `max_batch_size` 이외의 파라미터는 바꾸지 말아주세요.**
- 이미 `../data/ensemble/` 폴더가 있는 경우 아래 커맨드에 의해 저절로 지워집니다.

```bash
cd code/
python ensemble.py \
  --dataset_substring music_dataset_split \
  --best_metric validation/mAP/validation/macro/ \
  --score_type max \
  --max_batch_size 32 \
  --score_threshold 0.58 \
  --search_dir ../selected_experiments/ \
  --preserved_keys clip_duration_ms window_size_ms window_stride_ms audio_preprocess preprocess_method augmentation_method num_classes task_type output_name output_type width height num_mel_bins num_mfccs \
  --inference_output_dirname ../data/ensemble \
  --evaluate_file evaluate_audio.py \
  --inference_output prob \
  --dataset_path ../data/input/music_dataset_split \
  --dataset_split_name [dataset_split_name, 사용하는 데이터셋 기준으로 입력!] \
  --no-shuffle
```

위 커맨드를 돌리고나면 다음과 같은 아웃풋이 나와야한다.


```python
==============================
Accuracy on [모델 폴더1]: 0.51953125
Accuracy on [모델 폴더2]: 0.51171875
Accuracy on [모델 폴더3]: 0.5546875
Accuracy on [모델 폴더4]: 0.51171875
Accuracy on [모델 폴더5]: 0.5390625
==============================
Final accuracy: 0.59765625 on ../data/input/music_dataset_split/test
Save prediction to ../data/ensemble/ensemble.csv
==============================
```

각 파일별 예측 결과는 `../data/ensemble/ensemble.csv` 에서 확인 가능하다.

# *Project2* 는 다음 커맨드로 돌리면 됩니다.


```
python ensemble.py \
  --dataset_substring music_dataset_hit \
  --best_metric validation/mAP/validation/macro/ \
  --score_type max \
  --max_batch_size 32 \
  --score_threshold 0.31 \
  --search_dir ../selected_experiments-project2/ \
  --preserved_keys clip_duration_ms window_size_ms window_stride_ms audio_preprocess preprocess_method augmentation_method num_classes task_type output_name output_type width height num_mel_bins num_mfccs \
  --inference_output_dirname ../data/ensemble \
  --evaluate_file evaluate_audio.py \
  --inference_output prob \
  --dataset_path ../data/input/music_dataset_hit \
  --dataset_split_name validation \
  --no-shuffle
```

## 학습/평가 돌리는 방법

```bash
cd code/
python train_audio.py \
  --dataset_path ../data/input/music_dataset_split \
  --task_type audio_single \
  --output_name output/softmax \
  --output_type softmax \
  --preprocess_method preprocess_expand_only \
  --dataset_split_name training \
  --width -1 \
  --height -1 \
  --num_classes 8 \
  --augmentation_method random_slice_or_pad_with_repeat \
  --train_dir ../data/working/%DATE%-%USER%-experiment \
  --audio_preprocess log_mel_spectrogram \
  --clip_duration_ms 20000 \
  --window_size_ms 24 \
  --window_stride_ms 12 \
  Resnext50Model \
  --weight_decay 0.001
python evaluate_audio.py \
  --dataset_path ../data/input/music_dataset_split \
  --task_type audio_single \
  --output_name output/softmax \
  --output_type softmax \
  --preprocess_method preprocess_expand_only \
  --dataset_split_name validation \
  --width -1 \
  --height -1 \
  --num_classes 8 \
  --augmentation_method random_slice_or_pad_with_repeat \
  --checkpoint_path ../data/working/%DATE%-%USER%-experiment \
  --valid_type loop \
  --clip_duration_ms 20000 \
  --window_size_ms 24 \
  --window_stride_ms 12 \
  --audio_preprocess log_mel_spectrogram \
  Resnext50Model \
  --weight_decay 0.001
```


위 커맨드로 학습을 돌리면 `[루트]/data/working/`에 학습 결과가 저장됩니다.
그래프는 tensorboard를 사용해 확인할 수 있습니다.
