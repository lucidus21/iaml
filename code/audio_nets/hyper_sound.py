from tensorflow.contrib.framework.python.ops import add_arg_scope
from tensorflow.contrib.framework.python.ops import variables
import tensorflow as tf

slim = tf.contrib.slim


def hyper_sound_arg_scope(is_training=True,
                          weight_decay=0.00004,
                          stddev=0.09,
                          batch_norm_decay=0.997,
                          batch_norm_epsilon=0.001,
                          use_fused_batchnorm=True):
    batch_norm_params = {
        "is_training": is_training,
        "decay": batch_norm_decay,
        "epsilon": batch_norm_epsilon,
        "center": True,
        "scale": True,
        "fused": use_fused_batchnorm,
    }

    if stddev < 0:
        weight_intitializer = slim.initializers.xavier_initializer()
    else:
        weight_intitializer = tf.truncated_normal_initializer(stddev=stddev)

    with slim.arg_scope([slim.conv2d, slim.separable_conv2d, heightwise_conv2d],
                        weights_initializer=weight_intitializer,
                        weights_regularizer=slim.l2_regularizer(weight_decay),
                        normalizer_fn=slim.batch_norm,
                        activation_fn=tf.nn.relu,
                        padding="SAME"):
        with slim.arg_scope([slim.batch_norm], **batch_norm_params) as scope:
            return scope


def hyper_block(inputs, kernel_sizes, expanded_depth, output_depth, rates, strides, name):
    with tf.variable_scope(name):
        convs = []
        for i, rate in enumerate(rates):
            with tf.variable_scope(f"branch{i}"):
                # Inverted Residual
                conv = slim.conv2d(
                    inputs, num_outputs=expanded_depth, kernel_size=1, stride=1, scope="pointwise_conv"
                )
                if stride > 1:
                    # kernel_sizes: [kernel_height, kernel_width]
                    conv = separable_conv(
                        conv, num_outputs=None, kernel_size=kernel_sizes, stride=stride, depth_multiplier=1,
                        scope="depthwise_conv_stride"
                    )
                conv = separable_conv(conv, num_outputs=None, kernel_size=kernel_sizes, stride=1, depth_multiplier=1,
                                      rate=rate, scope="depthwise_conv_dilation")
                convs.append(conv)

        with tf.variable_scope("merge"):
            if len(convs) > 1:
                net = tf.concat(convs, axis=-1)
            else:
                net = convs[0]

            net = slim.conv2d(
                net,
                num_outputs=output_depth, kernel_size=1, stride=1,
                activation_fn=None, scope="pointwise_conv"
            )
    return net


@add_arg_scope
def heightwise_conv2d(
    inputs,
    num_outputs,
    kernel_sizes,
    strides,
    name,
    padding="VALID",
    normalizer_fn=None,
    activation_fn=None,
    weights_regularizer=None,
    weights_initializer=tf.contrib.layers.xavier_initializer(),
):
    """Implement batch_norm/activation implementations"""
    print(f"heightwise_conv2d: {normalizer_fn} {activation_fn} {kernel_sizes} {strides} {padding} ({name})")
    inputs.get_shape
    _filter = variables.model_variable(
        'filter',
        shape=[kernel_sizes[0], kernel_sizes[1], 1, num_outputs],  # [h, w, in, out]
        dtype=tf.float32,
        initializer=weights_initializer,
        regularizer=weights_regularizer,
        collections="heightwise_weight",
        trainable=True)
    # strides for [N, H, W, C]
    net = tf.nn.conv2d(inputs, filter=_filter, strides=strides, padding="VALID", name=name)
    net = tf.contrib.layers.batch_norm(net, activation_fn=tf.nn.relu)
    assert weights_regularizer is not None
    return net


def init_block(inputs, num_outputs, kernel_sizes, strides, name):
    with tf.variable_scope(name):
        net = heightwise_conv2d(inputs, num_outputs, kernel_sizes=kernel_sizes, strides=strides, name=name)
    return net


def hyper_sound(inputs, num_classes):
    endpoints = {}
    depth = 12
    endpoints["init_block"] = init_block(inputs, depth, [7, 5], [1, 2, 1, 1], "init_block")
    # hyper_block에서 heightwise convolution 사용하기
    # from IPython import embed; embed()  # XXX DEBUG
    # def hyper_block(inputs, kernel_sizes, expanded_depth, output_depth, rates, strides, name):
    endpoints["block0"] = hyper_block(
        endpoints["init_block"], [3, 1], depth*2, depth, rates=[1, 2, 4, 8], strides=[1, 1, 1, 1], name="block0"
    )
    endpoints["block0"] = hyper_block(
        endpoints["init_block"], [3, 1], 64, 32, rates=[1, 2, 4, 8], strides=[1, 1, 1, 1], name="block0"
    )
